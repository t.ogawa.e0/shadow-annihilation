//=================================================================
// エフェクト[Effect.h]
// author: tatuya ogawa
//=================================================================

#ifndef _EFFECT_H_ // インクルードガード
#define _EFFECT_H_

//=================================================================
// マクロ定義
#define LIMITEFFECT (2500)
#define EFFECTTIME (7)

//=================================================================
// エフェクト構造体
typedef struct _EFFECT
{
	POLYGON List; // リスト
	bool bDraw; // 描画判定
	bool bUpdate; // 更新判定
}EFFECT;

//=================================================================
// エフェクト
class CEffect
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// エフェクト生成
	void SetEffect(FLOAT_List Poly, int ID);

	// エフェクトの数の取得
	int GetNumEffect(void);
};

#endif // !_EFFECT_H_
