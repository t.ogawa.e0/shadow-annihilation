//=================================================================
// 背景[Background.h]
// author: tatuya ogawa
//=================================================================

#ifndef _BACKGROUND_H_ // インクルードガード
#define _BACKGROUND_H_

//=================================================================
// マクロ定義
#define NUMBACKGROUND (1) // 背景の数

//=================================================================
// 構造体
// バックグラウンドの構造体
typedef struct _BACKGROUND
{
	POLYGON List; // 構造体セット
}BACKGROUND;

//=================================================================
// 背景
class CBackGround
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 全パラメーター取得
	BACKGROUND Get(void);
};

#endif // !_BACKGROUND_H_
