//=================================================================
// マップ[Map.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "main.h"
#include "Map.h"
#include "MovePosData.h"
#include "Player.h"
#include "Restriction.h"

//=================================================================
// マクロ定義
#define _DATASIZED_ ((int)((float)(_MAPDATA_) / (float)(_MAPTEXSIZE_)) * (int)((float)(_MAPDATA_) / (float)(_MAPTEXSIZE_)))

//=================================================================
// グローバル変数
static MAPPOLIGON *g_aMapPoly; // マップポリゴンのデータ

//-----------------------------------------------------------------
// 初期化
void CMap::Init(void)
{
	CPolygon polygon;
	float fMapDataList[(int)(_MAPDATA_ / _MAPTEXSIZE_)]; // マップ生成用の箱
	int nCount = I_NULL;

	g_aMapPoly = MemoryAllocation(g_aMapPoly, (int)_DATASIZED_);
	
	// マップ生成用の値格納
	for (int i = 0; i < (int)(sizeof(fMapDataList) / _BIT_); i++)
	{
		fMapDataList[i] = (-(float)(_MAPDATA_ / CENTER_POS) + (float)((int)(_MAPDATA_ / (int)(_MAPDATA_ / _MAPTEXSIZE_)) * i));
	}

	// マップデータの初期化
	for (int i = 0; i < (int)_DATASIZED_; i++)
	{
		g_aMapPoly[i].List = polygon.PolygonDataInitList();
		g_aMapPoly[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_asphalt;
		g_aMapPoly[i].List.Poly.w = _MAPTEXSIZE_;
		g_aMapPoly[i].List.Poly.h = _MAPTEXSIZE_;
		g_aMapPoly[i].List.bAffine = B_OFF;
		if (nCount == (int)(_MAPDATA_ / _MAPTEXSIZE_)) { nCount = I_NULL; }
		g_aMapPoly[i].List.Poly.x = fMapDataList[nCount]; // マップ配置座標代入
		g_aMapPoly[i].List.Poly.y = fMapDataList[(i / (int)(_MAPDATA_ / _MAPTEXSIZE_))]; // マップ配置座標代入
		g_aMapPoly[i].List.Poly.x += MapCorrect().x; // 座標補正
		g_aMapPoly[i].List.Poly.y += MapCorrect().y; // 座標補正
		nCount++;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CMap::Uninit(void)
{
	AllMemoryDelete(g_aMapPoly);
}

//-----------------------------------------------------------------
// 更新処理
void CMap::Update(bool ControlStart)
{
	// 処理有効フラグ
	if (ControlStart != true)
	{
		CMovePosData move;

		for (int i = 0; i < (int)_DATASIZED_; i++)
		{
			g_aMapPoly[i].List.Poly.x -= move.Get().aMove.x;
			g_aMapPoly[i].List.Poly.y -= move.Get().aMove.y;
		}
	}
}

//-----------------------------------------------------------------
// 描画処理
void CMap::Draw(void)
{
	CPolygon polygon;
	CRestriction restriction;

	for (int i = 0; i < (int)_DATASIZED_; i++)
	{
		if (restriction.Draw(g_aMapPoly[i].List.Poly) == true)
		{
			polygon.Draw(g_aMapPoly[i].List);
		}
	}
}

//-----------------------------------------------------------------
// マップ補正
MAPCORRECT CMap::MapCorrect(void)
{
	MAPCORRECT Pos;
	CPolygon polygon;

	// マップ初期化時の補正値
	Pos.x = (float)(SCREEN_WIDTH / CENTER_SEARCH) - ((float)polygon.GetTextureInfo(GAME_TEXTURE_LIST::TEXTURE_RedCar).Widht / _PLAYER_SCALE_); // 座標補正値割り出し
	Pos.y = (float)(SCREEN_HEIGHT / CENTER_SEARCH) - (((float)polygon.GetTextureInfo(GAME_TEXTURE_LIST::TEXTURE_RedCar).Height / _PLAYER_SCALE_) / CENTER_POS); // 座標補正値割り出し

	return Pos;
}

//-----------------------------------------------------------------
// 全パラメーター取得
MAPPOLIGON CMap::Get(int Count)
{
	return  g_aMapPoly[Count];
}