//=================================================================
// ポーズ処理[Pause.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\NumBerPallet.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "input.h"
#include "Pause.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "SceneChange.h"

//=================================================================
// グローバル変数
static PAUSSE *g_aPause;
static INTFRAME *g_Manu;
static int *g_nPauseFont;
static int *g_ManuCenter;
static int *g_Cathi;
static int *g_nManu;
static bool *g_Pause;
static bool *g_bChange;

//-----------------------------------------------------------------
// 初期化
void CPause::Init(void)
{
	CPolygon polygon;
	int nData = -1;

	g_aPause = MemoryAllocation(g_aPause, NUMPAUSE);
	g_Pause = MemoryAllocation(g_Pause, 1);
	g_bChange = MemoryAllocation(g_bChange, 1);
	g_nPauseFont = MemoryAllocation(g_nPauseFont, 1);
	g_Cathi = MemoryAllocation(g_Cathi, 1);
	g_ManuCenter = MemoryAllocation(g_ManuCenter, 1);
	g_nManu = MemoryAllocation(g_nManu, 1);
	g_Manu = MemoryAllocation(g_Manu, 1);
	FLOAT_List Pos, Pos1, Pos2, Pos3;

	g_Pause[0] = B_OFF;
	g_bChange[0] = B_OFF;
	g_Cathi[0] = 0;

	for (int i = 0; i < NUMPAUSE; i++)
	{
		g_aPause[i].List = polygon.PolygonDataInitList();
		g_aPause[i].Time.Count = -1;
		g_aPause[i].Time.End = PAUSE_FONT_COUNT;
	}

	// ポーズウィンドウ
	nData++;
	g_aPause[nData].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aPause[nData].List.Poly.h = (float)SCREEN_HEIGHT;
	g_aPause[nData].List.Poly.w = (float)SCREEN_WIDTH;
	g_aPause[nData].List.Color.r = g_aPause[nData].List.Color.g = g_aPause[nData].List.Color.b = COLORPALLET_0;
	g_aPause[nData].List.Color.a = COLORPALLET_150;

	// メニューウィンドウ
	nData++;
	g_aPause[nData].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aPause[nData].List.Poly.h = SCREEN_HEIGHT / PAUSE_SCLLENSCALE;
	g_aPause[nData].List.Poly.w = SCREEN_WIDTH / PAUSE_SCLLENSCALE;
	g_aPause[nData].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aPause[nData].List.Poly.w / CENTER_POS);
	g_aPause[nData].List.Poly.y = (SCREEN_HEIGHT / CENTER_POS) - (g_aPause[nData].List.Poly.h / CENTER_POS);
	g_aPause[nData].List.Color.r = COLORPALLET_41;
	g_aPause[nData].List.Color.g = COLORPALLET_237;
	g_aPause[nData].List.Color.b = COLORPALLET_255;
	g_aPause[nData].List.Color.a = COLORPALLET_150;
	Pos = g_aPause[nData].List.Poly;

	// ポタン1
	nData++;
	g_Manu[0].Count = nData;
	g_nManu[0] = g_Manu[0].Count;
	g_aPause[nData].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aPause[nData].List.Poly.h = PAUSE_MANUE_H;
	g_aPause[nData].List.Poly.w = PAUSE_MANUE_W;
	g_aPause[nData].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aPause[nData].List.Poly.w / CENTER_POS);
	g_aPause[nData].List.Poly.y = Pos.y + PAUSE_SCLLENSCALE1;
	g_aPause[nData].List.Color.r = COLORPALLET_41;
	g_aPause[nData].List.Color.g = COLORPALLET_66;
	g_aPause[nData].List.Color.b = COLORPALLET_255;
	g_aPause[nData].List.Color.a = COLORPALLET_150;
	Pos1 = Pos = g_aPause[nData].List.Poly;

	// ポタン2
	nData++;
	g_ManuCenter[0] = nData;
	g_aPause[nData].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aPause[nData].List.Poly.h = PAUSE_MANUE_H;
	g_aPause[nData].List.Poly.w = PAUSE_MANUE_W;
	g_aPause[nData].List.Poly.x = Pos.x;
	g_aPause[nData].List.Poly.y = Pos.y + Pos.h + (g_aPause[nData].List.Poly.h / CENTER_POS);
	g_aPause[nData].List.Color.r = COLORPALLET_41;
	g_aPause[nData].List.Color.g = COLORPALLET_66;
	g_aPause[nData].List.Color.b = COLORPALLET_255;
	g_aPause[nData].List.Color.a = COLORPALLET_0;
	Pos2 = Pos = g_aPause[nData].List.Poly;

	// ポタン3
	nData++;
	g_Manu[0].End = nData;
	g_aPause[nData].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aPause[nData].List.Poly.h = PAUSE_MANUE_H;
	g_aPause[nData].List.Poly.w = PAUSE_MANUE_W;
	g_aPause[nData].List.Poly.x = Pos.x;
	g_aPause[nData].List.Poly.y = Pos.y + Pos.h + (g_aPause[nData].List.Poly.h / CENTER_POS);
	g_aPause[nData].List.Color.r = COLORPALLET_41;
	g_aPause[nData].List.Color.g = COLORPALLET_66;
	g_aPause[nData].List.Color.b = COLORPALLET_255;
	g_aPause[nData].List.Color.a = COLORPALLET_0;
	Pos3 = Pos = g_aPause[nData].List.Poly;

	// ポーズフォント
	nData++;
	g_nPauseFont[0] = nData;
	g_aPause[nData].List.TexName = GAME_TEXTURE_LIST::TEXTURE_PAUSE;
	g_aPause[nData].List.Poly.w = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Widht / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.h = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Height / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aPause[nData].List.Poly.w / CENTER_POS);
	g_aPause[nData].List.Poly.y = (SCREEN_HEIGHT / CENTER_POS) - g_aPause[nData].List.Poly.h - (g_aPause[nData].List.Poly.h / CENTER_POS);

	// 閉じるフォント
	nData++;
	g_aPause[nData].List.TexName = GAME_TEXTURE_LIST::TEXTURE_CloseFont;
	g_aPause[nData].List.Poly.w = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Widht / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.h = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Height / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.x = Pos1.x + PAUSE_SCLLENSCALE2;
	g_aPause[nData].List.Poly.y = Pos1.y - PAUSE_SCLLENSCALE1;

	// タイトルフォント
	nData++;
	g_aPause[nData].List.TexName = GAME_TEXTURE_LIST::TEXTURE_TitleFont;
	g_aPause[nData].List.Poly.w = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Widht / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.h = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Height / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.x = Pos2.x + PAUSE_SCLLENSCALE2;
	g_aPause[nData].List.Poly.y = Pos2.y;

	// リトライフォント
	nData++;
	g_aPause[nData].List.TexName = GAME_TEXTURE_LIST::TEXTURE_RetryFont;
	g_aPause[nData].List.Poly.w = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Widht / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.h = (float)(polygon.GetTextureInfo(g_aPause[nData].List.TexName).Height / PAUSE_SCLLENSCALE3);
	g_aPause[nData].List.Poly.x = Pos3.x + PAUSE_SCLLENSCALE2;
	g_aPause[nData].List.Poly.y = Pos3.y;
}

//-----------------------------------------------------------------
// 終了処理
void CPause::Uninit(void)
{
	AllMemoryDelete(g_Cathi);
	AllMemoryDelete(g_aPause);
	AllMemoryDelete(g_bChange);
	AllMemoryDelete(g_Pause);
	AllMemoryDelete(g_ManuCenter); 
	AllMemoryDelete(g_nPauseFont);
	AllMemoryDelete(g_Manu);
	AllMemoryDelete(g_nManu);
}

//-----------------------------------------------------------------
// 更新処理
bool CPause::Update(void)
{
	// Pキー入力時に切り替え
	if (_TRIGGER_(DIK_P))
	{
		PlaySound(LOAD_SOUND_LIST::Button_SE);
		g_Pause[0] = _BOOL_(g_Pause[0]);
		g_aPause[g_nPauseFont[0]].Time.Count = -NUMBERPALETTE_1;
		g_aPause[g_nPauseFont[0]].List.Color.a = COLORPALLET_255;
		g_Cathi[0] = I_NULL;
	}

	if (g_Pause[0] == B_ON)
	{
		CScene scene;

		g_aPause[g_nPauseFont[0]] = Font(g_aPause[g_nPauseFont[0]]);

		if (_TRIGGER_(DIK_W) || _TRIGGER_(DIK_UP))
		{
			g_Cathi[0]--;
			PlaySound(LOAD_SOUND_LIST::Button_SE);
			if (g_Cathi[0] < NUMBERPALETTE_0) { g_Cathi[0] = NUMBERPALETTE_0; }
		}

		if (_TRIGGER_(DIK_S) || _TRIGGER_(DIK_DOWN))
		{
			g_Cathi[0]++;
			PlaySound(LOAD_SOUND_LIST::Button_SE);
			if (g_Cathi[0] > NUMBERPALETTE_2) { g_Cathi[0] = NUMBERPALETTE_2; }
		}
		switch (g_Cathi[0])
		{
		case 0:
			g_nManu[0] = g_Manu[0].Count;
			g_aPause[g_Manu[0].Count].List.Color.a = COLORPALLET_150;
			g_aPause[g_ManuCenter[0]].List.Color.a = COLORPALLET_0;
			g_aPause[g_Manu[0].End].List.Color.a = COLORPALLET_0;
			break;
		case 1:
			g_nManu[0] = g_ManuCenter[0];
			g_aPause[g_Manu[0].Count].List.Color.a = COLORPALLET_0;
			g_aPause[g_ManuCenter[0]].List.Color.a = COLORPALLET_150;
			g_aPause[g_Manu[0].End].List.Color.a = COLORPALLET_0;
			break;
		case 2:
			g_nManu[0] = g_Manu[0].End;
			g_aPause[g_Manu[0].Count].List.Color.a = COLORPALLET_0;
			g_aPause[g_ManuCenter[0]].List.Color.a = COLORPALLET_0;
			g_aPause[g_Manu[0].End].List.Color.a = COLORPALLET_150;
			break;
		default:
			break;
		}

		if (_TRIGGER_(DIK_RETURN))
		{
			PlaySound(LOAD_SOUND_LIST::Button_SE);
			if (g_nManu[0] == g_Manu[0].Count)
			{
				g_Pause[0] = _BOOL_(g_Pause[0]);
			}
			if (g_nManu[0] == g_ManuCenter[0])
			{
				scene.Change(SCENE_TITLE);
			}
			if (g_nManu[0] == g_Manu[0].End)
			{
				g_bChange[0] = B_ON;
			}
		}
		scene.Retry(g_bChange[0]);
	}
	
	return g_Pause[0];
}

//-----------------------------------------------------------------
// 描画開始
void CPause::Draw(void)
{
	if (g_Pause[0] == B_ON)
	{
		int aArea = 0;
		CPolygon polygon;

		for (int i = 0; i < NUMPAUSE; i++)
		{
			if (i == NUMBERPALETTE_1) { aArea = polygon.StaticAreaStart(); }
			polygon.Draw(g_aPause[i].List);
			if (i == NUMBERPALETTE_1) { polygon.StaticAreaEnd(aArea); }
		}
	}
}

//-----------------------------------------------------------------
// ポーズフォント
PAUSSE CPause::Font(PAUSSE aPause)
{
	// α値変更
	if (aPause.Time.Count >= (aPause.Time.End / NUMBERPALETTE_2))
	{
		aPause.List.Color.a = I_NULL;
	}

	// α値変更
	if (aPause.Time.Count >= aPause.Time.End)
	{
		aPause.List.Color.a = _RGBACOLOR_;
		aPause.Time.Count = I_NULL;
	}
	aPause.Time.Count++;

	return aPause;
}