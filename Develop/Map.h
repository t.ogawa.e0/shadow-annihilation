//=================================================================
// マップ[Map.h]
// author: tatuya ogawa
//=================================================================

#ifndef _MAP_H_ // インクルードガード
#define _MAP_H_

//=================================================================
// マクロ定義
#define _MAPDATA_ (10000.0f) // マップサイズ
#define _MAPTEXSIZE_ (1000.0f) // 画像のサイズ固定

//=================================================================
// 構造体
typedef struct _MAPCORRECT
{
	float x;
	float y;
}MAPCORRECT;

// マップデータの構造体
typedef struct _MAPPOLIGON
{
	POLYGON List; // 構造体セット
}MAPPOLIGON;

//=================================================================
// マップ
class CMap
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(bool ControlStart);

	// 描画処理
	void Draw(void);

	// マップ補正
	MAPCORRECT MapCorrect(void);

	// 全パラメーター取得
	MAPPOLIGON Get(int Count);
};

#endif // !_MAP_H_
