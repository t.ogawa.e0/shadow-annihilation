//=================================================================
// 当たり判定[Judgment.h]
// author: tatuya ogawa
//=================================================================

#ifndef _JUDGMENT_H_ // インクルードガード
#define _JUDGMENT_H_

//=================================================================
// 当たり判定
class CJudgment
{
private:
	// 対象物A,Bの当たり判定(円形 : 2D用)
	bool Circle(ROUND TargetA, ROUND TargetB);

	// 当たり判定
	bool Judgment(FLOAT_List TargetA, FLOAT_List TargetB);

	// バレットに接触時
	void HitBullet(bool Hit, int Count);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(bool ControlStart);

	// 描画開始
	void Draw(void);
};

#endif // !_JUDGMENT_H_
