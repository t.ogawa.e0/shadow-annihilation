//=================================================================
// 矢印処理[Arrow.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\NumBerPallet.h"
#include "main.h"
#include "MovePosData.h"
#include "Keyboard.h"
#include "Cursor.h"
#include "Player.h"
#include "Enemy.h"
#include "Distance.h"
#include "RotationAngle.h"
#include "Arrow.h"

//=================================================================
// グローバル変数
static ARROW *g_aArrow; // 矢印のデータ

//-----------------------------------------------------------------
// 初期化
void CArrow::Init(void)
{
	CPolygon polygon;

	g_aArrow = MemoryAllocation(g_aArrow, (_NUM_ARROW_ + _NUM_ENEMY_));

	// 矢印初期化
	for (int i = 0; i < (_NUM_ARROW_ + _NUM_ENEMY_); i++)
	{
		g_aArrow[i].List = polygon.PolygonDataInitList();
		g_aArrow[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Arrow;
	
		switch (i)
		{
		case 0: // マウスの方向の視覚化
			g_aArrow[i].List.Poly.w = (float)polygon.GetTextureInfo(g_aArrow[i].List.TexName).Widht;
			g_aArrow[i].List.Poly.h = (float)polygon.GetTextureInfo(g_aArrow[i].List.TexName).Height;
			g_aArrow[i].List.Ang.x = -(g_aArrow[i].List.Poly.w * _SHIFT_);
			g_aArrow[i].List.Ang.y = (g_aArrow[i].List.Poly.h / CENTER_POS);
			g_aArrow[i].Shift = _SHIFT_;
			g_aArrow[i].Distance = F_NULL;
			g_aArrow[i].List.Color.r = g_aArrow[i].List.Color.g = g_aArrow[i].List.Color.b = I_NULL;
			g_aArrow[i].bDraw = true;
			break;
		default: // エネミーの座標視覚化用矢印の初期化
			g_aArrow[i].List.Poly.w = (float)polygon.GetTextureInfo(g_aArrow[i].List.TexName).Widht / _ARROW2SIZE_;
			g_aArrow[i].List.Poly.h = (float)polygon.GetTextureInfo(g_aArrow[i].List.TexName).Height / _ARROW2SIZE_;
			g_aArrow[i].List.Ang.x = -(g_aArrow[i].List.Poly.w* _SHIFT2_);
			g_aArrow[i].List.Ang.y = (g_aArrow[i].List.Poly.h / CENTER_POS);
			g_aArrow[i].Shift = _SHIFT2_;
			g_aArrow[i].Distance = F_NULL;
			g_aArrow[i].List.Color.r = COLORPALLET_40;
			g_aArrow[i].List.Color.g = COLORPALLET_31;
			g_aArrow[i].List.Color.b = COLORPALLET_244;
			g_aArrow[i].bDraw = true;
			break;
		}
		g_aArrow[i].List.bAffine = B_ON;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CArrow::Uninit(void)
{
	AllMemoryDelete(g_aArrow);
}

//-----------------------------------------------------------------
// 更新処理
void CArrow::Update(void)
{
	CCursor cursor;
	CPlayer player;
	CEnemy enemy;
	CDistance distance;
	CAngle angle;

	for (int i = 0; i < (_NUM_ARROW_ + _NUM_ENEMY_); i++)
	{
		switch (i)
		{
		case 0: // マウスの矢印
			g_aArrow[i].List.Poly = Arrow(player.Get().List.Poly, g_aArrow[i].List.Poly, g_aArrow[i].Shift);
			g_aArrow[i].List.Ang.Angle = angle.Angle2D(player.Get().List.Poly, cursor.Get().List.Poly);
			break;
		default: // エネミーの座標
			if (enemy.Get((i - NUMBERPALETTE_1)).bEmergence != false)
			{
				g_aArrow[i].bDraw = true;
				g_aArrow[i].Distance = distance.Distance2D(player.Get().List.Poly, enemy.Get((i - NUMBERPALETTE_1)).List.Poly);

				// 矢印のレベル
				if (_ARROW2_LEVEL2 <= g_aArrow[i].Distance)
				{
					g_aArrow[i].List.Color.r = COLORPALLET_40;
					g_aArrow[i].List.Color.g = COLORPALLET_31;
					g_aArrow[i].List.Color.b = COLORPALLET_244;
				}
				else if (_ARROW2_LEVEL3 <= g_aArrow[i].Distance && g_aArrow[i].Distance <= _ARROW2_LEVEL2)
				{
					g_aArrow[i].List.Color.r = COLORPALLET_236;
					g_aArrow[i].List.Color.g = COLORPALLET_250;
					g_aArrow[i].List.Color.b = COLORPALLET_0;
				}
				else if (_ARROW2_NOTDROW <= g_aArrow[i].Distance && g_aArrow[i].Distance <= _ARROW2_LEVEL3)
				{
					g_aArrow[i].List.Color.r = COLORPALLET_255;
					g_aArrow[i].List.Color.g = COLORPALLET_32;
					g_aArrow[i].List.Color.b = COLORPALLET_32;
				}
				else if (g_aArrow[i].Distance <= _ARROW2_NOTDROW)
				{
					g_aArrow[i].bDraw = false;
				}
				g_aArrow[i].List.Poly = Arrow(player.Get().List.Poly, g_aArrow[i].List.Poly, g_aArrow[i].Shift);
				g_aArrow[i].List.Ang.Angle = angle.Angle2D(player.Get().List.Poly, enemy.Get((i - NUMBERPALETTE_1)).List.Poly);
			}
			else
			{
				g_aArrow[i].bDraw = false;
			}
			break;
		}
	}
}

//-----------------------------------------------------------------
// 描画処理
void CArrow::Draw(void)
{
	CPolygon polygon;

	for (int i = 0; i < (_NUM_ARROW_ + _NUM_ENEMY_); i++)
	{
		if (g_aArrow[i].bDraw == true)
		{
			polygon.Draw(g_aArrow[i].List);
		}
	}
}

//-----------------------------------------------------------------
// カーソル標示位置
FLOAT_List CArrow::Arrow(FLOAT_List Player, FLOAT_List Arrow, float Shift)
{
	CPolygon polygon;

	Arrow.x = Player.x;
	Arrow.y = Player.y;
	Arrow.x += Arrow.w * (_ARROW_CORRECTION_ + Shift);
	Arrow.y += Arrow.h - ((float)polygon.GetTextureInfo(GAME_TEXTURE_LIST::TEXTURE_Arrow).Widht / Shift);

	return Arrow;
}

//-----------------------------------------------------------------
// 全パラメーター取得
ARROW CArrow::Get(int i)
{
	return g_aArrow[i];
}