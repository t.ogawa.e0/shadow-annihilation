//=================================================================
// デバッグ[Debug.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "MovePosData.h"
#include "Debug.h"
#include "input.h"
#include "Player.h"
#include "Cursor.h"
#include "Arrow.h"
#include "Map.h"
#include "Keyboard.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Explosion.h"
#include "Restriction.h"
#include "RotationAngle.h"
#include "Effect.h"

//=================================================================
// デバッグ用
#if defined(_DEBUG) || defined(DEBUG)
typedef struct _OGDEBUG
{
	bool bPolygonDrow = false; // 当たり判定視覚化
	bool bFontDrow = false; // デバッグフォント描画
}OGDEBUG;
#endif

//=================================================================
// グローバル変数
#if defined(_DEBUG) || defined(DEBUG) 
static OGDEBUG g_aDebug;
#endif

//-----------------------------------------------------------------
// 初期化
void _CDebug::Init(void)
{
#if defined(_DEBUG) || defined(DEBUG)
#endif
}

//-----------------------------------------------------------------
// 終了処理
void _CDebug::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
#endif
}

//-----------------------------------------------------------------
// 更新処理
void _CDebug::Update(void)
{
#if defined(_DEBUG) || defined(DEBUG)
#endif
}

//-----------------------------------------------------------------
// 描画処理
void _CDebug::GameDraw(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	CDebug debug;
	CPlayer player;
	CCursor cursor;
	CArrow arrow;
	CMovePosData move;
	CEnemy enemy;
	CBullet bullet;
	CExplosion eplosion;
	CRestriction restriction;
	CAngle angle;
	CEffect effect;

	if (_TRIGGER_(DIK_F1))
	{
		g_aDebug.bPolygonDrow = _BOOL_(g_aDebug.bPolygonDrow);
	}

	if (_TRIGGER_(DIK_F2))
	{
		g_aDebug.bFontDrow = _BOOL_(g_aDebug.bFontDrow);
	}

	if (g_aDebug.bFontDrow == true)
	{
		char *cTrueAndFalse[] = { "false","true" };
		int debugPos = I_NULL;

		debugPos += 2;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("自機の画面座標X=%.2f , 自機の画面座標Y=%.2f", player.Get().List.Poly.x, player.Get().List.Poly.y);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("自機のマップ上の座標X=%.2f , 自機のマップ上の座標Y=%.2f", player.Get().aMapPos.x, player.Get().aMapPos.y);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("自機のデータ上の座標X=%.2f , 自機のデータ上の座標Y=%.2f", player.Get().aDataPos.x, player.Get().aDataPos.y);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("自機の角度=%.2f", player.Get().List.Ang.Angle);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("カーソル座標X=%.2f , カーソル座標Y=%.2f", cursor.Get().List.Poly.x, cursor.Get().List.Poly.y);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("矢印X=%.2f , 矢印Y=%.2f , 矢印回転率=%.2f", arrow.Get(0).List.Poly.x, arrow.Get(0).List.Poly.y, arrow.Get(0).List.Ang.Angle);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("マップの座標X=%.2f , マップの座標Y=%.2f", move.Get().aMapPos.x, move.Get().aMapPos.y);

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("回転角度的なやつ=%.2f", angle.Angle2D(player.Get().List.Poly, cursor.Get().List.Poly));

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("バレット数=%d", bullet.GetNumRelease());

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("エネミーの数=%d", enemy.GetNumRelease());

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("エフェクトの数=%d", eplosion.GetNumRelease());

		debugPos += 20;
		debug.PosAndColor(1, debugPos);
		debug.DPrintf("エフェクト2の数=%d", effect.GetNumEffect());
	}

	if (g_aDebug.bPolygonDrow == true)
	{
		debug.Polygon(8, player.Get().List.Poly);

		for (int i = 0; i < _NUM_ENEMY_; i++)
		{
			if (restriction.Draw(enemy.Get(i).List.Poly) == true)
			{
				debug.Polygon(8, enemy.Get(i).List.Poly);
			}
		}
		
		for (int i = 0; i < MAX_BULLET; i++)
		{
			if (restriction.Draw(bullet.Get(i).List.Poly) == true)
			{
				if (bullet.Get(i).bBullet == true)
				{
					debug.Polygon(8, bullet.Get(i).List.Poly);
				}
			}
		}
	}
#endif
}