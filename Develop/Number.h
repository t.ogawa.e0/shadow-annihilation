//=================================================================
// 番号[Number.h]
// author: tatuya ogawa
//=================================================================

#ifndef _NUMBER_H_ // インクルードガード
#define _NUMBER_H_

//=================================================================
// マクロ定義
#define PUSYU_SCORE (10) // 繰り上げ
#define NUMBER_TOTAL_W (115) // ナンバーリストの幅
#define NUMBER_TOTAL_H (60) // ナンバーリストの高さ
#define NUM_NUMBER (10) // 数字の数
#define NUM_NUMBER_W (NUM_NUMBER / 2) // 横の数字の数
#define NUMBER_W (NUMBER_TOTAL_W / NUM_NUMBER_W) // 数字1つの幅
#define NUMBER_H (NUMBER_TOTAL_H / 2) // 数字1つの高さ

//=================================================================
// 数字
typedef struct _NUMBER
{
	int Digit; // 桁
	int Max; // 上限
	bool Zero; // ゼロ描画判定
	bool LeftAlignment; // 左寄せ判定
}NUMBER;

//=================================================================
// ナンバー
class CNumber
{
private:
public:
	// 初期化
	int Init(int Digit);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(POLYGON List, NUMBER Number, int Score);
};

#endif // !_NUMBER_H_
