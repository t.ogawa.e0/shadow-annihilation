//=================================================================
// 各種座標操作用[MovePosData.h]
// author: tatuya ogawa
//=================================================================

#ifndef _MOVEPOSDATA_H_ // インクルードガード
#define _MOVEPOSDATA_H_

//=================================================================
// マクロ定義
#define NUMMAP (1)

//=================================================================
// マップの始点と終点管理
typedef struct _MAPDATA
{
	float StartX; // マップの始点
	float StartY; // マップの始点
	float EndX; // マップの終点
	float EndY; // マップの終点
}MAPDATA;

//=================================================================
// 構造体
typedef struct _MOVEPOSDATA
{
	SPEED aMove; // 動かす値
	SPEED aMapPos; // 動かす値
	MAPDATA aMapData; // 移動関係
}MOVEPOSDATA;

//=================================================================
// マップ座標
class CMovePosData
{
private:
	// マップ移動座標算出
	SPEED MapMove(SPEED Spped, MAPDATA aData, FLOAT_List aPlayer, SPEED aMap);

	// マップの座標更新
	void SetMapPos(SPEED aMap);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void Draw(void);

	// 全パラメーター取得
	MOVEPOSDATA Get(void);
};

#endif // !_MOVEPOSDATA_H_
