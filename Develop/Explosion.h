//=================================================================
// 爆発[Explosion.h]
// author: tatuya ogawa
//=================================================================

#ifndef _EXPLOSION_H_ // インクルードガード
#define _EXPLOSION_H_

//=================================================================
// マクロ定義
#define _EXDETA_ (13)
#define _PLAYER_EXPLOSION_ (PLAYERID)
#define _ENEMY_EXPLOSION_ (ENEMYID)
#define _COLLISION (2)
#define _COMMON_EXPLOSION_ (3)
#define _EMERGENCE_	(4)
#define _BULLETEFFECT_ANIMATION_ (4) // アニメーション数
#define _BULLETEFFECT_SCALE (10.0f) // エフェクトサイズ調整
#define _ExplosionAcolor_ANIMATION_ (10) // 爆発アニメーション数
#define _ExplosionAcolor_SCALE (1.0f) // 爆発サイズ調整
#define _Spark_SCALE (10.0f) // 爆発サイズ調整
#define _Spark_FLAME (2) // 爆発サイズ調整
#define _EmergenceEffect_ANIMATION_ (5) // エンゲージエフェクトアニメーション数
#define _EmergenceEffect_SCALE (5.0f) // エンゲージエフェクトサイズ調整

//=================================================================
// 爆発エフェクト構造体
typedef struct _EXPLOSION
{
	POLYGON List; // リスト構造体
	INTFRAME Flame; // フレームカウンタ
	bool bExplosion; // 爆発処理フラグ
}EXPLOSION;

//=================================================================
// 爆発
class CExplosion
{
private:
	// 爆発処理
	EXPLOSION Explosion(EXPLOSION aExplosion);

	// 爆発の動き制御
	EXPLOSION MapMove(EXPLOSION aExplosion, SPEED PosData);

	// エフェクトカウンタ
	void DebugNumCount(void);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 爆発処理セット関数
	void Set(POLYGON List, int ExplosionID, bool bHit);

	// エフェクトの数
	int GetNumRelease(void);
};

#endif // !_EXPLOSION_H_
