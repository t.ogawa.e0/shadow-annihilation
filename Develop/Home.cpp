//=================================================================
// ホーム処理[Home.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "Home.h"
#include "input.h"
#include "SceneChange.h"

//=================================================================
// ホーム
typedef struct _HOME
{
	POLYGON List; // ポリゴン
}HOME;

//=================================================================
// マクロ定義
static HOME *g_aHome;

//-----------------------------------------------------------------
// 初期化
void InitHome(void)
{
	CPolygon polygon;

	g_aHome = MemoryAllocation(g_aHome, NUMBERPALETTE_1);
	g_aHome[0].List = polygon.PolygonDataInitList();
	g_aHome[0].List.TexName = I_NULL;
	g_aHome[0].List.Poly.x = F_NULL;
	g_aHome[0].List.Poly.y = F_NULL;
	g_aHome[0].List.Poly.w = (float)polygon.GetTextureInfo(g_aHome[0].List.TexName).Widht;
	g_aHome[0].List.Poly.h = (float)polygon.GetTextureInfo(g_aHome[0].List.TexName).Height;
	PlaySound(HOME_SOUND_LIST::HOME_BeyondTheImmediateSide);
}

//-----------------------------------------------------------------
// 終了処理
void UninitHome(void)
{
	AllMemoryDelete(g_aHome);
}

//-----------------------------------------------------------------
// 更新処理
void UpdateHome(void)
{
	CScene scene;

	if (GetKeyboardTrigger(DIK_RETURN))
	{
		scene.Change(SCENE_GAME);
	}
}

//-----------------------------------------------------------------
// 描画開始
void DrawHome(void)
{
	CPolygon polygon;

	polygon.Draw(g_aHome[0].List);
}