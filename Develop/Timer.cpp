//=================================================================
// タイマー[Timer.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "Number.h"
#include "Timer.h"
#include "main.h"

//=================================================================
// グローバル変数
static TIMER *g_aTimer;

//-----------------------------------------------------------------
// 初期化
void CTimer::Init(void)
{
	CPolygon polygon;
	CNumber number;

	g_aTimer = MemoryAllocation(g_aTimer, _NUM_TIMER_);

	for (int i = 0; i < _NUM_TIMER_; i++)
	{
		g_aTimer[i].List = polygon.PolygonDataInitList();	
		g_aTimer[i].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_NumberList0;
		g_aTimer[i].List.Poly.x = SCREEN_WIDTH / CENTER_POS;
		g_aTimer[i].List.Poly.y = F_NULL;
		g_aTimer[i].List.Anim.Count = -1;
		g_aTimer[i].List.Anim.Direction = NUM_NUMBER_W;
		g_aTimer[i].List.Anim.Pattern = NUM_NUMBER;
		g_aTimer[i].List.Anim.AnimationON = B_ON;
		g_aTimer[i].List.TexCut.x = g_aTimer[i].List.TexCut.w = NUMBER_W;
		g_aTimer[i].List.TexCut.y = g_aTimer[i].List.TexCut.h = NUMBER_H;
		g_aTimer[i].List.Poly.w = (float)_MULTIPLIED_(g_aTimer[i].List.TexCut.w, TIMER_SCALE);
		g_aTimer[i].List.Poly.h = (float)_MULTIPLIED_(g_aTimer[i].List.TexCut.h, TIMER_SCALE);
		g_aTimer[i].List.Anim.Frame = 1;
		g_aTimer[i].List.bAffine = B_ON;
		g_aTimer[i].List.Ang.x = g_aTimer[i].List.Scale.x = _DIVISION_(g_aTimer[i].List.Poly.w, CENTER_POS);
		g_aTimer[i].List.Ang.y = g_aTimer[i].List.Scale.y = _DIVISION_(g_aTimer[i].List.Poly.h, CENTER_POS);
		g_aTimer[i].Number.Digit = 2;
		g_aTimer[i].Number.LeftAlignment = B_ON;
		g_aTimer[i].Number.Max = number.Init(g_aTimer[i].Number.Digit);
		g_aTimer[i].Number.Zero = B_ON;
		g_aTimer[i].Tim.Count = 90;
		g_aTimer[i].Comma.Count = OneSecond;
		g_aTimer[i].Tim.End = g_aTimer[i].Comma.End = 0;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CTimer::Uninit(void)
{
	AllMemoryDelete(g_aTimer);
}

//-----------------------------------------------------------------
// 更新処理
void CTimer::Update(bool ControlStart, bool ControlEnd, bool ControlOver)
{
	if (ControlStart != true)
	{
		if (ControlEnd != true && ControlOver != true)
		{
			for (int i = 0; i < _NUM_TIMER_; i++)
			{
				g_aTimer[i] = Timer(g_aTimer[i]);
			}
		}
	}
}

//-----------------------------------------------------------------
// 描画開始
void CTimer::Draw(void)
{
	CPolygon polygon;
	CNumber number;

	int aArea = polygon.StaticAreaStart();

	for (int i = 0; i < _NUM_TIMER_; i++)
	{
		number.Draw(g_aTimer[i].List, g_aTimer[i].Number, g_aTimer[i].Tim.Count);
	}
	polygon.StaticAreaEnd(aArea);
}

//-----------------------------------------------------------------
// タイマーの処理
TIMER CTimer::Timer(TIMER Timer)
{
	if (Timer.Tim.End <= Timer.Tim.Count)
	{
		if (Timer.Comma.Count <= Timer.Comma.End)
		{
			Timer.Tim.Count--;
			Timer.Comma.Count = OneSecond;
			if (Timer.Tim.Count <= 10)
			{
				Timer.List.TexName = LOAD_TEXTURE_LIST::TEXTURE_NumberList2;
			}
		}
		Timer.Comma.Count--;
	}
	
	return Timer;
}

//-----------------------------------------------------------------
// 全パラメーター取得
TIMER CTimer::Get(void)
{
	return g_aTimer[0];
}