//=================================================================
// エネミー[Enemy.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include <time.h>
#include "main.h"
#include "MovePosData.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Distance.h"
#include "Keyboard.h"
#include "Map.h"
#include "LifeProcessing.h"
#include "Explosion.h"
#include "Restriction.h"
#include "Number.h"
#include "Counter.h"

//=================================================================
// デバッグ用カウンタ
#if defined(_DEBUG) || defined(DEBUG)
typedef struct _BULLETNUMCOUNT
{
	int NumCount;
	int LimitCount;
}BULLETNUMCOUNT;
#endif

//=================================================================
// グローバル変数
static ENEMY *g_aEnemy; // エネミーデータ
#if defined(_DEBUG) || defined(DEBUG)
static BULLETNUMCOUNT *g_Debug; // デバッグ用カウンタ
#endif

//-----------------------------------------------------------------
// 初期化
void CEnemy::Init(void)
{
	CPolygon polygon;
	CMovePosData move;
	CMap map;
	CKey key;
	bool bTeanX = false, bTeanY = false;
	int PosX = I_NULL, PosY = I_NULL;
	int Count = I_NULL;
	int s;

	g_aEnemy = MemoryAllocation(g_aEnemy, _NUM_ENEMY_);

	// エネミーの初期化
	for (int i = 0; i < _NUM_ENEMY_; i++)
	{
		g_aEnemy[i].List = polygon.PolygonDataInitList();
		g_aEnemy[i].Mode = I_NULL;
		g_aEnemy[i].ModePursuit = I_NULL;
		g_aEnemy[i].MoveID.Old = I_MAX;
		g_aEnemy[i].MoveID.New = rand() % 8;
		g_aEnemy[i].Frame.End = g_aEnemy[i].Frame.Count = I_NULL;
		g_aEnemy[i] = AISquence(g_aEnemy[i]);
		g_aEnemy[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_ENEMY00;
		g_aEnemy[i].List.Poly.w = (polygon.GetTextureInfo(g_aEnemy[i].List.TexName).Widht / _ENEMY_SCALE_);
		g_aEnemy[i].List.Poly.h = (polygon.GetTextureInfo(g_aEnemy[i].List.TexName).Height / _ENEMY_SCALE_);

		// エネミー設置座標割り出し
		Count = rand() % NUMBERPALETTE_100;
		
		for (s = 0; s < Count; s++)
		{
			rand();
			PosX = rand() % (int)(((int)_MAPDATA_ / CENTER_POS) - (int)_MAPDATA_ / NUMBERPALETTE_10);
			rand();
			PosY = rand() % (int)(((int)_MAPDATA_ / CENTER_POS) - (int)_MAPDATA_ / NUMBERPALETTE_10);
			rand();
		}

		// 反転するか否か
		Count = rand() % NUMBERPALETTE_100;
		for (s = 0; s < Count; s++) { bTeanX = _BOOL_(bTeanX); rand(); }
		Count = rand() % NUMBERPALETTE_100;
		for (s = 0; s < Count; s++) { bTeanY = _BOOL_(bTeanY); rand(); }
		if (bTeanX == true) { PosX = -PosX; }
		if (bTeanY == true) { PosY = -PosY; }
		g_aEnemy[i].List.Poly.x = (float)PosX;
		g_aEnemy[i].List.Poly.y = (float)PosY;
		g_aEnemy[i].List.Ang.x = (g_aEnemy[i].List.Poly.w / CENTER_POS);
		g_aEnemy[i].List.Ang.y = (g_aEnemy[i].List.Poly.h / CENTER_POS);
		g_aEnemy[i].List.Ang.Angle = key.Get().Angle[g_aEnemy[i].MoveID.New];
		g_aEnemy[i].List.bAffine = B_ON;
		g_aEnemy[i].Palameter.Speed.x = _ENEMY_SPEED_;
		g_aEnemy[i].Palameter.Speed.y = F_NULL;
		g_aEnemy[i].Mapdata = move.Get().aMapData;
		g_aEnemy[i].Mapdata.StartX += map.MapCorrect().x;
		g_aEnemy[i].Mapdata.EndX += map.MapCorrect().x;
		g_aEnemy[i].Mapdata.StartY += map.MapCorrect().y;
		g_aEnemy[i].Mapdata.EndY += map.MapCorrect().y;
		g_aEnemy[i].ID = ENEMYID;
		g_aEnemy[i].Palameter.Life.Current = g_aEnemy[i].Palameter.Life.Max = _ENEMY_LIFE_;
		g_aEnemy[i].Palameter.Attack.Power = _ENEMY_ATTAK_;
		g_aEnemy[i].bAttack = false;
		g_aEnemy[i].bEmergence = true;
	}
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug = MemoryAllocation(g_Debug, NUMBERPALETTE_1);
	g_Debug[0].NumCount = I_NULL;
#endif
}

//-----------------------------------------------------------------
// 終了処理
void CEnemy::Uninit(void)
{
	AllMemoryDelete(g_aEnemy);
#if defined(_DEBUG) || defined(DEBUG)
	AllMemoryDelete(g_Debug);
#endif
}

//-----------------------------------------------------------------
// 更新処理
void CEnemy::Update(bool ControlStart, bool ControlEnd)
{
	CMovePosData move;
	CPlayer player;
	CLife life;
	
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount = I_NULL;
#endif
	rand();

	// エネミーの数分処理
	for (int i = 0; i < _NUM_ENEMY_; i++)
	{	
		// ゲームスタート時
		if (ControlStart != true)
		{
			// ゲーム終了時
			if (ControlEnd != true)
			{
				g_aEnemy[i] = AI(g_aEnemy[i], player.Get());
				g_aEnemy[i] = MapMove(g_aEnemy[i], move.Get().aMove);
				g_aEnemy[i] = SetMove(g_aEnemy[i]);
				g_aEnemy[i] = LimitMove(g_aEnemy[i]);
				Attack(g_aEnemy[i], player.Get());

				// エネミー出現時
				if (g_aEnemy[i].bEmergence != false)
				{
					DebugNumCount();

					// エネミーHP判定
					if (g_aEnemy[i].Palameter.Life.Current <= I_NULL)
					{
						CExplosion explosion;
						CCounter counter;
						float fPos = F_NULL;
						int nPos = I_NULL;

						explosion.Set(g_aEnemy[i].List, _COMMON_EXPLOSION_, g_aEnemy[i].bEmergence);
						counter.Set();

						// リポップ座標の生成
						fPos = max(+g_aEnemy[i].Mapdata.StartX, +g_aEnemy[i].Mapdata.EndX);
						nPos = rand() % (int)fPos;
						g_aEnemy[i].List.Poly.x = (float)nPos;
						fPos = max(+g_aEnemy[i].Mapdata.StartY, +g_aEnemy[i].Mapdata.EndY);
						nPos = rand() % (int)fPos;
						g_aEnemy[i].List.Poly.y = (float)nPos;
						g_aEnemy[i].bEmergence = false;
					}
				}
				else if (g_aEnemy[i].bEmergence == false)
				{
					if ((_EME_TIME_ - NUMBERPALETTE_10) <= g_aEnemy[i].EmergenceTime)
					{
						CExplosion explosion;

						explosion.Set(g_aEnemy[i].List, _EMERGENCE_, _BOOL_(g_aEnemy[i].bEmergence));
					}
				}
			}
		}

		// ゲームタイムオーバー時
		if (ControlEnd == true)
		{
			g_aEnemy[i].Palameter.Life.Current = -NUMBERPALETTE_1;

			// 出現時
			if (g_aEnemy[i].bEmergence != false)
			{
				if (g_aEnemy[i].Palameter.Life.Current <= I_NULL)
				{
					CExplosion explosion;

					explosion.Set(g_aEnemy[i].List, _COMMON_EXPLOSION_, g_aEnemy[i].bEmergence);
					g_aEnemy[i].bEmergence = false;
				}
			}
		}
		life.SetEnemyLife(g_aEnemy[i].Palameter.Life, g_aEnemy[i].List.Poly, i);
	}
}

//-----------------------------------------------------------------
// 描画開始
void CEnemy::Draw(void)
{
	CPolygon polygon;
	CRestriction restriction;

	for (int i = 0; i < _NUM_ENEMY_; i++)
	{
		// 画面内のみ描画
		if (restriction.Draw(g_aEnemy[i].List.Poly) == true)
		{
			if (g_aEnemy[i].bEmergence != false)
			{
				polygon.Draw(g_aEnemy[i].List);
			}
		}
	}
}

//-----------------------------------------------------------------
// AI
ENEMY CEnemy::AI(ENEMY aEnemy, PLAYER aPlayer)
{
	// 出現時
	if (aEnemy.bEmergence != false)
	{
		// 頭脳
		aEnemy = AIBrain(aEnemy, aPlayer);

		// 胴体
		aEnemy = AISquence(aEnemy);

		aEnemy.EmergenceTime = I_NULL;
	}
	else if (aEnemy.bEmergence == false)
	{
		aEnemy.MoveID.New = I_MAX;
		aEnemy.EmergenceTime++;
		if (aEnemy.EmergenceTime == _EME_TIME_)
		{
			aEnemy.Palameter.Life.Current = aEnemy.Palameter.Life.Max = _ENEMY_LIFE_;
			aEnemy.bEmergence = true;
		}
	}

	return aEnemy;
}

//-----------------------------------------------------------------
// AI頭脳
ENEMY CEnemy::AIBrain(ENEMY aEnemy, PLAYER aPlayer)
{
	aEnemy.Mode = I_NULL; // 自動索敵固定

	// 出現時
	if (aPlayer.bEmergence != false)
	{
		// エネミーを起点
		if ((_MINUS_(aEnemy.List.Poly.x, _SEARCH_) <= aPlayer.List.Poly.x) && (aPlayer.List.Poly.x <= _PLUS_(aEnemy.List.Poly.x, _SEARCH_)))
		{
			if ((_MINUS_(aEnemy.List.Poly.y, _SEARCH_) <= aPlayer.List.Poly.y) && (aPlayer.List.Poly.y <= _PLUS_(aEnemy.List.Poly.y, _SEARCH_)))
			{
				aEnemy.Mode = 1; // 追尾モード

				// 下
				if (aEnemy.List.Poly.y + _SEARCH_SIZE_00 <= aPlayer.List.Poly.y)
				{
					aEnemy.ModePursuit = NUMBERPALETTE_2;
				}

				// 上
				if (aEnemy.List.Poly.y - _SEARCH_SIZE_00 >= aPlayer.List.Poly.y)
				{
					aEnemy.ModePursuit = NUMBERPALETTE_0;
				}

				// 右
				if (aEnemy.List.Poly.x + _SEARCH_SIZE_00 <= aPlayer.List.Poly.x)
				{
					aEnemy.ModePursuit = NUMBERPALETTE_3;
				}

				// 左
				if (aEnemy.List.Poly.x - _SEARCH_SIZE_00 >= aPlayer.List.Poly.x)
				{
					aEnemy.ModePursuit = NUMBERPALETTE_1;
				}

				// 右上
				if ((aEnemy.List.Poly.y >= aPlayer.List.Poly.y + _SEARCH_SIZE_01) && (aEnemy.List.Poly.x <= aPlayer.List.Poly.x - _SEARCH_SIZE_01))
				{
					aEnemy.ModePursuit = NUMBERPALETTE_4;
				}

				// 左上
				if ((aEnemy.List.Poly.y >= aPlayer.List.Poly.y + _SEARCH_SIZE_01) && (aEnemy.List.Poly.x >= aPlayer.List.Poly.x + _SEARCH_SIZE_01))
				{
					aEnemy.ModePursuit = NUMBERPALETTE_5;
				}

				// 右下
				if ((aEnemy.List.Poly.y <= aPlayer.List.Poly.y - _SEARCH_SIZE_01) && (aEnemy.List.Poly.x <= aPlayer.List.Poly.x - _SEARCH_SIZE_01))
				{
					aEnemy.ModePursuit = NUMBERPALETTE_6;
				}

				// 左下
				if ((aEnemy.List.Poly.y <= aPlayer.List.Poly.y - _SEARCH_SIZE_01) && (aEnemy.List.Poly.x >= aPlayer.List.Poly.x + _SEARCH_SIZE_01))
				{
					aEnemy.ModePursuit = NUMBERPALETTE_7;
				}
			}
		}
	}
	
	return aEnemy;
}

//-----------------------------------------------------------------
// AI動き制御
ENEMY CEnemy::AISquence(ENEMY aEnemy)
{
	// モード別処理
	switch (aEnemy.Mode)
	{
	case 0: // 自動索敵シーケンス
		aEnemy.Frame.Count++;
		if (aEnemy.Frame.End <= aEnemy.Frame.Count)
		{
			CKey key; key;

			aEnemy.Frame.End = aEnemy.Frame.Count = I_NULL;
			aEnemy.MoveID.Old = I_MAX; // 初期化
			aEnemy.MoveID.New = rand() % (int)_DIVISION_((int)sizeof(key.Get().Angle), _BIT_);
			aEnemy.Frame.End = rand() % NUMBERPALETTE_1000;
		}
		aEnemy.bAttack = false;
		break;
	case 1: // 追尾モード
		aEnemy.MoveID.Old = I_MAX; // 初期化
		aEnemy.Frame.End = I_NULL; // フレーム制御初期化
		aEnemy.MoveID.New = aEnemy.ModePursuit;
		aEnemy.bAttack = true;
		break;
	default:
		break;
	}

	return aEnemy;
}

//-----------------------------------------------------------------
// 移動に必要な値をセット
ENEMY CEnemy::SetMove(ENEMY aEnemy)
{
	if (aEnemy.MoveID.New != aEnemy.MoveID.Old)
	{
		CKey key;

		// 動作に必要な値
		switch (aEnemy.MoveID.New)
		{
		case 0: // 上
			aEnemy.Palameter.Speed.y = -_ENEMY_SPEED_;
			aEnemy.Palameter.Speed.x = F_NULL;
			break;
		case 1: // 左
			aEnemy.Palameter.Speed.y = F_NULL;
			aEnemy.Palameter.Speed.x = -_ENEMY_SPEED_;
			break;
		case 2: // 下
			aEnemy.Palameter.Speed.y = _ENEMY_SPEED_;
			aEnemy.Palameter.Speed.x = F_NULL;
			break;
		case 3: // 右
			aEnemy.Palameter.Speed.y = F_NULL;
			aEnemy.Palameter.Speed.x = _ENEMY_SPEED_;
			break;
		case 4: // 右上
			aEnemy.Palameter.Speed.y = -(_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			aEnemy.Palameter.Speed.x = (_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			break;
		case 5: // 左上
			aEnemy.Palameter.Speed.y = -(_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			aEnemy.Palameter.Speed.x = -(_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			break;
		case 6: // 右下
			aEnemy.Palameter.Speed.y = (_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			aEnemy.Palameter.Speed.x = (_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			break;
		case 7: // 左下
			aEnemy.Palameter.Speed.y = (_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			aEnemy.Palameter.Speed.x = -(_ENEMY_SPEED_ / _ENEMY_SPPED_Lif_);
			break;
		default: // 完全停止
			aEnemy.Palameter.Speed.y = F_NULL;
			aEnemy.Palameter.Speed.x = F_NULL;
			aEnemy.MoveID.New = aEnemy.MoveID.Old;
			break;
		}
		aEnemy.List.Ang.Angle = key.Get().Angle[aEnemy.MoveID.New];
		aEnemy.MoveID.Old = aEnemy.MoveID.New;
	}
	
	return aEnemy;
}

//-----------------------------------------------------------------
// エネミーの動き制御
ENEMY CEnemy::LimitMove(ENEMY aEnemy)
{
	aEnemy.List.Poly.x += aEnemy.Palameter.Speed.x;
	aEnemy.List.Poly.y += aEnemy.Palameter.Speed.y;

	// 反転処理
	switch (aEnemy.MoveID.New)
	{
	case 0: // 上
		if (aEnemy.List.Poly.y <= aEnemy.Mapdata.StartY)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_2;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	case 1: // 左
		if (aEnemy.List.Poly.x <= aEnemy.Mapdata.StartX)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_3;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		break;
	case 2: // 下
		if (aEnemy.Mapdata.EndY <= (aEnemy.List.Poly.y + aEnemy.List.Poly.h))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_0;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	case 3: // 右
		if (aEnemy.Mapdata.EndX <= (aEnemy.List.Poly.x + aEnemy.List.Poly.w))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_1;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		break;
	case 4: // 右上
		// 右
		if (aEnemy.Mapdata.EndX <= (aEnemy.List.Poly.x + aEnemy.List.Poly.w))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_5;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		// 上
		if (aEnemy.List.Poly.y <= aEnemy.Mapdata.StartY)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_6;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	case 5: // 左上
		// 左
		if (aEnemy.List.Poly.x <= aEnemy.Mapdata.StartX)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_4;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		// 上
		if (aEnemy.List.Poly.y <= aEnemy.Mapdata.StartY)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_7;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	case 6: // 右下
		// 右
		if (aEnemy.Mapdata.EndX <= (aEnemy.List.Poly.x + aEnemy.List.Poly.w))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_7;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		// 下
		if (aEnemy.Mapdata.EndY <= (aEnemy.List.Poly.y + aEnemy.List.Poly.h))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_4;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	case 7: // 左下
		// 左
		if (aEnemy.List.Poly.x <= aEnemy.Mapdata.StartX)
		{
			aEnemy.MoveID.New = NUMBERPALETTE_6;
			aEnemy.List.Poly.x -= aEnemy.Palameter.Speed.x;
		}
		// 下
		if (aEnemy.Mapdata.EndY <= (aEnemy.List.Poly.y + aEnemy.List.Poly.h))
		{
			aEnemy.MoveID.New = NUMBERPALETTE_5;
			aEnemy.List.Poly.y -= aEnemy.Palameter.Speed.y;
		}
		break;
	default:
		break;
	}
	
	return aEnemy;
}

//-----------------------------------------------------------------
// マップ上での動き
ENEMY CEnemy::MapMove(ENEMY aEnemy, SPEED PosData)
{
	aEnemy.Mapdata.StartX -= PosData.x;
	aEnemy.Mapdata.EndX -= PosData.x;
	aEnemy.Mapdata.StartY -= PosData.y;
	aEnemy.Mapdata.EndY -= PosData.y;
	aEnemy.List.Poly.x -= PosData.x;
	aEnemy.List.Poly.y -= PosData.y;

	return aEnemy;
}

//-----------------------------------------------------------------
// エネミーの攻撃
void CEnemy::Attack(ENEMY aEnemy, PLAYER aPlayer)
{
	// 出現時
	if (aEnemy.bEmergence != false)
	{
		// 攻撃ヒット時
		if (aEnemy.bAttack == true)
		{
			CBullet bullet;
			CDistance distance;
			float fDistance = distance.Distance2D(aEnemy.List.Poly, aPlayer.List.Poly); // プレイヤーとの距離割り出し

			// 射程内にプレイヤーがいるかいないか
			if (_ENEMY_RANGE_ <= fDistance && fDistance <= _SEARCH_)
			{
				bullet.Create(aEnemy.List.Poly, aPlayer.List.Poly, aEnemy.ID, aEnemy.Palameter.Attack);
			}
		}
	}
}

//-----------------------------------------------------------------
// ダメージ渡し
void CEnemy::Damage(bool Hit, ATTACK Attack, int NumEnemy)
{
	// ヒット時
	if (Hit)
	{
		g_aEnemy[NumEnemy].Palameter.Life.Current -= Attack.Power;
	}
}

//-----------------------------------------------------------------
// 全パラメーター取得
ENEMY CEnemy::Get(int NumEnemy)
{
	return g_aEnemy[NumEnemy];
}

//-----------------------------------------------------------------
// エネミーの出現数取得
int CEnemy::GetNumRelease(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	return g_Debug[0].NumCount;
#else
	return 0;
#endif
}

void CEnemy::DebugNumCount(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount++;
#endif
}