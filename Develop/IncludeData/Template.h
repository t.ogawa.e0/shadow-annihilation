//=================================================================
// テンプレートヘッダ[Template.h]
// author: tatuya ogawa
//=================================================================

#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>

template <typename T, typename S> inline T* MemoryAllocation(T*& p, S Size);
template <typename T> inline void MemoryDelete(T*& p);
template <typename T> inline void AllMemoryDelete(T*& p);
template <typename T> inline void MemoryRelease(T*& p);
template <typename T> inline T sqr(T x);

//メモリ解放 delete
// delete p;
template <typename T> inline void MemoryDelete(T*& p)
{
	if (p != NULL)
	{
		delete (p);
		(p) = NULL;
	}
}

// メモリ解放 delete[]
// delete[] p;
template <typename T> inline void AllMemoryDelete(T*& p)
{
	if ((*&p) != NULL)
	{
		delete[](*&p);
		(*&p) = NULL;
	}
}

// メモリ内のデータ破棄
// p->Release();
template <typename T> inline void MemoryRelease(T*& p)
{
	if ((*& p) != NULL)
	{
		(*& p)->Release();
		(*& p) = NULL;
	}
}

// メモリ確保
// *&p = new T[Size];
template <typename T, typename S> inline T* MemoryAllocation(T*& p, S Size)
{
	if ((*& p) == NULL)
	{
		(*& p) = new T[(Size)];
	}

	return (*& p);
}

//math 数学関数
//-------------------------------------------
template <typename T> inline T sqr(T x)
{
	return (x) * (x);
}

#endif // !_TEMPLATE_H_
