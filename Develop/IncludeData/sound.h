//=============================================================================
//
// サウンド処理 [sound.h]
// 
//=============================================================================
#ifndef _SOUND_H_
#define _SOUND_H_

#include <Windows.h>

//=================================================================
// マクロ定義
#ifndef I_MAX
#define I_MAX (2147483647)
#endif // !I_MAX

// エラーウィンドウ
#ifndef ErrorMessage
#define ErrorMessage(c) MessageBox(NULL, (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

#ifndef _BIT_
#define _BIT_ (4)
#endif // !_BIT_

#ifndef STRING_SIZE
#define STRING_SIZE (256) // 文字列のサイズ	
#endif // !STRING_SIZE

//=================================================================
// 構造体
// サウンドの音量管理
typedef struct _BGMSVolume
{
	float BGM; // 1.0f最大 無音0.0f
	float SE; // 1.0f最大 無音0.0f
}BGMSVolume;

//=================================================================
// プロトタイプ宣言
HRESULT InitSound(int Area, HWND hWnd);
void UninitSound(void);
HRESULT PlaySound(int label);
void StopLimitedSound(int label);
void StopAllSound(void);
BGMSVolume ConfigVolume(bool BGM, bool SE, BGMSVolume Volume);

// 静的な領域確保
// 戻り値 : 失敗=false 成功=true
bool InitSoundStaticArea(int Area);

// 静的な領域に切り替え
// 戻り値 : 前回の領域
int SoundStaticAreaStart(void);

// 静的な領域終了時の最後に記述
// Area : 前回の領域
void SoundStaticAreaEnd(int Area);

// サウンド領域の更新
bool UpdateSoundDynamicArea(int Area);

#endif // !_SOUND_H_
