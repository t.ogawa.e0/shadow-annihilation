◆DirectX2Dゲーム開発用関数リスト◆

◆作成者
　Twitter:@e4ka_

◆内容物
　・DirectX2DOriginalFunction.cpp
　・DirectX2DOriginalFunction.h
　・2DTextureInformationManagement.h

-------------------------------------------------------------

Win32API環境上でDirectXを使用した2Dゲーム開発を少しやりやすくしたソースコードです。

プロジェクトフォルダー内にIncludeDataフォルダーをそのままぶち込んでください。

テクスチャを読み込む際にTexture_Data.binファイルが必要になります。
.binデータを生成するためのツールはConvertFileFormatフォルダ内に入っています。
Readmeをよく読んで使用してください。

-------------------------------------------------------------

