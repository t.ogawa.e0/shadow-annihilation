//=================================================================
// DirectX2D用関数[DirectX2D.cpp]
// author: tatuya ogawa
//=================================================================

#include "DirectX2D.h"

//=================================================================
// グローバル変数
static LPDIRECT3DDEVICE9		 g_pD3DDevice = NULL;
static LPDIRECT3D9				 g_pD3D;			// ダイレクト3Dインターフェース
static LPDIRECT3DVERTEXBUFFER9	 g_pVertexBuffer;	// バッファ
static LINK_DATA				*g_aLinkData;		// リンクポインタ
static TEXTURE					*g_pTexture;		// テクスチャポインタ
static TEXTURE_DATA				*g_aTexture;		// テクスチャ情報の格納
static int						 g_Area;			// 領域番号
static int						 g_OldArea;			// 古い領域番号
static int						 g_StaticArea;		// 静的なデータ領域の番号
static int						 g_NumLink;			// リンクの数
static bool						 g_bStatic;			// 静的な領域有効状態
#if defined(_DEBUG) || defined(DEBUG)
static int						 g_FontPosX;		// 表示位置X
static int						 g_FontPosY;		// 表示位置Y
static LPD3DXFONT				 g_pFont;			// フォント
static DEBUG_VERTEX				*g_pV;				// 判定用
static int						 g_Width;			// 幅
static int						 g_Height;			// 高さ
static D3DCOLOR					 g_Color;			// カラー
#endif

//-----------------------------------------------------------------
// DirectXの機能を使用するための初期設定
// 返り値 : 生成又は取得の成功か失敗
// hInstance インスタンスハンドル
// hWnd ウインドウハンドル
// bWindow ウィンドウモード TRUEで非最大化 FALSEで最大化
bool CDevice::Init(HINSTANCE hInstance, HWND hWnd, BOOL bWindow, int Width, int Height)
{
	static bool bLock = false; // lock

	if (bLock == false)
	{
		D3DPRESENT_PARAMETERS d3dpp;
		D3DDISPLAYMODE d3dpm;
		hInstance = hInstance;
#if defined(_DEBUG) || defined(DEBUG)
		g_Width = Width;
		g_Height = Height;
#endif

		// Direct3Dオブジェクトの作成
		g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		if (g_pD3D == NULL)
		{
			return false;
		}

		// 現在のディスプレイモードを取得
		if (FAILED(g_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3dpm)))
		{
			return false;
		}

		// デバイスのプレゼンテーションパラメータ(デバイスの設定値)
		{
			ZeroMemory(&d3dpp, sizeof(d3dpp));
			d3dpp.BackBufferWidth = Width;								// バックバッファの幅
			d3dpp.BackBufferHeight = Height;							// バックバッファの高さ
			d3dpp.BackBufferFormat = d3dpm.Format;						// バックバッファフォーマット
			d3dpp.BackBufferCount = 1;									// バッファの数
			d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;					// 垂直？
			d3dpp.EnableAutoDepthStencil = TRUE;						// 3Dの描画に必要
			d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
			d3dpp.Windowed = bWindow;									// ウィンドウモード切り替え
			d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;	// フルスクリーンのとき、リフレッシュレートを変えられる
			d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
		}

		// デバイスオブジェクトを生成
		// [デバイス作成制御]<描画>と<頂点処理>
		if (FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &g_pD3DDevice)))
		{
			ErrorMessage("デバイスの生成に失敗しました。");
			return false;
		}

		// レンダーステートの設定
		{// この3つでα値が使える
			g_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを許可
			g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの設定
			g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// α
		}

		// テクスチャステージステートの設定
		{
			g_pD3DDevice->SetTextureStageState(0/*テクスチャステージ番号*/, D3DTSS_ALPHAOP/*演算子*/, D3DTOP_MODULATE/*掛け算*/);
			g_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			g_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
		}

		// サンプラーステートの設定(UV値を変えると増えるようになる)
		{
			// WRAP...		反復する
			// CLAMP...　	引き伸ばす
			// MIRROR...　	鏡状態
			g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
			g_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		}

		// フィルタリング
		{
			// 画像を滑らかにする
			g_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
			g_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
			g_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
		}
		g_Area = I_MAX;
		g_OldArea = 0;
		g_StaticArea = I_MAX;
		g_NumLink = 0;
		bLock = true;
		g_bStatic = false;
	}

	return true;
}

//-----------------------------------------------------------------
// デバイス解放
void CDevice::Uninit(void)
{
	UninitDevice();
	UninitDirect3DObject();
}

// デバイスの開放
void CDevice::UninitDevice(void)
{
	if (g_pD3DDevice != NULL)
	{
		g_pD3DDevice->Release();
		g_pD3DDevice = NULL;
	}
}

// Direct3Dオブジェクトの開放
void CDevice::UninitDirect3DObject(void)
{
	if (g_pD3D != NULL)
	{
		g_pD3D->Release();
		g_pD3D = NULL;
	}
}

//-----------------------------------------------------------------
// デバイスの習得
LPDIRECT3DDEVICE9 CDevice::GetD3DDevice(void)
{
	return g_pD3DDevice;
}

//-----------------------------------------------------------------
// ポリゴンの初期化
// 戻り値 : 失敗=false 成功=true
bool CPolygon::Init(int Area)
{
	static bool bLock = false; // 鍵

	// 鍵がかかっていない時
	if (bLock == false)
	{
		HRESULT hr;
		CDebug debug;

		// nullではないとき
		if (g_pD3DDevice == NULL) { return false; }
		g_OldArea = g_Area = Area; // 取得領域の記憶

		// 領域の確保
		if (!UpdateDynamicArea(Area)) { return false; }
		hr = g_pD3DDevice->CreateVertexBuffer(sizeof(VERTEX_2D) * _BIT_, D3DUSAGE_WRITEONLY, FVF_VERTEX_2D, D3DPOOL_MANAGED, &g_pVertexBuffer, NULL);
		debug.Init(); // デバッグフォントの初期化
		bLock = true; // 鍵をかける
		if (FAILED(hr))
		{
			ErrorMessage("頂点バッファが作れませんでした。");
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------
// 静的な領域確保
// 戻り値 : 失敗=false 成功=true
bool CPolygon::InitStaticArea(int Area)
{
	static bool bLock = false; // 鍵

	// 鍵がかかっていない時
	if (bLock == false)
	{
		// 読み込み領域が異なる場合
		if (g_Area != Area)
		{
			int nOldArea = g_OldArea;
			int nArea = g_Area;

			g_Area = g_OldArea = Area;

			// 領域の確保
			if (!UpdateDynamicArea(Area, true)) { return false; }
			g_OldArea = nOldArea;
			g_Area = nArea;
			g_StaticArea = Area;
			bLock = true; // 鍵をかける
			g_bStatic = true;
		}
	}

	return true;
}

//-----------------------------------------------------------------
// 静的な領域に切り替え
// 戻り値 : 前回の領域
int CPolygon::StaticAreaStart(void)
{
	static int nAreaData; // 読み込み領域の格納用

	// 上限値以外のとき
	if (g_StaticArea != I_MAX)
	{
		// 静的な領域と現在使用している領域が異なる場合
		if (g_StaticArea != g_Area)
		{
			nAreaData = g_Area;
			g_Area = g_StaticArea;
		}
	}
	else
	{
		nAreaData = g_Area;
	}

	return nAreaData;
}

//-----------------------------------------------------------------
// 静的な領域終了時の最後に記述
// Area : 前回の領域
void CPolygon::StaticAreaEnd(int Area)
{
	// 領域が上限値じゃない限り
	if (g_StaticArea != I_MAX)
	{
		// 静的なエリアと現在使用している領域が一致時
		if (g_StaticArea == g_Area) { g_Area = Area; }
	}
}

//-----------------------------------------------------------------
// メモリ確保
bool CPolygon::GetMemory(int Area, int NumTex)
{
	// テクスチャが0でない限りメモリを確保する
	if (!(NumTex == 0))
	{
		if (g_aTexture == NULL)
		{
			g_aTexture = new TEXTURE_DATA[NumTex];
		}
		if (g_pTexture[Area].Widht == NULL)
		{
			g_pTexture[Area].Widht = new int[NumTex];
		}
		if (g_pTexture[Area].Height == NULL)
		{
			g_pTexture[Area].Height = new int[NumTex];
		}
		if (g_pTexture[Area].Texture == NULL)
		{
			g_pTexture[Area].Texture = new LPDIRECT3DTEXTURE9[NumTex];
		}
		return true;
	}

	return false;
}

//-----------------------------------------------------------------
// 動的な領域確保
void CPolygon::GetDynamicArea(void)
{
	static bool bLock = false; // 鍵

	// lock中ではない場合
	if (bLock == false)
	{
		// テクスチャ構造体のメモリ確保
		if (g_pTexture == NULL)
		{
			g_pTexture = new TEXTURE[g_NumLink];

			// テクスチャ領域をNULLで初期化
			for (int i = 0; i < g_NumLink; i++)
			{
				g_pTexture[i].Widht = NULL;
				g_pTexture[i].Height = NULL;
				g_pTexture[i].Texture = NULL;
			}
		}
		bLock = true; // 鍵をかける
	}
}

//-----------------------------------------------------------------
// 読み取り用データの開放
void CPolygon::FreeReadData(void)
{
	if (g_aTexture != NULL)
	{
		delete[] g_aTexture;
		g_aTexture = NULL;
	}
	if (g_aLinkData != NULL)
	{
		delete[] g_aLinkData;
		g_aLinkData = NULL;
	}
}

//-----------------------------------------------------------------
// テクスチャの破棄
void CPolygon::ReleaseTexture(int Area, int NumTex)
{
	// テクスチャのデータがある限り行う
	for (int i = 0; i < NumTex; i++)
	{
		if (g_pTexture[Area].Texture[i] != NULL)
		{
			g_pTexture[Area].Texture[i]->Release();
			g_pTexture[Area].Texture[i] = NULL;
		}
	}
}

//-----------------------------------------------------------------
// テクスチャのデータを開放
void CPolygon::FreeTextureData(int Area)
{
	if (g_pTexture[Area].Texture != NULL)
	{
		delete[] g_pTexture[Area].Texture;
		g_pTexture[Area].Texture = NULL;
	}
	if (g_pTexture[Area].Widht != NULL)
	{
		delete[] g_pTexture[Area].Widht;
		g_pTexture[Area].Widht = NULL;
	}
	if (g_pTexture[Area].Height != NULL)
	{
		delete[] g_pTexture[Area].Height;
		g_pTexture[Area].Height = NULL;
	}
	if (g_pTexture[Area].NumTexture != NULL)
	{
		g_pTexture[Area].NumTexture = NULL;
	}
}

//-----------------------------------------------------------------
// テクスチャのデータ数取得
bool CPolygon::GetNumTextureData(void)
{
	FILE* pFile;

	// リンク読み込み
	pFile = fopen(TEXTURE_LINK, "rb");
	if (pFile)
	{
		// テクスチャデータの数の読み取り
		fread(&g_NumLink, _BIT_, 1, pFile);
		if (g_aLinkData == NULL)
		{
			g_aLinkData = new LINK_DATA[g_NumLink];
		}
		fread(g_aLinkData, sizeof(LINK_DATA), g_NumLink, pFile);
		fclose(pFile);
	}
	else
	{
		ErrorMessage("リンク情報の取得に失敗しました。");
		return false;
	}

	return true;
}

//-----------------------------------------------------------------
// テクスチャの呼び出し
bool CPolygon::CallTexture(int Area)
{
	FILE* pFile;

	pFile = fopen(g_aLinkData[Area].LinkName, "rb");
	if (pFile)
	{
		GetDynamicArea();

		// テクスチャデータの数の読み取り
		fread(&g_pTexture[Area].NumTexture, _BIT_, 1, pFile);
		if (!GetMemory(Area, g_pTexture[Area].NumTexture))
		{
			return false;
		}
		fread(g_aTexture, sizeof(TEXTURE_DATA), g_pTexture[Area].NumTexture, pFile);
		fclose(pFile);
	}
	else
	{
		ErrorMessage("テクスチャのパスの読み込みに失敗しました。");
		return false;
	}

	return true;
}

//-----------------------------------------------------------------
// テクスチャの読み込み
bool CPolygon::LoadTexture(int Area, int NumTex)
{
	HRESULT hr;
	D3DXIMAGE_INFO dil; // 画像データの管理

	for (int i = 0; i < NumTex; i++)
	{
		// 画像データの格納
		D3DXGetImageInfoFromFile(g_aTexture[i].FileName, &dil);
		g_pTexture[Area].Height[i] = dil.Height;
		g_pTexture[Area].Widht[i] = dil.Width;

		// 画像情報の格納
		hr = D3DXCreateTextureFromFile(g_pD3DDevice, g_aTexture[i].FileName, &g_pTexture[Area].Texture[i]);
		if (FAILED(hr))
		{
			ErrorMessage("テクスチャの読み込み失敗しました。");
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------
// 次の領域の確保
// Area : 次の領域
// 戻り値 : 失敗=false 成功=true
bool CPolygon::UpdateDynamicArea(int Area, bool bStatic)
{
	static bool bOpen = false; // 鍵

	// 領域重複回避
	if (g_StaticArea != Area)
	{
		// 一度読み込まれたとき
		if (bOpen == true && bStatic == false)
		{
			// 切り替え時の初期化
			ReleaseTexture(g_OldArea, g_pTexture[g_OldArea].NumTexture);
			FreeTextureData(g_OldArea);
		}
		else
		{
			bOpen = true; // 初期時に鍵を開く
		}

		// 切り替え時のデータ取得
		if (g_pD3DDevice == NULL) { return false; }
		if (!GetNumTextureData()) { return false; }
		if (!CallTexture(Area)) { return false; }
		if (!LoadTexture(Area, g_pTexture[Area].NumTexture)) { return false; }
		g_OldArea = g_Area = Area;
		FreeReadData();
	}

	return true;
}

//-----------------------------------------------------------------
// ポリゴンの終了処理
void CPolygon::Uninit(void)
{
	static bool bLock = false; // 鍵

	// 鍵がかかっていないとき
	if (bLock == false)
	{
		CDebug debug;

		debug.Uninit();

		// バッファ解放
		if (g_pVertexBuffer != NULL)
		{
			g_pVertexBuffer->Release();
			g_pVertexBuffer = NULL;
		}
		
		if (g_bStatic == true)
		{
			ReleaseTexture(g_StaticArea, g_pTexture[g_StaticArea].NumTexture);
			FreeTextureData(g_StaticArea);
		}

		ReleaseTexture(g_Area, g_pTexture[g_Area].NumTexture);
		FreeTextureData(g_Area);

		// テクスチャのデータ領域解放
		if (g_pTexture != NULL)
		{
			delete[] g_pTexture;
			g_pTexture = NULL;
		}
		g_NumLink = NULL;
		bLock = true; // 鍵をかける
	}
}

//-----------------------------------------------------------------
// 読み込んだテクスチャの幅と高さの取得
TEXTURE_PLAM CPolygon::GetTextureInfo(int TexNum)
{
	TEXTURE_PLAM aTexture;

	aTexture.Widht = g_pTexture[g_Area].Widht[TexNum];
	aTexture.Height = g_pTexture[g_Area].Height[TexNum];

	return aTexture;
}

//-----------------------------------------------------------------
// ポリゴンの描画
// Poly : ポリゴンの構造体
// Affine : 角度変更ON,OFF
void CPolygon::DrawPolygon(POLYGON Polygon)
{
	if (g_pD3DDevice == NULL) { return; }

	if (Polygon.bAffine == B_ON)
	{
		CreateVertexAngle(Polygon);
	}
	else
	{
		CreateVertex(Polygon);
	}

	// 0 初めから
	// g_pVertexBuffer つなぐもの
	// 0 先頭から
	// サイズ
	g_pD3DDevice->SetStreamSource(0, g_pVertexBuffer, 0, sizeof(VERTEX_2D));	// パイプライン

	// FVF （今から使用する頂点情報）の設定
	g_pD3DDevice->SetFVF(FVF_VERTEX_2D);

	// テクスチャのセット
	g_pD3DDevice->SetTexture(0, g_pTexture[g_Area].Texture[Polygon.TexName]);

	// ポリゴンを呼ぶ
	// D3DPT_TRIANGLESTRIP 三角
	// 0 先頭から
	// 2 いくつ使うか
	g_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
}

//-----------------------------------------------------------------
// 頂点生成関数
// ポリゴンの描画と引数は同じ
void CPolygon::CreateVertex(POLYGON Polygon)
{
	// 頂点バッファのロック
	VERTEX_2D* pPseudo;

	// 左上
	float u0 = _DIVISION_((float)Polygon.TexCut.x, g_pTexture[g_Area].Widht[Polygon.TexName]);
	float v0 = _DIVISION_((float)Polygon.TexCut.y, g_pTexture[g_Area].Height[Polygon.TexName]);

	// 左下
	float u1 = _DIVISION_((float)_PLUS_(Polygon.TexCut.x, Polygon.TexCut.w), g_pTexture[g_Area].Widht[Polygon.TexName]);
	float v1 = _DIVISION_((float)_PLUS_(Polygon.TexCut.y, Polygon.TexCut.h), g_pTexture[g_Area].Height[Polygon.TexName]);

	// 0 先頭
	// 0で全部　正しい=sizeof(VERTEX_2D) * 4 
	// (void**)&pPseudo 擬似アドレス
	// D3DLOCK_DISCARD 0でもおｋ　ロックをかける
	g_pVertexBuffer->Lock(0, sizeof(VERTEX_2D) * _BIT_, (void**)&pPseudo, D3DLOCK_DISCARD);

	pPseudo[0].Pos = D3DXVECTOR4(_D3DXVECTOR4FX_(Polygon.Poly.x, F_NULL, CORRECTION, Polygon.Scale.w), _D3DXVECTOR4FY_(Polygon.Poly.y, F_NULL, CORRECTION, Polygon.Scale.h), F_DEF, F_DEF);
	pPseudo[1].Pos = D3DXVECTOR4(_D3DXVECTOR4FX_(Polygon.Poly.x, Polygon.Poly.w, CORRECTION, Polygon.Scale.w), _D3DXVECTOR4FY_(Polygon.Poly.y, F_NULL, CORRECTION, Polygon.Scale.h), F_DEF, F_DEF);
	pPseudo[2].Pos = D3DXVECTOR4(_D3DXVECTOR4FX_(Polygon.Poly.x, F_NULL, CORRECTION, Polygon.Scale.w), _D3DXVECTOR4FY_(Polygon.Poly.y, Polygon.Poly.h, CORRECTION, Polygon.Scale.h), F_DEF, F_DEF);
	pPseudo[3].Pos = D3DXVECTOR4(_D3DXVECTOR4FX_(Polygon.Poly.x, Polygon.Poly.w, CORRECTION, Polygon.Scale.w), _D3DXVECTOR4FY_(Polygon.Poly.y, Polygon.Poly.h, CORRECTION, Polygon.Scale.h), F_DEF, F_DEF);

	pPseudo[0].Color = pPseudo[1].Color = pPseudo[2].Color = pPseudo[3].Color = D3DCOLOR_RGBA(Polygon.Color.r, Polygon.Color.g, Polygon.Color.b, Polygon.Color.a);

	pPseudo[0].Texcoord = D3DXVECTOR2(u0, v0);
	pPseudo[1].Texcoord = D3DXVECTOR2(u1, v0);
	pPseudo[2].Texcoord = D3DXVECTOR2(u0, v1);
	pPseudo[3].Texcoord = D3DXVECTOR2(u1, v1);

	g_pVertexBuffer->Unlock();	// ロック解除
}

//-----------------------------------------------------------------
// 角度付き頂点生成関数 
// ポリゴンの描画と引数は同じ
void CPolygon::CreateVertexAngle(POLYGON Polygon)
{
	// 頂点バッファのロック
	VERTEX_2D* pPseudo;

	// 左上
	float u0 = _DIVISION_((float)Polygon.TexCut.x, g_pTexture[g_Area].Widht[Polygon.TexName]);
	float v0 = _DIVISION_((float)Polygon.TexCut.y, g_pTexture[g_Area].Height[Polygon.TexName]);

	// 左下
	float u1 = _DIVISION_((float)_PLUS_(Polygon.TexCut.x, Polygon.TexCut.w), g_pTexture[g_Area].Widht[Polygon.TexName]);
	float v1 = _DIVISION_((float)_PLUS_(Polygon.TexCut.y, Polygon.TexCut.h), g_pTexture[g_Area].Height[Polygon.TexName]);

	// 中心座標割り出し
	float APolyX	 = _PLUS_(_MULTIPLIED_(-Polygon.Scale.x, Polygon.Scale.w), Polygon.Scale.x);
	float APolyX_W = _PLUS_(_MULTIPLIED_(_MINUS_(Polygon.Poly.w, Polygon.Scale.x), Polygon.Scale.w), Polygon.Scale.x);
	float APolyY	 = _PLUS_(_MULTIPLIED_(-Polygon.Scale.y, Polygon.Scale.h), Polygon.Scale.y);
	float APolyY_H = _PLUS_(_MULTIPLIED_(_MINUS_(Polygon.Poly.h, Polygon.Scale.y), Polygon.Scale.h), Polygon.Scale.y);

	// ずらした分修正
	APolyX	 -= Polygon.Ang.x;
	APolyX_W -= Polygon.Ang.x;
	APolyY	 -= Polygon.Ang.y;
	APolyY_H -= Polygon.Ang.y;

	Polygon.Poly.x += Polygon.Ang.x;
	Polygon.Poly.y += Polygon.Ang.y;

	g_pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);

	pPseudo[0].Pos = D3DXVECTOR4(_D3DXVECTOR4AFX_(Polygon.Poly.x, APolyX, Polygon.Ang.Angle, APolyY, CORRECTION), _D3DXVECTOR4AFY_(Polygon.Poly.y, APolyX, Polygon.Ang.Angle, APolyY, CORRECTION), F_DEF, F_DEF);
	pPseudo[1].Pos = D3DXVECTOR4(_D3DXVECTOR4AFX_(Polygon.Poly.x, APolyX_W, Polygon.Ang.Angle, APolyY, CORRECTION), _D3DXVECTOR4AFY_(Polygon.Poly.y, APolyX_W, Polygon.Ang.Angle, APolyY, CORRECTION), F_DEF, F_DEF);
	pPseudo[2].Pos = D3DXVECTOR4(_D3DXVECTOR4AFX_(Polygon.Poly.x, APolyX, Polygon.Ang.Angle, APolyY_H, CORRECTION), _D3DXVECTOR4AFY_(Polygon.Poly.y, APolyX, Polygon.Ang.Angle, APolyY_H, CORRECTION), F_DEF, F_DEF);
	pPseudo[3].Pos = D3DXVECTOR4(_D3DXVECTOR4AFX_(Polygon.Poly.x, APolyX_W, Polygon.Ang.Angle, APolyY_H, CORRECTION), _D3DXVECTOR4AFY_(Polygon.Poly.y, APolyX_W, Polygon.Ang.Angle, APolyY_H, CORRECTION), F_DEF, F_DEF);

	pPseudo[0].Color = pPseudo[1].Color = pPseudo[2].Color = pPseudo[3].Color = D3DCOLOR_RGBA(Polygon.Color.r, Polygon.Color.g, Polygon.Color.b, Polygon.Color.a);

	pPseudo[0].Texcoord = D3DXVECTOR2(u0, v0);
	pPseudo[1].Texcoord = D3DXVECTOR2(u1, v0);
	pPseudo[2].Texcoord = D3DXVECTOR2(u0, v1);
	pPseudo[3].Texcoord = D3DXVECTOR2(u1, v1);

	g_pVertexBuffer->Unlock();	// ロック解除
}

//-----------------------------------------------------------------
// テクスチャ切り取り設定
// TexCut : テクスチャ切り取り用構造体
INT_List CPolygon::SetTexturCut(INT_List TexCut, int TexNum)
{
	if (TexCut.w == I_NULL)
	{
		TexCut.w = g_pTexture[g_Area].Widht[TexNum];
	}
	if (TexCut.h == I_NULL)
	{
		TexCut.h = g_pTexture[g_Area].Height[TexNum];
	}
	return TexCut;
}

//-----------------------------------------------------------------
// アニメーションの設定
// Anim : アニメーション構造体
INT_List CPolygon::SetAnimation(ANIMATION Anim, INT_List TexCut)
{
	if (Anim.AnimationON == B_ON)
	{
		int patternNum = _RESIDUAL_(_DIVISION_(Anim.Count, Anim.Frame), Anim.Pattern);	// フレームに１回	パターン数
		int patternV = _RESIDUAL_(patternNum, Anim.Direction); // 横方向のパターン
		int patternH = _DIVISION_(patternNum, Anim.Direction); // 縦方向のパターン

		TexCut.x = _MULTIPLIED_(patternV, TexCut.x); // 切り取り座標X
		TexCut.y = _MULTIPLIED_(patternH, TexCut.y); // 切り取り座標Y
	}

	return TexCut;
}

//-----------------------------------------------------------------
// ポリゴン描画
// POLYGON 構造体を使用
// ADD 加算合成の有効化
void CPolygon::Draw(POLYGON Polygon, bool ADD)
{
	// 加算合成の設定
	if (ADD == B_ON)
	{
		g_pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);	 // ブレンディング処理
		g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); // αソースカラーの指定
		g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	Polygon.TexCut = SetTexturCut(Polygon.TexCut, Polygon.TexName);
	if (Polygon.bAffine == B_ON) { Polygon.TexCut = SetAnimation(Polygon.Anim, Polygon.TexCut); }
	DrawPolygon(Polygon);

	// 通常の半透明処理に戻す
	if (ADD == B_ON)
	{
		g_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの設定
		g_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// α
	}
}

//-----------------------------------------------------------------
// POLYGON 構造体の共通初期化
POLYGON CPolygon::PolygonDataInitList(void)
{
	POLYGON List;

	List.Ang.Angle = List.Ang.x = List.Ang.y = F_NULL;
	List.bAffine = B_OFF;
	List.Color.r = List.Color.g = List.Color.b = List.Color.a = _RGBACOLOR_;
	List.Poly.x = List.Poly.y = List.Poly.w = List.Poly.h = List.Poly.z = F_NULL;
	List.Scale.x = List.Scale.y = List.Scale.w = List.Scale.h = F_DEF;
	List.TexCut.x = List.TexCut.y = List.TexCut.w = List.TexCut.h = List.TexCut.z = I_NULL;
	List.TexName = I_MAX;
	List.Anim.Count = List.Anim.Direction = List.Anim.Frame = List.Anim.Pattern = I_NULL;
	List.Anim.AnimationON = B_OFF;

	return List;
}

//-----------------------------------------------------------------
// デバッグ用テキスト初期化
void CDebug::Init(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;

	if (bLock == false)
	{
		CDevice device;

		D3DXCreateFont(
			device.GetD3DDevice(),		// デバイス
			18,					// 文字の高さ
			0,					// 文字の幅
			0,					// フォントの太さ
			0,					// MIPMAPのレベル
			FALSE,				// イタリックか？
			SHIFTJIS_CHARSET,	// 文字セット
			OUT_DEFAULT_PRECIS,	// 出力精度
			DEFAULT_QUALITY,	// 出力品質
			DEFAULT_PITCH,		// フォントピッチとファミリ
			"Terminal",			// フォント名
			&g_pFont			// Direct3Dフォントへのポインタへのアドレス
		);
		bLock = true;
	}
#endif
}

//-----------------------------------------------------------------
// デバッグ用テキスト終了処理
void CDebug::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;

	if (bLock == false)
	{
		if (g_pFont != NULL)
		{
			g_pFont->Release();
			g_pFont = NULL;
		}
		bLock = true;
	}
#endif
}

//-----------------------------------------------------------------
// 描画位置と色の指定
// x,y : 標示位置X,Y
// Color : 文字の色
void CDebug::PosAndColor(int x, int y, D3DCOLOR Color)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_FontPosX = x;
	g_FontPosY = y;
	g_Color = Color;
#else
	x; y; Color;
#endif
}

//-----------------------------------------------------------------
// デバッグ用plyntf
// plyntfと使い方は同じ
void CDebug::DPrintf(const char* pFormat, ...)
{
#if defined(_DEBUG) || defined(DEBUG)
	va_list argp;
	RECT rect = { g_FontPosX, g_FontPosY, g_Width, g_Height };
	char strBuf[_MULTIPLIED_(STRING_SIZE, 2)];

	va_start(argp, pFormat);
	vsprintf_s(strBuf, _MULTIPLIED_(STRING_SIZE, 2), pFormat, argp);
	va_end(argp);
	g_pFont->DrawText(NULL, strBuf, -1, &rect, DT_LEFT, g_Color);
#else
	pFormat;
#endif
}

//-----------------------------------------------------------------
// 当たり判定の視覚化
// NumLine : 線の数
void CDebug::Polygon(int NumLine, FLOAT_List Poly, D3DCOLOR Color)
{
#if defined(_DEBUG) || defined(DEBUG)
	CDevice device;

	if (device.GetD3DDevice() == NULL) { return; }

	const float C_R = _PI_(NumLine, 2);	//円周率を使い計算
	
	if (g_pV == NULL)
	{
		g_pV = new DEBUG_VERTEX[(NumLine + 1)];
	}
	float PolyR = min(Poly.w, Poly.h);

	PolyR = _MULTIPLIED_W(PolyR, CORRECTION, CORRECTION);
	
	for (int i = 0; i < (NumLine + 1); i++)
	{
		g_pV[i].Pos.x = _DEBUG_COSF_(Poly.x, C_R, i, PolyR, Poly.w, CORRECTION);
		g_pV[i].Pos.y = _DEBUG_SINF_(Poly.y, C_R, i, PolyR, Poly.h, CORRECTION);
		g_pV[i].Pos.z = F_DEF;
		g_pV[i].Pos.w = F_DEF;
		g_pV[i].Color = Color;
	}

	device.GetD3DDevice()->SetTexture(I_NULL, NULL);
	device.GetD3DDevice()->SetFVF(FVF_DEBUG);
	device.GetD3DDevice()->DrawPrimitiveUP(D3DPT_LINESTRIP, NumLine, g_pV, sizeof(DEBUG_VERTEX));

	// メモリの解放
	if (g_pV != NULL)
	{
		delete[]g_pV;
		g_pV = NULL;
	}
#else
	NumLine; Color; Poly;
#endif
}
