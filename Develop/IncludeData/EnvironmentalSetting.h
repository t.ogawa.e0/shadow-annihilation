//=================================================================
// DirectX2D環境設定用ヘッダ[EnvironmentalSetting.h]
// author: tatuya ogawa
//=================================================================

#ifndef _ENVIRONMENTALSETTING_H_ // インクルードガード
#define _ENVIRONMENTALSETTING_H_

//=================================================================
// マクロ定義
// 読み込みファイル名
#define TEXTURE_LINK "./data/LINK/Texture_Link_Data.bin" // linkデータ
#define SOUND_LINK "./data/LINK/Sound_Link_Data.bin" // linkデータ
#define SCORE_LINK "./data/SaveData/ScoreData.bin" // スコアデータ

// バックグラウンドカラー
#ifndef BACKGROUND_COLOR
#define BACKGROUND_COLOR D3DCOLOR_RGBA(50, 50, 50, 0)
#endif // !BACKGROUND_COLOR

//=================================================================
// ENUM定義
typedef enum _level
{
	levelNONE = -1,
	level00,	/// ロード
	level01,	/// タイトル
	level02,	/// ホーム
	level03,	/// ゲーム
	level04,	/// リザルト
	levelMAX
}level;

typedef enum _LOAD_SOUND_LIST
{
	LOAD_SE1 = 0,
	LOAD_SE2,
	Button_SE,
	END_Static_SE,
}LOAD_SOUND_LIST;

typedef enum _TITLE_SOUND_LIST
{
	TITLE_Quriora = END_Static_SE,
}TITLE_SOUND_LIST;

typedef enum _HOME_SOUND_LIST
{
	HOME_BeyondTheImmediateSide = END_Static_SE,
}HOME_SOUND_LIST;

typedef enum _RESULT_SOUND_LIST
{
	RESULT_BeyondTheImmediateSide = END_Static_SE,
}RESULT_SOUND_LIST;

typedef enum _GAME_SOUND_LIST
{
	GAME_ElectroPeaceful = END_Static_SE,
}GAME_SOUND_LIST;

typedef enum _LOAD_TEXTURE_LIST
{
	LOADType = level::level00,
	LOADTextureNONE = -1,
	TEXTURE_LoadScreen,			/// ロード画面
	TEXTURE_NumberList0,			/// 数字
	TEXTURE_NumberList1,			/// 数字
	TEXTURE_NumberList2,			/// 数字
	TEXTURE_COLOR,
	TEXTURE_BC53,					/// ロード用
	TEXTURE_LOADING,				/// ロードフォント
	TEXTURE_ThankYouForPlaying,
	LOADTEXTURE_MAX					/// TEXTURE
}LOAD_TEXTURE_LIST;

typedef enum _TITLE_TEXTURE_LIST
{
	TEXTURE_TITLE = 0,				/// タイトル画像
	TEXTURE_TITLE_PRESSENTER,		/// エンター表示
	TEXTURE_TITLE_BIGWhiteNumber,		/// デジタル数字
	TEXTURE_TITLE_Ranking,
	TEXTURE_TITLE_BONTFONT,			/// 撃破数フォント
	TEXTURE_TITLE_KUUHAKU,
	TEXTURE_TITLE_A,
	TEXTURE_TITLE_B,
	TEXTURE_TITLE_C,
	TEXTURE_TITLE_D,
	TEXTURE_TITLE_E,
	TEXTURE_TITLE_F,
	TEXTURE_TITLE_G,
	TEXTURE_TITLE_H,
	TEXTURE_TITLE_I,
	TEXTURE_TITLE_J,
	TEXTURE_TITLE_K,
	TEXTURE_TITLE_L,
	TEXTURE_TITLE_M,
	TEXTURE_TITLE_N,
	TEXTURE_TITLE_O,
	TEXTURE_TITLE_P,
	TEXTURE_TITLE_Q,
	TEXTURE_TITLE_R,
	TEXTURE_TITLE_S,
	TEXTURE_TITLE_T,
	TEXTURE_TITLE_U,
	TEXTURE_TITLE_V,
	TEXTURE_TITLE_W,
	TEXTURE_TITLE_X,
	TEXTURE_TITLE_Y,
	TEXTURE_TITLE_Z,
	TITLETEXTURE_MAX				/// TEXTURE
}TITLE_TEXTURE_LIST;

typedef enum _GAME_TEXTURE_LIST
{
	GAMEType= level::level03,
	GAMETextureNONE = -1,
	TEXTURE_Sky,				/// 空
	TEXTURE_RedCar,				/// 車
	TEXTURE_Cursor,				/// カーソル
	TEXTURE_Arrow,				/// 矢印
	TEXTURE_asphalt,			/// ステージ
	TEXTURE_ENEMY00,			/// エネミー1
	TEXTURE_ENEMY01,			/// エネミー2
	TEXTURE_PLAYER_BULLET,		/// プレイヤーのバレット
	TEXTURE_ENEMY_BULLET,		/// エネミーのバレット
	TEXTURE_LIFEFONT,			/// HPフォント
	TEXTURE_LIFEBER,			/// HP枠
	TEXTURE_LIFEMAIN,			/// メインHP
	TEXTURE_LIFEBACKGROUND,		/// HPバー背景
	TEXTURE_PLAYERBULLETEFFECT,	/// プレイヤーのバレットのエフェクト
	TEXTURE_ENEMYBBULLETEFFECT,	/// エネミーのバレットのエフェクト 
	TEXTURE_EplosionAcolor_10F, /// 爆発
	TEXTURE_BC59,				/// 火花
	TEXTURE_EmergenceEffect,	/// エンゲージエフェクト
	TEXTURE_InformationBar,		/// 情報バー
	TEXTURE_Font00,				/// カウントダウンフォント
	TEXTURE_Font01,				/// 撃破数フォント
	TEXTURE_Font02,				/// WINフォント
	TEXTURE_Font03,				/// LOUSフォント
	TEXTURE_PressEnter,			/// PressEnterフォント
	TEXTURE_PAUSE,				/// ポーズフォント
	TEXTURE_EFFECT,
	TEXTURE_TitleFont,
	TEXTURE_RetryFont,
	TEXTURE_CloseFont,
	GAMETEXTURE_MAX				/// TEXTURE
}GAME_TEXTURE_LIST;

typedef enum _RESULT_TEXTURE_LIST
{
	RESULTType = level::level04,
	RESULTTextureNONE = -1,
	TEXTURE_COROR00,			/// リザルト背景色
	TEXTURE_Result,				/// リザルトフォント
	TEXTURE_COROR01,			///
	TEXTURE_BONTFONT,			/// 撃破数フォント
	TEXTURE_BIGWhiteNumber,		/// デジタル数字
	TEXTURE_RESULT_KUUHAKU,
	TEXTURE_RESULT_A,
	TEXTURE_RESULT_B,
	TEXTURE_RESULT_C,
	TEXTURE_RESULT_D,
	TEXTURE_RESULT_E,
	TEXTURE_RESULT_F,
	TEXTURE_RESULT_G,
	TEXTURE_RESULT_H,
	TEXTURE_RESULT_I,
	TEXTURE_RESULT_J,
	TEXTURE_RESULT_K,
	TEXTURE_RESULT_L,
	TEXTURE_RESULT_M,
	TEXTURE_RESULT_N,
	TEXTURE_RESULT_O,
	TEXTURE_RESULT_P,
	TEXTURE_RESULT_Q,
	TEXTURE_RESULT_R,
	TEXTURE_RESULT_S,
	TEXTURE_RESULT_T,
	TEXTURE_RESULT_U,
	TEXTURE_RESULT_V,
	TEXTURE_RESULT_W,
	TEXTURE_RESULT_X,
	TEXTURE_RESULT_Y,
	TEXTURE_RESULT_Z,
	TEXTURE_Juni,
	TEXTURE_RESULT_YourName,
	TEXTURE_RESULT_OKQ,
	TEXTURE_RESULT_OK,
	TEXTURE_RESULT_NO,
	TEXTURE_RESULT_PRESSENTER,		/// エンター表示
	RESULTTEXTURE_MAX			/// TEXTURE_RESULT_
}RESULT_TEXTURE_LIST;

#endif // !_ENVIRONMENTALSETTING_H_
