//=============================================================================
//
// サウンド処理 [sound.cpp]
// 
//=============================================================================
#include <stdio.h>
#include "sound.h"
#include <XAudio2.h>
#include "EnvironmentalSetting.h"

//=================================================================
// パラメータ構造体定義
typedef struct _SOUNDPARAM
{
	IXAudio2SourceVoice **apSourceVoice; // サウンド
	BYTE **apDataAudio; // オーディオデーター
	DWORD *aSizeAudio; // オーディオデータサイズ
	int *nCntLoop; // ループカウント
	int nNumSound; // サウンド数
} SOUNDPARAM;

//=================================================================
// サウンドデータ情報格納用構造体
typedef struct _SOUND_DATA
{
	char pFilename[STRING_SIZE]; // ファイル名
	int nCntLoop; // ループカウント
} SOUND_DATA;

//=================================================================
// リンク格納用構造体
typedef struct _LINK_DATA
{
	char LinkName[STRING_SIZE];	// リンク名
} LINK_DATA;

//=================================================================
// プロトタイプ宣言
static HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition);
static HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);
static HRESULT LoadSound(int Area, int NumSound);
static bool CallSound(int Area);
static bool GetMemory(int Area, int NumSound);
static bool GetNumSoundData(void);
static void FreeSoundData(int Area);
static void FreeReadData(void);
static void ReleaseSound(int Area, int NumSound);
static void GetDynamicArea(void);

//=================================================================
// グローバル変数
static IXAudio2					*g_pXAudio2 = NULL;			// XAudio2オブジェクトへのインターフェイス
static IXAudio2MasteringVoice	*g_pMasteringVoice = NULL;	// マスターボイス
static SOUND_DATA				*g_aSound;					// テクスチャ情報の格納
static SOUNDPARAM				*g_aParam;					// サウンドデータ構造体のポインタ
static LINK_DATA				*g_aLinkData;				// リンクポインタ
static BGMSVolume				g_aVolume;					// 音量調節
static int						g_NumLink;					// 読み込まれたリンク数
static int						g_OldArea;					// 再生サウンドの情報
static int						g_Area;						// 再生サウンドの情報
static int						g_StaticArea;				// 静的なデータ領域の番号

//-----------------------------------------------------------------
// 初期化処理
HRESULT InitSound(int Area, HWND hWnd)
{
	static bool bLock = false;

	if (bLock == false)
	{
		HRESULT hr;

		// COMライブラリの初期化
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		// XAudio2オブジェクトの作成
		hr = XAudio2Create(&g_pXAudio2, 0);
		if (FAILED(hr))
		{
			MessageBox(hWnd, "XAudio2オブジェクトの作成に失敗！", "警告！", MB_ICONWARNING);

			// COMライブラリの終了処理
			CoUninitialize();

			return E_FAIL;
		}

		// マスターボイスの生成
		hr = g_pXAudio2->CreateMasteringVoice(&g_pMasteringVoice);
		if (FAILED(hr))
		{
			MessageBox(hWnd, "マスターボイスの生成に失敗！", "警告！", MB_ICONWARNING);

			if (g_pXAudio2)
			{
				// XAudio2オブジェクトの開放
				g_pXAudio2->Release();
				g_pXAudio2 = NULL;
			}

			// COMライブラリの終了処理
			CoUninitialize();

			return E_FAIL;
		}

		g_aVolume.BGM = 0.5f;
		g_aVolume.SE = 0.5f;
		g_NumLink = 0;
		g_StaticArea = I_MAX;

		g_OldArea = g_Area = Area;
		if (!UpdateSoundDynamicArea(Area)) { return S_FALSE; }
		bLock = true;
	}

	return S_OK;
}

//-----------------------------------------------------------------
// 終了処理
void UninitSound(void)
{
	static bool bLock = false;

	if (bLock == false)
	{
		// 一時停止
		for (int i = 0; i < g_NumLink; i++)
		{
			ReleaseSound(i, g_aParam[i].nNumSound);
			FreeSoundData(i);
		}

		if (g_aParam != NULL)
		{
			delete[] g_aParam;
			g_aParam = NULL;
		}

		// マスターボイスの破棄
		g_pMasteringVoice->DestroyVoice();
		g_pMasteringVoice = NULL;

		if (g_pXAudio2)
		{
			// XAudio2オブジェクトの開放
			g_pXAudio2->Release();
			g_pXAudio2 = NULL;
		}

		// COMライブラリの終了処理
		CoUninitialize();
		bLock = true;
	}
}

//-----------------------------------------------------------------
// セグメント再生(再生中なら停止)
HRESULT PlaySound(int label)
{
	XAUDIO2_VOICE_STATE xa2state;
	XAUDIO2_BUFFER buffer;

	// バッファの値設定
	memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
	buffer.AudioBytes = g_aParam[g_Area].aSizeAudio[label];
	buffer.pAudioData = g_aParam[g_Area].apDataAudio[label];
	buffer.Flags      = XAUDIO2_END_OF_STREAM;
	buffer.LoopCount  = g_aParam[g_Area].nCntLoop[label];

	// 状態取得
	g_aParam[g_Area].apSourceVoice[label]->GetState(&xa2state);
	if(xa2state.BuffersQueued != 0)
	{// 再生中
		// 一時停止
		g_aParam[g_Area].apSourceVoice[label]->Stop(0);

		// オーディオバッファの削除
		g_aParam[g_Area].apSourceVoice[label]->FlushSourceBuffers();
	}

	// オーディオバッファの登録
	g_aParam[g_Area].apSourceVoice[label]->SubmitSourceBuffer(&buffer);

	// 再生
	g_aParam[g_Area].apSourceVoice[label]->Start(0);

	return S_OK;
}

//-----------------------------------------------------------------
// セグメント停止(ラベル指定)
void StopLimitedSound(int label)
{
	XAUDIO2_VOICE_STATE xa2state;

	// 状態取得
	g_aParam[g_Area].apSourceVoice[label]->GetState(&xa2state);
	if(xa2state.BuffersQueued != 0)
	{// 再生中
		// 一時停止
		g_aParam[g_Area].apSourceVoice[label]->Stop(0);

		// オーディオバッファの削除
		g_aParam[g_Area].apSourceVoice[label]->FlushSourceBuffers();
	}
}

//-----------------------------------------------------------------
// セグメント停止(全て)
void StopAllSound(void)
{
	// 一時停止
	for(int nCntSound = 0; nCntSound < g_aParam[g_Area].nNumSound; nCntSound++)
	{
		if(g_aParam[g_Area].apSourceVoice[nCntSound])
		{
			// 一時停止
			g_aParam[g_Area].apSourceVoice[nCntSound]->Stop(0);
		}
	}
}

//-----------------------------------------------------------------
// チャンクのチェック
HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition)
{
	HRESULT hr = S_OK;
	DWORD dwRead;
	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD dwBytesRead = 0;
	DWORD dwOffset = 0;

	if (SetFilePointer(hFile, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを先頭に移動
		return HRESULT_FROM_WIN32(GetLastError());
	}

	while (hr == S_OK)
	{
		if (ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, NULL) == 0)
		{// チャンクの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		if (ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, NULL) == 0)
		{// チャンクデータの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch (dwChunkType)
		{
		case 'FFIR':
			dwRIFFDataSize = dwChunkDataSize;
			dwChunkDataSize = 4;
			if (ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, NULL) == 0)
			{// ファイルタイプの読み込み
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;

		default:
			if (SetFilePointer(hFile, dwChunkDataSize, NULL, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			{// ファイルポインタをチャンクデータ分移動
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD) * 2;
		if (dwChunkType == format)
		{
			*pChunkSize = dwChunkDataSize;
			*pChunkDataPosition = dwOffset;

			return S_OK;
		}

		dwOffset += dwChunkDataSize;
		if (dwBytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}

	return S_OK;
}

//-----------------------------------------------------------------
// チャンクデータの読み込み
HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset)
{
	DWORD dwRead;
	
	if(SetFilePointer(hFile, dwBufferoffset, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを指定位置まで移動
		return HRESULT_FROM_WIN32(GetLastError());
	}

	if(ReadFile(hFile, pBuffer, dwBuffersize, &dwRead, NULL) == 0)
	{// データの読み込み
		return HRESULT_FROM_WIN32(GetLastError());
	}
	
	return S_OK;
}

//-----------------------------------------------------------------
// 静的な領域確保
// 戻り値 : 失敗=false 成功=true
bool InitSoundStaticArea(int Area)
{
	static bool bLock = false;

	if (bLock == false)
	{
		if (g_Area != Area)
		{
			int nOldArea = g_OldArea;
			int nArea = g_Area;

			g_Area = g_OldArea = Area;
			if (!UpdateSoundDynamicArea(Area)) { return false; }
			g_OldArea = nOldArea;
			g_Area = nArea;
			g_StaticArea = Area;
			bLock = true;
		}
	}

	return true;
}

//-----------------------------------------------------------------
// 静的な領域に切り替え
// 戻り値 : 前回の領域
int SoundStaticAreaStart(void)
{
	static int nAreaData;

	if (g_StaticArea != I_MAX)
	{
		if (g_StaticArea != g_Area)
		{
			nAreaData = g_Area;
			g_Area = g_StaticArea;
		}
	}
	else
	{
		nAreaData = g_Area;
	}

	return nAreaData;
}

//-----------------------------------------------------------------
// 静的な領域終了時の最後に記述
// Area : 前回の領域
void SoundStaticAreaEnd(int Area)
{
	if (g_StaticArea != I_MAX)
	{
		if (g_StaticArea == g_Area) { g_Area = Area; }
	}
}

//-----------------------------------------------------------------
// サウンド領域の更新
bool UpdateSoundDynamicArea(int Area)
{
	static bool bOpen = false;

	if (g_StaticArea != Area)
	{
		if (bOpen == true)
		{
			// 切り替え時の初期化
			ReleaseSound(g_OldArea, g_aParam[g_OldArea].nNumSound);
			FreeSoundData(g_OldArea);
		}
		else
		{
			bOpen = true;
		}

		// 切り替え時のデータ取得
		if (!GetNumSoundData()) { return false; }
		if (!CallSound(Area)) { return false; }
		if (LoadSound(Area, g_aParam[Area].nNumSound) != S_OK) { return false; }
		g_OldArea = g_Area = Area;
		FreeReadData();
	}

	return true;
}

//-----------------------------------------------------------------
// サウンドのデータ数取得
bool GetNumSoundData(void)
{
	FILE* pFile;

	// リンク読み込み
	pFile = fopen(SOUND_LINK, "rb");
	if (pFile)
	{
		//サウンドデータ数の読み取り
		fread(&g_NumLink, _BIT_, 1, pFile);
		g_aLinkData = new LINK_DATA[g_NumLink];
		fread(g_aLinkData, sizeof(LINK_DATA), g_NumLink, pFile);
		fclose(pFile);
	}
	else
	{
		ErrorMessage("サウンドデータリンクの取得に失敗しました。");
		return false;
	}
	return true;
}

//-----------------------------------------------------------------
// メモリ確保
bool GetMemory(int Area, int NumSound)
{
	if (!(NumSound == 0))
	{
		g_aSound = new SOUND_DATA[NumSound];
		g_aParam[Area].apDataAudio = new BYTE*[NumSound];
		g_aParam[Area].apSourceVoice = new IXAudio2SourceVoice*[NumSound];
		g_aParam[Area].aSizeAudio = new DWORD[NumSound];
		g_aParam[Area].nCntLoop = new int[NumSound];
	}
	else
	{
		ErrorMessage("サウンド数が0のため動作を終了します。");
		return false;
	}
	return true;
}

//-----------------------------------------------------------------
// メモリ確保
void GetDynamicArea(void)
{
	static bool bLock = false;

	if (bLock == false)
	{
		if (g_aParam == NULL)
		{
			// サウンド構造体のメモリ確保
			g_aParam = new SOUNDPARAM[g_NumLink];

			// サウンド領域をNULLで初期化
			for (int i = 0; i < g_NumLink; i++)
			{
				g_aParam[i].apDataAudio = NULL;
				g_aParam[i].apSourceVoice = NULL;
				g_aParam[i].aSizeAudio = NULL;
				g_aParam[i].nCntLoop = NULL;
				g_aParam[i].nNumSound = NULL;
			}
		}
		bLock = true;
	}
}

//-----------------------------------------------------------------
// サウンドの呼び出し
bool CallSound(int Area)
{
	FILE* pFile;

	pFile = fopen(g_aLinkData[Area].LinkName, "rb");
	if (pFile)
	{
		GetDynamicArea();

		// テクスチャデータの数の読み取り
		fread(&g_aParam[Area].nNumSound, _BIT_, 1, pFile);
		if (!GetMemory(Area, g_aParam[Area].nNumSound)) { return false; }
		fread(g_aSound, sizeof(SOUND_DATA), g_aParam[Area].nNumSound, pFile);
		fclose(pFile);
	}
	else
	{
		ErrorMessage("サウンドデータパスの読み込みに失敗しました。");
		return false;
	}
	return true;
}

//-----------------------------------------------------------------
// サウンドの破棄
void ReleaseSound(int Area, int NumSound)
{
	if (g_aParam[Area].apSourceVoice != NULL)
	{
		for (int i = 0; i < NumSound; i++)
		{
			// 一時停止
			g_aParam[Area].apSourceVoice[i]->Stop(0);

			// ソースボイスの破棄
			g_aParam[Area].apSourceVoice[i]->DestroyVoice();
			g_aParam[Area].apSourceVoice[i] = NULL;

			// オーディオデータの開放
			free(g_aParam[Area].apDataAudio[i]);
			g_aParam[Area].apDataAudio[i] = NULL;
		}
	}
}

//-----------------------------------------------------------------
// サウンドのデータを開放
void FreeSoundData(int Area)
{
	if (g_aParam[Area].apSourceVoice != NULL)
	{
		delete[] g_aParam[Area].apSourceVoice;
		g_aParam[Area].apSourceVoice = NULL;
	}
	if (g_aParam[Area].apDataAudio != NULL)
	{
		delete[] g_aParam[Area].apDataAudio;
		g_aParam[Area].apDataAudio = NULL;
	}
	if (g_aParam[Area].aSizeAudio != NULL)
	{
		delete[] g_aParam[Area].aSizeAudio;
		g_aParam[Area].aSizeAudio = NULL;
	}
	if (g_aParam[Area].nCntLoop != NULL)
	{
		delete[] g_aParam[Area].nCntLoop;
		g_aParam[Area].nCntLoop = NULL;
	}
}

//-----------------------------------------------------------------
// 読み取り用データの開放
void FreeReadData(void)
{
	if (g_aSound != NULL)
	{
		delete[] g_aSound;
		g_aSound = NULL;
	}
	if (g_aLinkData != NULL)
	{
		delete[] g_aLinkData;
		g_aLinkData = NULL;
	}
}

//-----------------------------------------------------------------
// サウンドの読み込み
HRESULT LoadSound(int Area, int NumSound)
{
	HRESULT hr;

	for (int i = 0; i < NumSound; i++)
	{
		HANDLE hFile;
		DWORD dwChunkSize = 0;
		DWORD dwChunkPosition = 0;
		DWORD dwFiletype;
		WAVEFORMATEXTENSIBLE wfx;
		XAUDIO2_BUFFER buffer;

		// バッファのクリア
		memset(&wfx, 0, sizeof(WAVEFORMATEXTENSIBLE));
		memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));

		// サウンドデータファイルの生成
		g_aParam[Area].nCntLoop[i] = g_aSound[i].nCntLoop;
		hFile = CreateFile(g_aSound[i].pFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			MessageBox(NULL, "サウンドデータファイルの生成に失敗！(1)", "警告！", MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}

		if (SetFilePointer(hFile, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
		{// ファイルポインタを先頭に移動
			MessageBox(NULL, "サウンドデータファイルの生成に失敗！(2)", "警告！", MB_ICONWARNING);
			return HRESULT_FROM_WIN32(GetLastError());
		}

		// WAVEファイルのチェック
		hr = CheckChunk(hFile, 'FFIR', &dwChunkSize, &dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "WAVEファイルのチェックに失敗！(1)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}
		hr = ReadChunkData(hFile, &dwFiletype, sizeof(DWORD), dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "WAVEファイルのチェックに失敗！(2)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}
		if (dwFiletype != 'EVAW')
		{
			MessageBox(NULL, "WAVEファイルのチェックに失敗！(3)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}

		// フォーマットチェック
		hr = CheckChunk(hFile, ' tmf', &dwChunkSize, &dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "フォーマットチェックに失敗！(1)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}
		hr = ReadChunkData(hFile, &wfx, dwChunkSize, dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "フォーマットチェックに失敗！(2)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}

		// オーディオデータ読み込み
		hr = CheckChunk(hFile, 'atad', &g_aParam[Area].aSizeAudio[i], &dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "オーディオデータ読み込みに失敗！(1)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}
		g_aParam[Area].apDataAudio[i] = (BYTE*)malloc(g_aParam[Area].aSizeAudio[i]);
		hr = ReadChunkData(hFile, g_aParam[Area].apDataAudio[i], g_aParam[Area].aSizeAudio[i], dwChunkPosition);
		if (FAILED(hr))
		{
			MessageBox(NULL, "オーディオデータ読み込みに失敗！(2)", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}

		// ソースボイスの生成
		hr = g_pXAudio2->CreateSourceVoice(&g_aParam[Area].apSourceVoice[i], &(wfx.Format));
		if (FAILED(hr))
		{
			MessageBox(NULL, "ソースボイスの生成に失敗！", "警告！", MB_ICONWARNING);
			return S_FALSE;
		}

		// バッファの値設定
		memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
		buffer.AudioBytes = g_aParam[Area].aSizeAudio[i];
		buffer.pAudioData = g_aParam[Area].apDataAudio[i];
		buffer.Flags = XAUDIO2_END_OF_STREAM;
		buffer.LoopCount = g_aParam[Area].nCntLoop[i];

		// 音量の調節
		if (g_aParam[Area].nCntLoop[i] == 0)
		{
			g_aParam[Area].apSourceVoice[i]->SetVolume(g_aVolume.SE);
		}
		else
		{
			g_aParam[Area].apSourceVoice[i]->SetVolume(g_aVolume.BGM);
		}

		// オーディオバッファの登録
		g_aParam[Area].apSourceVoice[i]->SubmitSourceBuffer(&buffer);
	}
	return S_OK;
}

//-----------------------------------------------------------------
// 音量の設定
BGMSVolume ConfigVolume(bool BGM, bool SE, BGMSVolume Volume)
{
	if (g_aVolume.BGM != Volume.BGM || g_aVolume.SE != Volume.SE)
	{
		if (BGM) { g_aVolume.BGM = Volume.BGM; }
		if (SE) { g_aVolume.SE = Volume.SE; }
		for (int i = 0; i < g_aParam[g_Area].nNumSound; i++)
		{
			if (g_aParam[g_Area].nCntLoop[i] == 0)
			{
				g_aParam[g_Area].apSourceVoice[i]->SetVolume(g_aVolume.SE);
			}
			else
			{
				g_aParam[g_Area].apSourceVoice[i]->SetVolume(g_aVolume.BGM);
			}
		}
	}

	return g_aVolume;
}