//=================================================================
// DirectX2D用関数ヘッダ[DirectX2D.h]
// author: tatuya ogawa
//=================================================================

#ifndef _DIRECTX2D_H_ // インクルードガード
#define _DIRECTX2D_H_

#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "EnvironmentalSetting.h"

//=================================================================
// マクロ定義

// 各型初期化マクロ
#ifndef F_NULL
#define F_NULL (0.0F) // float型
#endif // !F_NULL
#ifndef I_NULL
#define I_NULL (0) // int型
#endif // !I_NULL
#ifndef C_NULL
#define C_NULL (0) // char型
#endif // !C_NULL
#ifndef L_NULL
#define L_NULL (0L) // long型
#endif // !L_NULL
#ifndef LL_NULL
#define LL_NULL (0LL) // long long型
#endif // !LL_NULL
#ifndef U_NULL
#define U_NULL (0U) // unsigned型
#endif // !U_NULL
#ifndef UL_NULL
#define UL_NULL (0UL) // unsigned long 型
#endif // !UL_NULL
#ifndef D_NULL
#define D_NULL (0.0) // double型
#endif // !D_NULL

#ifndef I_MAX
#define I_MAX (2147483647)
#endif // !I_MAX

// ON,OFF マクロ
#ifndef B_ON
#define B_ON true // オン
#endif // !B_ON
#ifndef B_OFF
#define B_OFF false // オフ
#endif // !B_OFF

// データサイズ マクロ
#ifndef STRING_SIZE
#define STRING_SIZE (256) // 文字列のサイズ	
#endif // !STRING_SIZE
#ifndef _BIT_
#define _BIT_ (4)
#endif // !_BIT_
#ifndef F_DEF
#define F_DEF (1.0f) // デフォルト値
#endif // !F_DEF

// 誤差補正
#ifndef CORRECTION
#define CORRECTION (0.5f) // 座標補正
#endif // !CORRECTION
#ifndef CENTER_POS
#define CENTER_POS (2.0f) // テクスチャの中心
#endif // !CENTER_POS
#ifndef CENTER_SEARCH
#define CENTER_SEARCH (2) // 中心を割り出す
#endif // !CENTER_SEARCH

// D3DFVF_DIFFUSE	色
// D3DFVF_TEX1		テクスチャ
#ifndef FVF_VERTEX_2D
#define FVF_VERTEX_2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#endif // !FVF_VERTEX_2D

// カラー
#ifndef _RGBACOLOR_
#define _RGBACOLOR_ (255)
#endif // !_RGBACOLOR_

// エラーウィンドウ
#ifndef ErrorMessage
#define ErrorMessage(c) MessageBox(NULL, (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

// 計算マクロ
#ifndef _PLUS_
#define  _PLUS_(a,b) ((a) + (b))
#endif // !_PLUS_
#ifndef _MINUS_
#define  _MINUS_(a,b) ((a) - (b))
#endif // !_MINUS_
#ifndef _DIVISION_
#define  _DIVISION_(a,b) ((a) / (b))
#endif // !_DIVISION_
#ifndef _MULTIPLIED_
#define  _MULTIPLIED_(a,b) ((a) * (b))
#endif // !_MULTIPLIED_
#ifndef _RESIDUAL_
#define  _RESIDUAL_(a,b) ((a) % (b))
#endif // !_RESIDUAL_
#ifndef _PI_
#define  _PI_(a,b) (((D3DX_PI) * (b) / (a)))
#endif // !_PI_
#ifndef _BOOL_
#define _BOOL_(a) ((a) ^ 1)
#endif // !_BOOL_
#ifndef _ROUNDCRATIO_ //半径×半径,次条
#define _ROUNDCRATIO_(a,b,c) ((a) < ((b) + (c)) * ((b) + (c)))
#endif // !_ROUNDCRATIO_ 
#ifndef FOR
#define FOR(i,a,b) for(int i=(a);i<(b);++i)
#endif // !FOR
#ifndef REP
#define REP(i,n)  FOR((i),0,(n))
#endif // !REP

//=================================================================
// 構造体

//=================================================================
// INT型xywh
typedef struct _INT_List
{
	int x;
	int y;
	int w;
	int h;
	int z;
} INT_List;

//=================================================================
// FLOAT型xywh
typedef struct _FLOAT_List
{
	float x;
	float y;
	float w;
	float h;
	float z;
} FLOAT_List;

//=================================================================
// LONG型xywh
typedef struct _LONG_List
{
	long x;
	long y;
	long w;
	long h;
	long z;
} LONG_List;

//=================================================================
// ID
typedef struct _ID
{
	int New; // 新しいID
	int Old; // 古いID
}ID;

//=================================================================
// カウンタ
typedef struct _INTFRAME
{
	int Count; // カウンタ
	int End; // 終了
}INTFRAME, INTTIM;

//=================================================================
// カウンタ
typedef struct _FLOATFRAME
{
	float Count; // カウンタ
	float End; // 終了
}FLOATFRAME, FLOATTIM;

//=================================================================
// 色構造体
typedef struct _RGBA_COLOR
{
	int r;
	int g;
	int b;
	int a;
} RGBA_COLOR;

//=================================================================
// アニメーション構造体
typedef struct _ANIMATION
{
	int Count; // アニメーションカウンタ
	int Frame; // 更新タイミング
	int Pattern; // アニメーションのパターン数
	int Direction; // 一行のアニメーション数
	bool AnimationON; // アニメーションの有無
}ANIMATION;

//=================================================================
// 表示角度変更
typedef struct _ANGLE
{
	float x; // ポリゴンの中心座標 初期値1.0f推奨
	float y; // ポリゴンの中心座標 初期値1.0f推奨
	float Angle; // 角度 初期値1.0f推奨
}ANGLE;

//=================================================================
// テクスチャ拡大用
typedef struct _SCALE
{
	float x; // 拡大中心 初期値1.0f推奨
	float y; // 拡大中心 初期値1.0f推奨
	float w; // 拡大するサイズ 初期値1.0f推奨
	float h; // 拡大するサイズ 初期値1.0f推奨
}SCALE;

//=================================================================
// 円形の構造体
typedef struct _ROUND
{
	float x; // 座標
	float y; // 座標
	float r; // 幅
}ROUND;

//=================================================================
// ライフ構造体
typedef struct _LIFELIST
{
	int Max; // 最大
	int Current; // 現在
}LIFE;

//=================================================================
// 攻撃力
typedef struct _ATTACK
{
	int Power; // 攻撃力
}ATTACK;

//=================================================================
// 速度
typedef struct _SPEED
{
	float x; // x方向
	float y; // y方向
}SPEED;

//=================================================================
// パラメーターリスト
typedef struct _PARAMETERLIST
{
	LIFE Life; // ライフ
	ATTACK Attack; // 攻撃力
	SPEED Speed; // 速度
}PARAMETERLIST;

//=================================================================
// ポリゴン用構造体詰め合わせ
typedef struct _POLYGON_DATA_LIST
{
	int TexName; // 使用するテクスチャの名前
	FLOAT_List Poly; // ポリゴン
	INT_List TexCut; // テクスチャ
	ANGLE Ang; // 回転情報
	RGBA_COLOR Color; // 色
	SCALE Scale; // スケール
	ANIMATION Anim; // アニメーション
	bool bAffine; // 回転の有無
}POLYGON;

//=================================================================
// テクスチャの情報構造体
typedef struct _VERTEX_2D
{
	D3DXVECTOR4 Pos; // 座標
	D3DCOLOR	Color; // ポリゴンの色
	D3DXVECTOR2 Texcoord; // テクスチャ座標
} VERTEX_2D;

//=================================================================
// リンク格納用構造体
typedef struct _LINK_DATA
{
	char LinkName[STRING_SIZE]; // リンク
} LINK_DATA;

//=================================================================
// 読み込みデータ格納用構造体
typedef struct _TEXTURE_DATA
{
	char FileName[STRING_SIZE]; // テクスチャのパス
} TEXTURE_DATA;

//=================================================================
// テクスチャデータ管理用構造体
typedef struct _TEXTURE
{
	LPDIRECT3DTEXTURE9 *Texture; // テクスチャポインタ
	int *Widht; // 幅
	int *Height; // 高さ
	int NumTexture; // 枚数
} TEXTURE;

typedef struct _TEXTURE_PLAM
{
	int Widht; // 幅
	int Height; // 高さ
} TEXTURE_PLAM;

#if defined(_DEBUG) || defined(DEBUG)
//=================================================================
// デバッグ用構造体
typedef struct _DEBUG_VERTEX
{
	D3DXVECTOR4 Pos; // 座標
	D3DCOLOR Color; // 色
} DEBUG_VERTEX;
#endif

// デバイス設定
class CDevice
{
private:
	void UninitDirect3DObject(void);
	void UninitDevice(void);
public:
	//-----------------------------------------------------------------
	// デバイスの初期化
	bool Init(HINSTANCE hInstance, HWND hWnd, BOOL bWindow, int Width, int Height);

	//-----------------------------------------------------------------
	// デバイスの解放
	void Uninit(void);

	//-----------------------------------------------------------------
	// デバイス渡し
	LPDIRECT3DDEVICE9 GetD3DDevice(void);
};

// ポリゴン
class CPolygon
{
private:
#ifndef _MULTIPLIED_W
#define  _MULTIPLIED_W(a,b,c)  (((a) * (b) * (c)))
#endif // !_MULTIPLIED_W
#ifndef _D3DXVECTOR4FX_
#define  _D3DXVECTOR4FX_(a,b,c,d)  (((a) + (b) - (c)) * (d))
#endif // !_D3DXVECTOR4FX_
#ifndef _D3DXVECTOR4FY_
#define  _D3DXVECTOR4FY_(a,b,c,d) _D3DXVECTOR4FX_(a,b,c,d)
#endif // !_D3DXVECTOR4FY_
#ifndef _D3DXVECTOR4AFX_
#define  _D3DXVECTOR4AFX_(a,b,c,d,e)  ((a) + (b) * cosf((c)) - (d) * sinf((c)) - (e))
#endif // !_D3DXVECTOR4AFX_
#ifndef _D3DXVECTOR4AFY_
#define  _D3DXVECTOR4AFY_(a,b,c,d,e)  ((a) + (b) * sinf((c)) + (d) * cosf((c)) - (e))
#endif // !_D3DXVECTOR4AFY_

	// メモリ確保
	bool GetMemory(int Area, int NumTex);

	// 動的な領域確保
	void GetDynamicArea(void);

	// 読み取り用データの開放
	void FreeReadData(void);

	// テクスチャの破棄
	void ReleaseTexture(int Area, int NumTex);

	// テクスチャのデータを開放
	void FreeTextureData(int Area);

	// テクスチャのデータ数取得
	bool GetNumTextureData(void);

	// テクスチャの呼び出し
	bool CallTexture(int Area);

	// テクスチャの読み込み
	bool LoadTexture(int Area, int NumTex);

	// テクスチャ切り取り設定
	INT_List SetTexturCut(INT_List TexCut, int TexNum);

	// アニメーションの設定
	INT_List SetAnimation(ANIMATION Anim, INT_List TexCut);

	// ポリゴンの描画
	void DrawPolygon(POLYGON Polygon);

	// 非回転
	void CreateVertex(POLYGON Polygon);

	// 回転
	void CreateVertexAngle(POLYGON Polygon);
public:
	//-----------------------------------------------------------------
	// ポリゴンの初期化
	// 戻り値 : 失敗=false 成功=true
	bool Init(int Area);

	//-----------------------------------------------------------------
	// ポリゴンの解放
	void Uninit(void);

	//-----------------------------------------------------------------
	// 静的な領域確保
	// 戻り値 : 失敗=false 成功=true
	bool InitStaticArea(int Area);

	//-----------------------------------------------------------------
	// 静的な領域に切り替え
	// 戻り値 : 前回の領域
	int StaticAreaStart(void);

	//-----------------------------------------------------------------
	// 静的な領域終了時の最後に記述
	// Area : 前回の領域
	void StaticAreaEnd(int Area);

	//-----------------------------------------------------------------
	// 次の領域の確保
	// Area : 次の領域
	// 戻り値 : 失敗=false 成功=true
	bool UpdateDynamicArea(int Area, bool bStatic = false);

	//-----------------------------------------------------------------
	// ポリゴン描画
	// POLYGON 構造体を使用
	// ADD 加算合成の有効化
	void Draw(POLYGON Polygon, bool ADD = B_OFF);

	//-----------------------------------------------------------------
	// POLYGON 構造体の共通初期化
	POLYGON PolygonDataInitList(void);

	//-----------------------------------------------------------------
	// 読み込んだテクスチャの幅と高さの取得
	TEXTURE_PLAM GetTextureInfo(int TexNum);
};

// デバッグ用
class CDebug
{
private:
#ifndef FVF_DEBUG
#define FVF_DEBUG		(D3DFVF_XYZRHW | D3DFVF_DIFFUSE)
#endif // !FVF_DEBUG
#ifndef _DEBUG_COSF_
#define  _DEBUG_COSF_(a,b,c,d,e,f)  (((a) + cosf((b) * (c)) * (d)) + (e) * (f))
#endif // !_DEBUG_COSF_
#ifndef _DEBUG_SINF_
#define  _DEBUG_SINF_(a,b,c,d,e,f)  (((a) + sinf((b) * (c)) * (d)) + (e) * (f))
#endif // !_DEBUG_SINF_
public:
	//-----------------------------------------------------------------
	// デバッグ用テキスト初期化
	void Init(void);

	//-----------------------------------------------------------------
	// デバッグ用テキスト終了処理
	void Uninit(void);

	//-----------------------------------------------------------------
	// 描画位置と色の指定
	// x,y : 標示位置X,Y
	// Color : 文字の色
	void PosAndColor(int x, int y, D3DCOLOR Color = D3DCOLOR_RGBA(255, 255, 255, 255));

	//-----------------------------------------------------------------
	// デバッグ用plyntf
	// plyntfと使い方は同じ
	void DPrintf(const char* pFormat, ...);

	//-----------------------------------------------------------------
	// 当たり判定の視覚化
	// NumLine : 線の数
	void Polygon(int NumLine, FLOAT_List Poly, D3DCOLOR Color = D3DCOLOR_RGBA(255, 255, 255, 255));
};

class CDirectX2D : public CDevice, CPolygon, CDebug
{
public:
private:
protected:
};

#endif // !_DIRECTX2D_H_