//=================================================================
// ポーズ処理[Pause.h]
// author: tatuya ogawa
//=================================================================

#ifndef _PAUSE_H_ // インクルードガード
#define _PAUSE_H_

//=================================================================
// マクロ定義
#define NUMPAUSE (9) // ポーズテクスチャ数
#define PAUSE_EFFENI (50.0f)
#define PAUSE_FONT_COUNT (70) // ポーズフォント表示時間
#define PAUSE_MANUE_W (300.0f) // ポーズメニュー幅
#define PAUSE_MANUE_H (100.0f) // ポーズメニュー高さ
#define PAUSE_SCLLENSCALE (1.5f) // 文字フォントサイズ
#define PAUSE_SCLLENSCALE1 (1.0f) // 文字フォントサイズ
#define PAUSE_SCLLENSCALE2 (15.0f) // 文字フォントサイズ
#define PAUSE_SCLLENSCALE3 (2.5f) // 文字フォントサイズ

//=================================================================
// 構造体
// ポーズデータ格納
typedef struct _PAUSSE
{
	POLYGON List; // リスト
	INTTIM Time; // タイマー
}PAUSSE;

//=================================================================
// ポーズ
class CPause
{
private:
	// ポーズフォント
	PAUSSE Font(PAUSSE aPause);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	bool Update(void);

	// 描画開始
	void Draw(void);
};

#endif // !_PAUSE_H_
