//=================================================================
// タイマー[Timer.h]
// author: tatuya ogawa
//=================================================================

#ifndef _TIMER_H_ // インクルードガード
#define _TIMER_H_

//=================================================================
// マクロ定義
#define _NUM_TIMER_ (1) // タイマーの数
#define TIMER_SCALE (1.0f)
#define OneSecond (60)

//=================================================================
// タイマー構造体
typedef struct _TIMER
{
	POLYGON List; // リスト構造体
	INTTIM Tim; // タイマー
	INTTIM Comma; // コンマ
	NUMBER Number; // ナンバー
}TIMER;

//=================================================================
// タイマー
class CTimer
{
private:
	// タイマーの処理
	TIMER Timer(TIMER Timer);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(bool ControlStart, bool ControlEnd, bool ControlOver);

	// 描画開始
	void Draw(void);

	// 全パラメーター取得
	TIMER Get(void);
};

#endif // !_TIMER_H_
