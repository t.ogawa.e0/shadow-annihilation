//=================================================================
// 情報[Information.h]
// author: tatuya ogawa
//=================================================================

#ifndef _INFORMATION_H_ // インクルードガード
#define _INFORMATION_H_

//=================================================================
// マクロ定義
#define NUM_INFORMATION (6)
#define FONT_MOVE_SPPED (50.0f)
#define TIINFORMATION_COUNT (70)
#define INFO_MOVELIMIT (1500.0f)

//=================================================================
// 情報の構造体
typedef struct _INFORMATION
{
	POLYGON List; // リスト構造体 
	INTTIM Time; // タイマー
	float fLimitPos; // 移動制限
	bool bDraw; // 描画判定
	bool bUpdate; // 更新判定
	bool Enter; // カウンタ
}INFORMATION;

//=================================================================
// 情報
class CInformation
{
private:
	// カウントダウンフォント
	INFORMATION InfoFont00(INFORMATION Info);

	// 撃破数フォント
	INFORMATION InfoFont01(INFORMATION Info, bool InfoFont00);

	// WIN & LOUS
	INFORMATION InfoFont02_03(INFORMATION Info);

	void InfoFont04Draw(void);

	INFORMATION InfoFont5(INFORMATION Info);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	void InfoFont02Draw(void);

	void InfoFont03Draw(void);

	bool GetEnter(void);
};

#endif // !_INFORMATION_H_
