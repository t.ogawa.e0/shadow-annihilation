//=================================================================
// ゲーム処理[Game.cpp]
// author: tatuya ogawa
//=================================================================

#include <time.h>
#include "main.h"
#include "Game.h"
#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "IncludeData\sound.h"
#include "MovePosData.h"
#include "SceneChange.h"
#include "input.h"
#include "Background.h"
#include "Player.h"
#include "map.h"
#include "Cursor.h"
#include "Arrow.h"
#include "Debug.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Distance.h"
#include "Judgment.h"
#include "LifeProcessing.h"
#include "Explosion.h"
#include "Number.h"
#include "Timer.h"
#include "Restriction.h"
#include "Information.h"
#include "Countdown.h"
#include "Counter.h"
#include "Pause.h"
#include "Effect.h"

//=================================================================
// グローバル変数
static GAME *g_aGame;

//-----------------------------------------------------------------
// 初期化
void InitGame(void)
{
	CGame game;
	CPlayer player;
	CBackGround background;
	CMap map;
	CCursor cursor;
	CArrow arrow;
	CMovePosData MovePos;
	CEnemy enemy;
	CBullet bullet;
	CJudgment judgment;
	CLife lifeber;
	CExplosion explosion;
	CTimer timer;
	CRestriction restriction;
	CInformation info;
	CCountdown countdown;
	CCounter counter;
	CPause pause;
	CEffect effect;

	srand((unsigned)time(NULL));
	game.Init();
	MovePos.Init();
	map.Init();
	background.Init();
	arrow.Init();
	player.Init();
	enemy.Init();
	cursor.Init();
	bullet.Init();
	effect.Init();
	judgment.Init();
	explosion.Init();
	lifeber.Init();
	info.Init();
	countdown.Init();
	timer.Init();
	restriction.Init();
	counter.Init();
	pause.Init();
	PlaySound(GAME_SOUND_LIST::GAME_ElectroPeaceful);

#if defined(_DEBUG) || defined(DEBUG)
	_CDebug debug;
	debug.Init();
#endif
}

//-----------------------------------------------------------------
// 終了処理
void UninitGame(void)
{
	CGame game;
	CPlayer player;
	CBackGround background;
	CMap map;
	CCursor cursor;
	CArrow arrow;
	CMovePosData MovePos;
	CEnemy enemy;
	CBullet bullet;
	CJudgment judgment;
	CLife lifeber;
	CExplosion explosion;
	CTimer timer;
	CRestriction restriction;
	CInformation info;
	CCountdown countdown;
	CCounter counter;
	CPause pause;
	CEffect effect;

	game.Uninit();
	MovePos.Uninit();
	background.Uninit();
	map.Uninit();
	player.Uninit();
	cursor.Uninit();
	arrow.Uninit();
	enemy.Uninit();
	bullet.Uninit();
	effect.Uninit();
	judgment.Uninit();
	explosion.Uninit();
	lifeber.Uninit();
	info.Uninit();
	countdown.Uninit();
	timer.Uninit();
	restriction.Uninit();
	pause.Uninit();

	counter.Uninit();
#if defined(_DEBUG) || defined(DEBUG)
	_CDebug debug;
	debug.Uninit();
#endif
}

//-----------------------------------------------------------------
// 更新処理
void UpdateGame(void)
{
	CGame game;
	CPlayer player;
	CMap map;
	CCursor cursor;
	CArrow arrow;
	CMovePosData MovePos;
	CEnemy enemy;
	CBullet bullet;
	CJudgment judgment;
	CLife lifeber;
	CExplosion explosion;
	CTimer timer;
	CInformation info;
	CCountdown countdown;
	CCounter counter;
	CPause pause;
	CEffect effect;

	GAME aGame = game.Update();

	if (pause.Update() != B_ON)
	{
		MovePos.Update();
		cursor.Update();
		player.Update(aGame.ControlStart);
		enemy.Update(aGame.ControlStart, aGame.ControlEnd);
		arrow.Update();
		map.Update(aGame.ControlStart);
		bullet.Update();
		effect.Update();
		judgment.Update(aGame.ControlStart);
		explosion.Update();
		info.Update();
		countdown.Update();
		lifeber.Update();
		timer.Update(aGame.ControlStart, aGame.ControlEnd, aGame.ControlOver);
		counter.Update();
	}
	
#if defined(_DEBUG) || defined(DEBUG)
	_CDebug debug;
	debug.Update();
#endif
}

//-----------------------------------------------------------------
// 描画開始
void DrawGame(void)
{
	CGame game;
	CMovePosData MovePos;
	CBackGround background;
	CPlayer player;
	CMap map;
	CCursor cursor;
	CArrow arrow;
	CEnemy enemy;
	CBullet bullet;
	CJudgment judgment;
	CLife lifeber;
	CExplosion explosion;
	CTimer timer;
	CInformation info;
	CCountdown countdown;
	CCounter counter;
	CPause pause;
	CEffect effect;

	game.Draw();
	MovePos.Draw();
	background.Draw();
	map.Draw();
	enemy.Draw();
	lifeber.Draw(ENEMYID);
	player.Draw();
	bullet.Draw();
	effect.Draw();
	arrow.Draw();
	cursor.Draw();
	judgment.Draw();
	explosion.Draw();
	info.Draw();
	countdown.Draw();
	lifeber.Draw(PLAYERID);
	timer.Draw();
	counter.Draw();
	pause.Draw();
#if defined(_DEBUG) || defined(DEBUG)
	_CDebug debug;
	debug.GameDraw();
#endif
}

//-----------------------------------------------------------------
// 初期化
void CGame::Init(void)
{
	g_aGame = MemoryAllocation(g_aGame, NUMBERPALETTE_1);
	
	g_aGame[0].Control = false;
	g_aGame[0].ControlStart = true;
	g_aGame[0].ControlEnd = false;
	g_aGame[0].ControlOver = false;
}

//-----------------------------------------------------------------
// 終了処理
void CGame::Uninit(void)
{
	AllMemoryDelete(g_aGame);
}

//-----------------------------------------------------------------
// 更新処理
GAME CGame::Update(void)
{
	g_aGame[0] = Start(g_aGame[0]);
	g_aGame[0] = End(g_aGame[0]);
	g_aGame[0] = Over(g_aGame[0]);

	return g_aGame[0];
}

//-----------------------------------------------------------------
// 描画開始
void CGame::Draw(void)
{

}

//-----------------------------------------------------------------
// ゲームスタート演出
GAME CGame::Start(GAME aGame)
{
	if (aGame.ControlStart == true)
	{
		CCountdown countdown;
		if (countdown.Get().Timer.Tim.Count <= countdown.Get().Timer.Tim.End)
		{
			aGame.ControlStart = false;
		}
	}

	return aGame;
}

//-----------------------------------------------------------------
// ゲーム終了演出 Ready to   Move On
GAME CGame::End(GAME aGame)
{
	if (aGame.ControlStart == false)
	{
		CTimer timer;

		if (timer.Get().Tim.Count <= timer.Get().Tim.End)
		{
			CInformation Info;
			CScene scene;

			Info.InfoFont02Draw();
			aGame.ControlEnd = true;
			if (Info.GetEnter())
			{
				if (_TRIGGER_(DIK_RETURN))
				{
					scene.Change(SCENE_RESULT);
				}
			}
		}
	}
	
	return aGame;
}

//-----------------------------------------------------------------
// ゲームオーバー演出
GAME CGame::Over(GAME aGame)
{
	if (aGame.ControlStart == false)
	{
		CPlayer player;

		if (player.Get().Palameter.Life.Current <= I_NULL)
		{
			CInformation Info;
			CScene scene;

			Info.InfoFont03Draw();
			aGame.ControlOver = true;
			if (Info.GetEnter())
			{
				if (_TRIGGER_(DIK_RETURN))
				{
					scene.Change(SCENE_RESULT);
				}
			}
		}
	}

	return aGame;
}