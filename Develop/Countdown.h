//=================================================================
// カウントダウン[Countdown.h]
// author: tatuya ogawa
//=================================================================

#ifndef _COUNTDOWN_H_ // インクルードガード
#define _COUNTDOWN_H_

//=================================================================
// マクロ定義
#define NUM_COUNTDOWN (1) // カウンタの数
#define COUNTDOWN_SCALE (1.0f) // カウンタのスケール
#define COUNTDOWN_TIME (5) // カウントダウンの秒数

//=================================================================
// カウントダウン構造体
typedef struct _COUNTDOWN
{
	TIMER Timer; // タイマー
	bool bDraw; // 描画判定
	bool bUpdate; // 更新判定
}COUNTDOWN;

//=================================================================
// カウントダウン
class CCountdown
{
private:
	// タイマーの処理
	COUNTDOWN Countdown(COUNTDOWN aCountdown);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 全パラメーター取得
	COUNTDOWN Get(void);
};

#endif // !_COUNTDOWN_H_
