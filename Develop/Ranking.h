//=================================================================
// ランキングヘッダ[Ranking.h]
// author: tatuya ogawa
//=================================================================

#ifndef _RANKING_H_ // インクルードガード
#define _RANKING_H_

#include "Number.h"

//=================================================================
// マクロ定義
#define NAMELIMIT (6) // 名前数
#define RANKING_SPPED (8) // スクロール速度

//=================================================================
// 構造体
// ランキングデータ格納用構造体
typedef struct _RANKINGDATA
{
	int Score; // スコア
	int Ranking; // 順位
	int NameCase[NAMELIMIT]; // 名前
}RANKINGDATA;

// ランキング描画用ポリゴンデータ格納
typedef struct _RANKINGFONT
{
	POLYGON NameFont[NAMELIMIT]; // 名前描画用
	POLYGON ScoreFont; // スコアフォント
	POLYGON RankingFont; // 順位フォント
	POLYGON BontFont; // 撃破数フォント
	POLYGON RankingNumBer; // 名前フォント
	NUMBER NunBer; // 数字
}RANKINGFONT;

// 名前入力用領域
typedef struct _NAMEWINDOW
{
	POLYGON NameFont[NAMELIMIT]; // 名前描画用
	POLYGON NameWindow; // 名前入力ウィンドウ
	POLYGON YourName; // ネームフォント
	POLYGON OKQUESTION; // OK?画面
	POLYGON OK; // OKフォント
	POLYGON NO; // NOフォント
}NAMEWINDOW;

//=================================================================
// ランキング
class CRanking
{
private:
	// データの読み取り
	void Load(void);

	// データの保存
	void Save(void);

	// スクロール
	int Scroll(void);
public:
	// 初期化
	void Init(void);

	// 描画のための初期化
	void DrawInit(void);

	// 名前入力ウィンドウ生成
	void NameWindowInit(void);

	// 終了処理
	void Uninit(void);

	// 順位更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 描画開始
	void Draw3(void);

	// ランキングウィンドウスクロール
	void WindowScroll(void);

	// 限定描画
	RANKINGDATA Draw2(void);

	// 得点格納
	void GetPoint(int Point);
	
	// 名前入力
	bool SetName(void);
};

#endif // !_RANKING_H_
