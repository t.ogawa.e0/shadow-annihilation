//=================================================================
// シーン変更[SceneChange.h]
// author: 
//=================================================================

#ifndef _SCENECHANGE_H_ // インクルードガード
#define _SCENECHANGE_H_

#include "IncludeData\EnvironmentalSetting.h"

//=================================================================
// シーンリスト
typedef enum _SCENE_ID
{
	SCENE_NONE	 = level::levelNONE,
	SCENE_LOAD	 = level::level00,
	SCENE_TITLE  = level::level01,
	SCENE_HOME	 = level::level02,
	SCENE_GAME	 = level::level03,
	SCENE_RESULT = level::level04,
	SCENE_MAX
}SCENE_ID;

//=================================================================
// マクロ定義
#define START_SCENE (SCENE_ID::SCENE_TITLE)

//=================================================================
// シーン
class CScene
{
private:
public:
	// 初期化
	bool Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	bool Update(void);

	// 描画処理
	void Draw(void);

	// シーンの変更
	SCENE_ID Change(SCENE_ID id);

	// 再度展開
	void Retry(bool ON);
};

#endif // !_SCENECHANGE_H_
