//=================================================================
// マウス処理[Mouse.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "Mouse.h"

//=================================================================
// グローバル変数
static POINT g_mousePos;
static HWND g_hWnd;

//-----------------------------------------------------------------
// 初期化
void CMouse::Init(HWND hWnd)
{
	g_hWnd = hWnd;
	g_mousePos.x = g_mousePos.y = L_NULL;
}

//-----------------------------------------------------------------
// 終了処理
void CMouse::Uninit(void)
{
	g_hWnd = NULL;
}

//-----------------------------------------------------------------
// 更新処理
POINT CMouse::Update(void)
{
	GetCursorPos(&g_mousePos);
	ScreenToClient(g_hWnd, &g_mousePos);

	return g_mousePos;
}

//-----------------------------------------------------------------
// 描画処理
void CMouse::Draw(void)
{
	
}

//-----------------------------------------------------------------
// カーソルの座標
FLOAT_List CMouse::Cursor(float w, float h)
{
	FLOAT_List aCursor;

	aCursor.x = (float)Update().x;
	aCursor.y = (float)Update().y;
	aCursor.w = w;
	aCursor.h = h;

	aCursor.x -= aCursor.w / CENTER_POS;
	aCursor.y -= aCursor.h / CENTER_POS;

	return aCursor;
}

//-----------------------------------------------------------------
// 左クリック
SHORT CMouse::LeftClick(void)
{
	return GetAsyncKeyState(VK_LBUTTON);
}

//-----------------------------------------------------------------
// 右クリック
SHORT CMouse::RightClick(void)
{
	return GetAsyncKeyState(VK_RBUTTON);
}