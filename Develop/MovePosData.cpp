//=================================================================
// 各種座標操作用[MovePosData.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "main.h"
#include "MovePosData.h"
#include "Map.h"
#include "Player.h"

//=================================================================
// グローバル変数
static MOVEPOSDATA *g_aPosData;

//-----------------------------------------------------------------
// 初期化
void CMovePosData::Init(void)
{
	g_aPosData = MemoryAllocation(g_aPosData, NUMMAP);

	g_aPosData[0].aMapData.StartX = g_aPosData[0].aMapData.StartY = -(_MAPDATA_ / CENTER_POS); // 判定用マップサイズの割り出し
	g_aPosData[0].aMapData.EndX = g_aPosData[0].aMapData.EndY = +(_MAPDATA_ / CENTER_POS); // 判定用マップサイズの割り出し
	g_aPosData[0].aMapPos.x = g_aPosData[0].aMapPos.y = F_NULL;
}

//-----------------------------------------------------------------
// 終了処理
void CMovePosData::Uninit(void)
{
	AllMemoryDelete(g_aPosData);
}

//-----------------------------------------------------------------
// 更新処理
void CMovePosData::Update(void)
{
	CPlayer player;

	SPEED aMapMove = MapMove(player.Get().Palameter.Speed, g_aPosData[0].aMapData, player.Get().List.Poly, g_aPosData[0].aMapPos);
	g_aPosData[0].aMove = aMapMove;
}

//-----------------------------------------------------------------
// 描画処理
void CMovePosData::Draw(void)
{

}

//-----------------------------------------------------------------
// マップ移動座標算出
SPEED CMovePosData::MapMove(SPEED Spped, MAPDATA aData, FLOAT_List aPlayer, SPEED aMap)
{
	// マップ座標に速度渡し
	aMap.x += Spped.x;
	aMap.y += Spped.y;

	// 位置修正
	if (aMap.x < aData.StartX + aPlayer.w)
	{
		aMap.x -= Spped.x;
		Spped.x = F_NULL;
	}

	if (aData.EndX - aPlayer.w < aMap.x)
	{
		aMap.x -= Spped.x;
		Spped.x = F_NULL;
	}

	if (aMap.y < aData.StartY)
	{
		aMap.y -= Spped.y;
		Spped.y = F_NULL;
	}

	if (aData.EndY - aPlayer.h < aMap.y)
	{
		aMap.y -= Spped.y;
		Spped.y = F_NULL;
	}
	SetMapPos(aMap);

	return Spped;
}

//-----------------------------------------------------------------
// マップの座標更新
void CMovePosData::SetMapPos(SPEED aMap)
{
	g_aPosData[0].aMapPos = aMap;
}

//-----------------------------------------------------------------
// 全パラメーター取得
MOVEPOSDATA CMovePosData::Get(void)
{
	return g_aPosData[0];
}