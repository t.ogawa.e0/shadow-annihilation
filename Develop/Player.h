//=================================================================
// プレイヤー処理[Player.h]
// author: tatuya ogawa
//=================================================================

#ifndef _PLAYER_H_ // インクルードガード
#define _PLAYER_H_

//=================================================================
// マクロ定義
#define _NUM_PLAYER_		(1) // プレイヤー数
#define _PLAYER_LIFE_		(300) // プレイヤーのHP
#define _PLAYER_ATTAK_		(9) // プレイヤーの攻撃力
#define _PLAYER_ANIM_		(3) // アニメーション数
#define _PLAYER_ANIM_SPEED_	(3) // アニメーション速度
#define _PLAYER_SCALE_		(5.5f) // プレイヤーのスケール
#define _PLAYER_SPEED_		(20.0f)  // プレイヤーの速度
#define _MOBILITY_			(300.0f)

//=================================================================
// 構造体
// プレイヤーの構造体
typedef struct _PLAYER
{
	POLYGON List; // 構造体リスト
	FLOAT_List aMapPos; // マップ座標
	FLOAT_List aDataPos; // データ上の座標
	PARAMETERLIST Palameter; // パラメーターリスト
	int ID; // プレイヤーID
	bool bEmergence; // 出現中
}PLAYER;

//=================================================================
// プレイヤー
class CPlayer
{
private:
	// 移動速度
	PLAYER MoveSpeed(PLAYER aPlayer, int Key[], float MoveSpped);

	// 画面上での動き
	PLAYER Move(PLAYER aPlayer);
	
	// マップ上での動き
	PLAYER MapMove(PLAYER aPlayer, MAPDATA aMapData);

	// データ上の座標
	PLAYER DataMove(PLAYER aPlayer, MAPDATA aMapData);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(bool ControlStart);

	// 描画処理
	void Draw(void);

	// 全パラメーター取得
	PLAYER Get(void);

	// ダメージ渡し
	void Damage(bool Hit, ATTACK Attack);
};

#endif // !_PLAYER_H_
