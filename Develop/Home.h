//=================================================================
// ホーム処理[Home.h]
// author: tatuya ogawa 
//=================================================================

#ifndef _HOME_H_ // インクルードガード
#define _HOME_H_

// 初期化
void InitHome(void);

// 終了処理
void UninitHome(void);

// 更新処理
void UpdateHome(void);

// 描画開始
void DrawHome(void);
#endif // !_HOME_H_
