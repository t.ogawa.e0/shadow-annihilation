//=================================================================
// プレイヤー処理[Player.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "MovePosData.h"
#include "input.h"
#include "main.h"
#include "Player.h"
#include "Keyboard.h"
#include "Map.h"
#include "Cursor.h"
#include "Bullet.h"
#include "LifeProcessing.h"
#include "Explosion.h"
#include "Mouse.h"

//=================================================================
// グローバル変数
static PLAYER *g_aPlayer; // プレイヤーデータ

//-----------------------------------------------------------------
// 初期化
void CPlayer::Init(void)
{
	CPolygon polygon;

	g_aPlayer = MemoryAllocation(g_aPlayer, _NUM_PLAYER_);
	
	// 共通の初期化
	g_aPlayer[0].List = polygon.PolygonDataInitList();

	// プレイヤーの初期化
	g_aPlayer[0].List.TexName = GAME_TEXTURE_LIST::TEXTURE_RedCar;
	g_aPlayer[0].List.Poly.w = ((float)polygon.GetTextureInfo(g_aPlayer[0].List.TexName).Widht / _PLAYER_SCALE_);
	g_aPlayer[0].List.Poly.h = ((float)polygon.GetTextureInfo(g_aPlayer[0].List.TexName).Height / _PLAYER_SCALE_);
	g_aPlayer[0].List.Poly.x = (float)(SCREEN_WIDTH / CENTER_SEARCH) - g_aPlayer[0].List.Poly.w;
	g_aPlayer[0].List.Poly.y = (float)(SCREEN_HEIGHT / CENTER_SEARCH) - (g_aPlayer[0].List.Poly.h / CENTER_POS);
	g_aPlayer[0].List.Poly.y += 14.0f / 1.5f;
	g_aPlayer[0].List.Ang.x = (g_aPlayer[0].List.Poly.w / CENTER_POS);
	g_aPlayer[0].List.Ang.y = (g_aPlayer[0].List.Poly.h / CENTER_POS);
	g_aPlayer[0].List.bAffine = B_ON;

	// プレイヤーのマップデータ初期化
	g_aPlayer[0].aMapPos = g_aPlayer[0].aDataPos = g_aPlayer[0].List.Poly;

	g_aPlayer[0].Palameter.Life.Current = g_aPlayer[0].Palameter.Life.Max = _PLAYER_LIFE_;
	g_aPlayer[0].Palameter.Attack.Power = _PLAYER_ATTAK_;
	g_aPlayer[0].ID = PLAYERID;
	g_aPlayer[0].bEmergence = true;
}

//-----------------------------------------------------------------
// 終了処理
void CPlayer::Uninit(void)
{
	AllMemoryDelete(g_aPlayer);
}

//-----------------------------------------------------------------
// 更新処理
void CPlayer::Update(bool ControlStart)
{
	CMovePosData move;
	CKey key;
	CLife life;
	CMouse mouse;

	// 処理有効フラグ
	if (ControlStart != true)
	{
		g_aPlayer[0] = MoveSpeed(g_aPlayer[0], key.Get().Key, _PLAYER_SPEED_);
		g_aPlayer[0] = MapMove(g_aPlayer[0], move.Get().aMapData);
		g_aPlayer[0] = DataMove(g_aPlayer[0], move.Get().aMapData);

		// プレイヤー出現時
		if (g_aPlayer[0].bEmergence != false)
		{
			g_aPlayer[0].List.Ang.Angle = key.Direction(key.Get());

			// スペースキーを押したとき
			if (_PRESS_(DIK_SPACE) || mouse.LeftClick() || mouse.RightClick())
			{
				CCursor cursor;
				CBullet bullet;
				bullet.Create(g_aPlayer[0].List.Poly, cursor.Get().List.Poly, g_aPlayer[0].ID, g_aPlayer[0].Palameter.Attack);
			}

			// HPが0になったとき
			if (g_aPlayer[0].Palameter.Life.Current <= 0)
			{
				CExplosion explosion;

				explosion.Set(g_aPlayer[0].List, _COMMON_EXPLOSION_, true);
				g_aPlayer[0].bEmergence = false;
			}
		}
	}
	life.SetPlayerLife(g_aPlayer[0].Palameter.Life);
}

//-----------------------------------------------------------------
// 描画処理
void CPlayer::Draw(void)
{
	CPolygon polygon;

	// プレイヤー出現時
	if (g_aPlayer[0].bEmergence != false)
	{
		polygon.Draw(g_aPlayer[0].List);
	}
}

//-----------------------------------------------------------------
// 移動速度
PLAYER CPlayer::MoveSpeed(PLAYER aPlayer, int Key[], float MoveSpped)
{
	aPlayer.Palameter.Speed.x = aPlayer.Palameter.Speed.y = F_NULL;

	// プレイヤー出現時
	if (aPlayer.bEmergence != false)
	{
		CKey key;
		KEYFLOAT aKeyboard = key.Case(Key);

		aPlayer.Palameter.Speed.x = _MULTIPLIED_(aKeyboard.x, MoveSpped);
		aPlayer.Palameter.Speed.y = _MULTIPLIED_(aKeyboard.y, MoveSpped);
	}

	return aPlayer;
}

//-----------------------------------------------------------------
// プレイヤーの動き
PLAYER CPlayer::Move(PLAYER aPlayer)
{
	KEYFLOAT aScreenMoveLimit[2];

	// 画面上の移動制限の生成
	aScreenMoveLimit[0].x = ((float)SCREEN_WIDTH / CENTER_POS) / CENTER_POS;
	aScreenMoveLimit[1].x = ((float)SCREEN_WIDTH / CENTER_POS) + aScreenMoveLimit[0].x;
	aScreenMoveLimit[0].y = ((float)SCREEN_HEIGHT / CENTER_POS) / CENTER_POS;
	aScreenMoveLimit[1].y = ((float)SCREEN_HEIGHT / CENTER_POS) + aScreenMoveLimit[0].y;

	// データマップの動き
	aPlayer.List.Poly.x += aPlayer.Palameter.Speed.x;
	aPlayer.List.Poly.y += aPlayer.Palameter.Speed.y;

	// データマップの移動制限
	if (aPlayer.List.Poly.x < aScreenMoveLimit[0].x)
	{
		aPlayer.List.Poly.x -= aPlayer.Palameter.Speed.x;
	}

	if (aScreenMoveLimit[1].x < (aPlayer.List.Poly.x + aPlayer.List.Poly.w))
	{
		aPlayer.List.Poly.x -= aPlayer.Palameter.Speed.x;
	}

	if (aPlayer.List.Poly.y <  aScreenMoveLimit[0].y)
	{
		aPlayer.List.Poly.y -= aPlayer.Palameter.Speed.y;
	}

	if (aScreenMoveLimit[1].y < (aPlayer.List.Poly.y + aPlayer.List.Poly.h))
	{
		aPlayer.List.Poly.y -= aPlayer.Palameter.Speed.y;
	}

	return aPlayer;
}

//-----------------------------------------------------------------
// マップ上での動き
PLAYER CPlayer::MapMove(PLAYER aPlayer, MAPDATA aMapData)
{
	// プレイヤー出現時
	if (aPlayer.bEmergence != false)
	{
		// データマップ上の動き
		aPlayer.aMapPos.x += aPlayer.Palameter.Speed.x;
		aPlayer.aMapPos.y += aPlayer.Palameter.Speed.y;

		// データマップの移動制限
		if (aPlayer.aMapPos.x < aMapData.StartX)
		{
			aPlayer.aMapPos.x -= aPlayer.Palameter.Speed.x;
		}

		if (aMapData.EndX < (aPlayer.aMapPos.x + aPlayer.aMapPos.w))
		{
			aPlayer.aMapPos.x -= aPlayer.Palameter.Speed.x;
		}

		if (aPlayer.aMapPos.y < aMapData.StartY)
		{
			aPlayer.aMapPos.y -= aPlayer.Palameter.Speed.y;
		}

		if (aMapData.EndY < (aPlayer.aMapPos.y + aPlayer.aMapPos.h))
		{
			aPlayer.aMapPos.y -= aPlayer.Palameter.Speed.y;
		}
	}

	return aPlayer;
}

//-----------------------------------------------------------------
// データ上の座標
PLAYER CPlayer::DataMove(PLAYER aPlayer, MAPDATA aMapData)
{
	// プレイヤー出現時
	if (aPlayer.bEmergence != false)
	{
		// データ上の座標の動き
		aPlayer.aDataPos.x += aPlayer.Palameter.Speed.x;
		aPlayer.aDataPos.y += aPlayer.Palameter.Speed.y;

		// 位置修正
		if (aPlayer.aMapPos.x < aMapData.StartX)
		{
			aPlayer.aDataPos.x -= aPlayer.Palameter.Speed.x;
		}

		if (aMapData.EndX < (aPlayer.aMapPos.x + aPlayer.aMapPos.w))
		{
			aPlayer.aDataPos.x -= aPlayer.Palameter.Speed.x;
		}

		if (aPlayer.aMapPos.y < aMapData.StartY)
		{
			aPlayer.aDataPos.y -= aPlayer.Palameter.Speed.y;
		}

		if (aMapData.EndY < (aPlayer.aMapPos.y + aPlayer.aMapPos.h))
		{
			aPlayer.aDataPos.y -= aPlayer.Palameter.Speed.y;
		}
	}

	return aPlayer;
}

//-----------------------------------------------------------------
// ダメージ渡し
void CPlayer::Damage(bool Hit, ATTACK Attack)
{
	// 当たり判定有効時
	if (Hit)
	{
		g_aPlayer[0].Palameter.Life.Current -= Attack.Power;
	}
}

//-----------------------------------------------------------------
// 全パラメーター取得
PLAYER CPlayer::Get(void)
{
	return g_aPlayer[0];
}