//=================================================================
// バレット[Bullet.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\NumBerPallet.h"
#include "MovePosData.h"
#include "Bullet.h"
#include "Keyboard.h"
#include "Restriction.h"
#include "RotationAngle.h"
#include "Effect.h"

//=================================================================
// デバッグ用カウンタ
#if defined(_DEBUG) || defined(DEBUG)
typedef struct _BULLETNUMCOUNT
{
	int NumCount;
	int LimitCount;
}BULLETNUMCOUNT;
#endif

//=================================================================
// グローバル変数
static BULLET *g_aBullet; // バレットデータ
static int *g_Control; // コントロール
#if defined(_DEBUG) || defined(DEBUG)
static BULLETNUMCOUNT *g_Debug; // デバッグ用カウンタ
#endif

//-----------------------------------------------------------------
// 初期化
void CBullet::Init(void)
{
	CPolygon polygon;

	g_aBullet = MemoryAllocation(g_aBullet, MAX_BULLET);
	g_Control = MemoryAllocation(g_Control, NUM_BULLETID);

	g_Control[PLAYERID] = g_Control[ENEMYID] = -NUMBERPALETTE_1;

	for (int i = 0; i < MAX_BULLET; i++)
	{
		g_aBullet[i].List = polygon.PolygonDataInitList();
		g_aBullet[i].Target = g_aBullet[i].List.Poly;
		g_aBullet[i].bBullet = false;
		g_aBullet[i].HitID = I_NULL;
		g_aBullet[i].Frame.Count = I_NULL;
		g_aBullet[i].Frame.End = BULLET_ENDFLAME;
		g_aBullet[i].List.bAffine = B_ON;
		g_aBullet[i].Speed.x = g_aBullet[i].Speed.y = F_NULL;
		g_aBullet[i].Damage.Power = I_NULL;
	}

#if defined(_DEBUG) || defined(DEBUG)
	g_Debug = MemoryAllocation(g_Debug, NUMBERPALETTE_1);
	g_Debug[0].NumCount = I_NULL;
#endif
}

//-----------------------------------------------------------------
// 終了処理
void CBullet::Uninit(void)
{
	AllMemoryDelete(g_aBullet);
	AllMemoryDelete(g_Control);
#if defined(_DEBUG) || defined(DEBUG)
	AllMemoryDelete(g_Debug);
#endif
}

//-----------------------------------------------------------------
// 更新処理
void CBullet::Update(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount = I_NULL;
#endif
	for (int i = 0; i < MAX_BULLET; i++)
	{
		if (g_aBullet[i].bBullet == true)
		{
			CMovePosData move;
			CEffect effect;

			g_aBullet[i] = MapMove(g_aBullet[i], move.Get().aMove);
			g_aBullet[i] = Process(g_aBullet[i]);
			effect.SetEffect(g_aBullet[i].List.Poly, g_aBullet[i].HitID);
			DebugNumCount();
		}
	}
}

//-----------------------------------------------------------------
// 描画開始
void CBullet::Draw(void)
{
	CRestriction restriction;
	
	for (int i = 0; i < MAX_BULLET; i++)
	{
		if (restriction.Draw(g_aBullet[i].List.Poly) == true)
		{
			if (g_aBullet[i].bBullet == true)
			{
				CPolygon polygon;

				polygon.Draw(g_aBullet[i].List, B_ON);
			}
		}
	}
}

//-----------------------------------------------------------------
// バレット処理
BULLET CBullet::Process(BULLET Bullet)
{
	Bullet.List.Poly.x += Bullet.Speed.x;
	Bullet.List.Poly.y += Bullet.Speed.y;
	Bullet.Frame.Count++;
	Bullet.List.Anim.Count++;
	if (Bullet.Frame.End <= Bullet.Frame.Count)
	{
		Bullet.bBullet = false;
	}

	return Bullet;
}

//-----------------------------------------------------------------
// 弾の生成
void CBullet::Create(FLOAT_List TargetA, FLOAT_List TargetB, int ID, ATTACK Attack)
{
	for (int i = 0; i < MAX_BULLET; i++)
	{
		if (g_aBullet[i].bBullet == false)
		{
			CPolygon polygon;
			
			g_aBullet[i].List = polygon.PolygonDataInitList();
			g_aBullet[i].List.Color.a = COLORPALLET_200;

			switch (ID)
			{
			case PLAYERID:
				if (g_Control[ID] == CONTROL_TIME)
				{
					g_aBullet[i] = Set(ID, i, GAME_TEXTURE_LIST::TEXTURE_PLAYER_BULLET, PLAYER_BULLET_ANIM, PLAYER_BULLET_SCALE, NUMBERPALETTE_3, TargetA, TargetB, Attack);
					g_Control[ID] = -NUMBERPALETTE_1;
				}
				break;
			case ENEMYID:
				if (g_Control[ID] == CONTROL_TIME)
				{
					g_aBullet[i] = Set(ID, i, GAME_TEXTURE_LIST::TEXTURE_ENEMY_BULLET, ENEMY_BULLET_ANIM, ENEMY_BULLET_SCALE, NUMBERPALETTE_1, TargetA, TargetB, Attack);
					g_Control[ID] = -NUMBERPALETTE_1;
				}
				break;
			default:
				break;
			}
			// 弾生成間隔制御
			if (ID == PLAYERID) { g_Control[ID]++; }
			if (ID == ENEMYID) { g_Control[ID]++; }
			break;
		}
	}
}

//-----------------------------------------------------------------
// バレットセット
BULLET CBullet::Set(int ID, int BulletID, int TexNum, int TexAnim, float Scale, int AnglePattan, FLOAT_List TargetA, FLOAT_List TargetB, ATTACK Attack)
{
	SPEED Differenc;
	CKey key;
	CPolygon polygon;
	CAngle angle;
	BULLET aBullet;

	aBullet.List = polygon.PolygonDataInitList();
	aBullet.List.Color.a = COLORPALLET_200;
	aBullet.List.TexName = TexNum;
	aBullet.List.Anim.Count = -NUMBERPALETTE_1;
	aBullet.List.Anim.Direction = TexAnim;
	aBullet.List.Anim.Frame = BULLET_FLAME;
	aBullet.List.Anim.Pattern = TexAnim;
	aBullet.List.Anim.AnimationON = B_ON;
	aBullet.List.TexCut.x = aBullet.List.TexCut.w = (polygon.GetTextureInfo(aBullet.List.TexName).Widht / TexAnim);
	aBullet.List.TexCut.y = aBullet.List.TexCut.h = polygon.GetTextureInfo(aBullet.List.TexName).Height;
	aBullet.List.Poly.w = (aBullet.List.TexCut.w / Scale);
	aBullet.List.Poly.h = (aBullet.List.TexCut.h / Scale);
	aBullet.List.Ang.x = aBullet.List.Scale.x = (aBullet.List.Poly.w / CENTER_POS);
	aBullet.List.Ang.y = aBullet.List.Scale.y = (aBullet.List.Poly.h / CENTER_POS);
	aBullet.List.Ang.Angle = angle.Angle2D(TargetA, TargetB);
	aBullet.List.Ang.Angle += key.Get().Angle[AnglePattan];
	aBullet.Damage.Power = Attack.Power;
	aBullet.List.bAffine = B_ON;
	aBullet.HitID = ID;
	aBullet.BulletID = BulletID;
	aBullet.List.Poly.x = TargetA.x + ((TargetA.w / CENTER_POS) - (aBullet.List.Poly.w / CENTER_POS));
	aBullet.List.Poly.y = TargetA.y;
	aBullet.Target = TargetB;
	aBullet.Frame.Count = I_NULL;
	aBullet.Frame.End = BULLET_ENDFLAME;
	Differenc.x = (TargetB.x - TargetA.x)* BULLET_SPEED;
	Differenc.y = (TargetB.y - TargetA.y)* BULLET_SPEED;
	aBullet.Speed = Differenc;
	aBullet.bBullet = true;

	return aBullet;
}

//-----------------------------------------------------------------
// 弾処理終了
void CBullet::EndProcess(int ID)
{
	g_aBullet[ID].bBullet = false;
}

//-----------------------------------------------------------------
// バレットの動き制御
BULLET CBullet::MapMove(BULLET Bullet, SPEED PosData)
{
	Bullet.List.Poly.x -= PosData.x;
	Bullet.List.Poly.y -= PosData.y;

	return Bullet;
}

//-----------------------------------------------------------------
// 全パラメーター取得
BULLET CBullet::Get(int Count)
{
	return g_aBullet[Count];
}

//-----------------------------------------------------------------
// 射出された弾数を返す
int CBullet::GetNumRelease(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	return g_Debug[0].NumCount;
#else
	return 0;
#endif
}

//-----------------------------------------------------------------
// デバッグ用弾カウンタ
void CBullet::DebugNumCount(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount++;
#endif
}