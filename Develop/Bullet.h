//=================================================================
// バレットBullet.h]
// author: tatuya ogawa
//=================================================================

#ifndef _BULLET_H_ // インクルードガード
#define _BULLET_H_

//=================================================================
// マクロ定義
#define MAX_BULLET (1000) // バレットの上限
#define BULLET_SSPEED (10.0f) // バレットの速度
#define PLAYERID (0) // プレイヤーのID
#define PLAYER_BULLET_ANIM (3) // バレットのアニメーション数
#define PLAYER_BULLET_SCALE (10.0f) // スケール調整
#define BULLET_TARGET_RANGE (1000)
#define ENEMYID (1) // エネミーのID
#define ENEMY_BULLET_ANIM (5) // バレットのアニメーション数
#define ENEMY_BULLET_SCALE (3.0f) // スケール調整
#define BULLET_ENDFLAME (100) // 弾の命
#define BULLET_FLAME (2) // 更新フレーム
#define NUM_BULLETID (2) // IDの数
#define CONTROL_TIME (3) // コントロール
#define BULLET_SPEED (0.1f) // 弾の速度

//=================================================================
// バレットの構造体
typedef struct _BULLET
{
	POLYGON List; // 構造体リスト
	FLOAT_List Target; // ターゲットの座標
	INTFRAME Frame; // カウンタ
	SPEED Speed; // 各方向へ対しての速度
	ATTACK Damage; // ダメージ
	int HitID; // 当たり判定識別ID
	int BulletID; // バレットID
	bool bBullet; // 判定
}BULLET;

//=================================================================
// バレット
class CBullet
{
private:
	// バレット処理
	BULLET Process(BULLET Bullet);

	// バレットの動き制御
	BULLET MapMove(BULLET Bullet, SPEED PosData);

	// デバッグ用弾カウンタ
	void DebugNumCount(void);

	// バレットセット
	BULLET Set(int ID, int BulletID, int TexNum, int TexAnim, float Scale, int AnglePattan, FLOAT_List TargetA, FLOAT_List TargetB, ATTACK Attack);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 弾の生成
	void Create(FLOAT_List TargetA, FLOAT_List TargetB, int ID, ATTACK Attack);

	// 弾処理終了
	void EndProcess(int ID);

	// 全パラメーター取得
	BULLET Get(int Count);

	// 射出された弾数を返す
	int GetNumRelease(void);
};

#endif // !_BULLET_H_
