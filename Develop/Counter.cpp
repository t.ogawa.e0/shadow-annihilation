//=================================================================
// カウンタ[Counter.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "main.h"
#include "Number.h"
#include "Counter.h"
#include "Ranking.h"

//=================================================================
// グローバル変数
static COUNTER *g_aCounter; // カウンタ

//-----------------------------------------------------------------
// 初期化
void CCounter::Init(void)
{
	CPolygon polygon;
	CNumber number;

	g_aCounter = MemoryAllocation(g_aCounter, NUMBERPALETTE_1);

	g_aCounter[0].List = polygon.PolygonDataInitList();
	g_aCounter[0].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_NumberList0;
	g_aCounter[0].List.Poly.x = F_NULL - (NUMBER_W * COUNTER_LINE);
	g_aCounter[0].List.Poly.y = F_NULL;
	g_aCounter[0].List.Anim.Count = -NUMBERPALETTE_1;
	g_aCounter[0].List.Anim.Direction = NUM_NUMBER_W;
	g_aCounter[0].List.Anim.Pattern = NUM_NUMBER;
	g_aCounter[0].List.Anim.AnimationON = B_ON;
	g_aCounter[0].List.TexCut.x = g_aCounter[0].List.TexCut.w = NUMBER_W;
	g_aCounter[0].List.TexCut.y = g_aCounter[0].List.TexCut.h = NUMBER_H;
	g_aCounter[0].List.Poly.w = (float)_MULTIPLIED_(g_aCounter[0].List.TexCut.w, COUNTER_SCALE);
	g_aCounter[0].List.Poly.h = (float)_MULTIPLIED_(g_aCounter[0].List.TexCut.h, COUNTER_SCALE);
	g_aCounter[0].List.Anim.Frame = NUMBERPALETTE_1;
	g_aCounter[0].List.bAffine = B_ON;
	g_aCounter[0].List.Ang.x = g_aCounter[0].List.Scale.x = _DIVISION_(g_aCounter[0].List.Poly.w, CENTER_POS);
	g_aCounter[0].List.Ang.y = g_aCounter[0].List.Scale.y = _DIVISION_(g_aCounter[0].List.Poly.h, CENTER_POS);	
	g_aCounter[0].Number.Digit = COUNTER_LINE;
	g_aCounter[0].Number.LeftAlignment = B_ON;
	g_aCounter[0].Number.Max = number.Init(g_aCounter[0].Number.Digit);
	g_aCounter[0].Number.Zero = B_ON;
	g_aCounter[0].Count = I_NULL;
}

//-----------------------------------------------------------------
// 終了処理
void CCounter::Uninit(void)
{
	AllMemoryDelete(g_aCounter);
}

//-----------------------------------------------------------------
// 更新処理
void CCounter::Update(void)
{

}

//-----------------------------------------------------------------
// 描画開始
void CCounter::Draw(void)
{
	CPolygon polygon;
	CNumber number;

	int aArea = polygon.StaticAreaStart();
	number.Draw(g_aCounter[0].List, g_aCounter[0].Number, g_aCounter[0].Count);
	polygon.StaticAreaEnd(aArea);
}

//-----------------------------------------------------------------
// 加算
void CCounter::Set(void)
{
	g_aCounter[0].Count++;
}

//-----------------------------------------------------------------
// ゲット関数
int CCounter::Get(void)
{
	return g_aCounter[0].Count;
}

//-----------------------------------------------------------------
// 移動速度
void CCounter::MoveCounter(float fSpeed)
{
	g_aCounter[0].List.Poly.x += fSpeed;
}