//=================================================================
// 爆発[Explosion.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "main.h"
#include "MovePosData.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Explosion.h"
#include "Restriction.h"

//=================================================================
// デバッグ用カウンタ
#if defined(_DEBUG) || defined(DEBUG)
typedef struct _EXPLOSIONNUMCOUNT
{
	int NumCount;
	int LimitCount;
}EXPLOSIONNUMCOUNT;
#endif

//=================================================================
// グローバル変数
static EXPLOSION *g_aExplosion;
#if defined(_DEBUG) || defined(DEBUG)
static EXPLOSIONNUMCOUNT *g_Debug; // デバッグ用カウンタ
#endif

//-----------------------------------------------------------------
// 初期化
void CExplosion::Init(void)
{
	CPolygon polygon;
	int nData = ((_NUM_PLAYER_ + _NUM_ENEMY_) + (MAX_BULLET / _EXDETA_));

	g_aExplosion = MemoryAllocation(g_aExplosion, nData);
	
	for (int i = 0; i < nData; i++)
	{
		g_aExplosion[i].List.TexName = I_NULL;
		g_aExplosion[i].List = polygon.PolygonDataInitList();
		g_aExplosion[i].bExplosion = false;
		g_aExplosion[i].Flame.Count = -NUMBERPALETTE_1;
		g_aExplosion[i].Flame.End = I_NULL;
		g_aExplosion[i].List.Anim.Count = -NUMBERPALETTE_1;
		g_aExplosion[i].List.Anim.Frame = NUMBERPALETTE_1;
		g_aExplosion[i].List.bAffine = B_ON;
	}
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug = MemoryAllocation(g_Debug, NUMBERPALETTE_1);
	g_Debug[0].LimitCount = nData;
	g_Debug[0].NumCount = I_NULL;
#endif
}

//-----------------------------------------------------------------
// 終了処理
void CExplosion::Uninit(void)
{
	AllMemoryDelete(g_aExplosion);
#if defined(_DEBUG) || defined(DEBUG)
	AllMemoryDelete(g_Debug);
#endif
}

//-----------------------------------------------------------------
// 更新処理
void CExplosion::Update(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount = I_NULL;
#endif
	for (int i = 0; i < ((_NUM_PLAYER_ + _NUM_ENEMY_) + (MAX_BULLET / _EXDETA_)); i++)
	{
		if (g_aExplosion[i].bExplosion != false)
		{
			CMovePosData move;

			g_aExplosion[i] = MapMove(g_aExplosion[i], move.Get().aMove);
			g_aExplosion[i] = Explosion(g_aExplosion[i]);
			DebugNumCount();
		}
	}
}

//-----------------------------------------------------------------
// 描画開始
void CExplosion::Draw(void)
{
	CRestriction restriction;
	
	for (int i = 0; i < ((_NUM_PLAYER_ + _NUM_ENEMY_) + (MAX_BULLET / _EXDETA_)); i++)
	{
		if (restriction.Draw(g_aExplosion[i].List.Poly) == true)
		{
			if (g_aExplosion[i].bExplosion != false)
			{
				CPolygon polygon;

				polygon.Draw(g_aExplosion[i].List, B_ON);
			}
		}
	}
}

//-----------------------------------------------------------------
// 爆発処理
EXPLOSION CExplosion::Explosion(EXPLOSION aExplosion)
{
	aExplosion.Flame.Count++;
	aExplosion.List.Anim.Count++;
	if (aExplosion.Flame.End == aExplosion.Flame.Count)
	{
		aExplosion.bExplosion = false;
	}

	return aExplosion;
}

//-----------------------------------------------------------------
// 爆発の動き制御
EXPLOSION CExplosion::MapMove(EXPLOSION aExplosion, SPEED PosData)
{
	aExplosion.List.Poly.x -= PosData.x;
	aExplosion.List.Poly.y -= PosData.y;

	return aExplosion;
}

//-----------------------------------------------------------------
// 爆発処理セット関数
void CExplosion::Set(POLYGON List, int ExplosionID, bool bHit)
{
	if (bHit)
	{
		for (int i = 0; i < ((_NUM_PLAYER_ + _NUM_ENEMY_) + (MAX_BULLET / _EXDETA_)); i++)
		{
			if (g_aExplosion[i].bExplosion != true)
			{
				CPolygon polygon;

				g_aExplosion[i].List = polygon.PolygonDataInitList();
				g_aExplosion[i].List.Poly.x = List.Poly.x;
				g_aExplosion[i].List.Poly.y = List.Poly.y;
				g_aExplosion[i].Flame.Count = -NUMBERPALETTE_1;
				g_aExplosion[i].List.Anim.Count = -NUMBERPALETTE_1;

				switch (ExplosionID)
				{
				case _PLAYER_EXPLOSION_: // プレイヤーの弾
					g_aExplosion[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_PLAYERBULLETEFFECT;
					g_aExplosion[i].List.Anim.Direction = _BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Anim.Pattern = _BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Anim.AnimationON = B_ON;
					g_aExplosion[i].List.Anim.Frame = NUMBERPALETTE_5;
					g_aExplosion[i].List.TexCut.x = g_aExplosion[i].List.TexCut.w = (polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Widht / _BULLETEFFECT_ANIMATION_);
					g_aExplosion[i].List.TexCut.y = g_aExplosion[i].List.TexCut.h = polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Height;
					g_aExplosion[i].List.Poly.w = (float)(g_aExplosion[i].List.TexCut.w / _BULLETEFFECT_SCALE);
					g_aExplosion[i].List.Poly.h = (float)(g_aExplosion[i].List.TexCut.h / _BULLETEFFECT_SCALE);
					g_aExplosion[i].Flame.End = g_aExplosion[i].List.Anim.Frame*_BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Ang = List.Ang;
					g_aExplosion[i].List.bAffine = B_ON;
					break;
				case _ENEMY_EXPLOSION_: // エネミーの弾
					g_aExplosion[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_ENEMYBBULLETEFFECT;
					g_aExplosion[i].List.Anim.Direction = _BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Anim.Pattern = _BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Anim.AnimationON = B_ON;
					g_aExplosion[i].List.Anim.Frame = NUMBERPALETTE_5;
					g_aExplosion[i].List.TexCut.x = g_aExplosion[i].List.TexCut.w = (polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Widht / _BULLETEFFECT_ANIMATION_);
					g_aExplosion[i].List.TexCut.y = g_aExplosion[i].List.TexCut.h = polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Height;
					g_aExplosion[i].List.Poly.w = (float)(g_aExplosion[i].List.TexCut.w / _BULLETEFFECT_SCALE);
					g_aExplosion[i].List.Poly.h = (float)(g_aExplosion[i].List.TexCut.h / _BULLETEFFECT_SCALE);
					g_aExplosion[i].Flame.End = g_aExplosion[i].List.Anim.Frame*_BULLETEFFECT_ANIMATION_;
					g_aExplosion[i].List.Ang = List.Ang;
					g_aExplosion[i].List.bAffine = B_ON;
					break;
				case _COLLISION: // 互が接触時の弾
					g_aExplosion[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_BC59;
					g_aExplosion[i].List.TexCut.x = g_aExplosion[i].List.TexCut.w = polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Widht;
					g_aExplosion[i].List.TexCut.y = g_aExplosion[i].List.TexCut.h = polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Height;
					g_aExplosion[i].List.Poly.w = (float)(g_aExplosion[i].List.TexCut.w / _Spark_SCALE);
					g_aExplosion[i].List.Poly.h = (float)(g_aExplosion[i].List.TexCut.h / _Spark_SCALE);
					g_aExplosion[i].Flame.End = _Spark_FLAME;
					g_aExplosion[i].List.Ang = List.Ang;
					g_aExplosion[i].List.Anim.AnimationON = B_OFF;
					g_aExplosion[i].List.Ang.Angle;
					g_aExplosion[i].List.bAffine = B_OFF;
					break;
				case _COMMON_EXPLOSION_: // 爆発時
					g_aExplosion[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_EplosionAcolor_10F;
					g_aExplosion[i].List.Anim.Direction = (_ExplosionAcolor_ANIMATION_ / NUMBERPALETTE_2);
					g_aExplosion[i].List.Anim.Pattern = _ExplosionAcolor_ANIMATION_;
					g_aExplosion[i].List.Anim.AnimationON = B_ON;
					g_aExplosion[i].List.Anim.Frame = NUMBERPALETTE_2;
					g_aExplosion[i].List.TexCut.x = g_aExplosion[i].List.TexCut.w = (polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Widht / g_aExplosion[i].List.Anim.Direction);
					g_aExplosion[i].List.TexCut.y = g_aExplosion[i].List.TexCut.h = (polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Height / NUMBERPALETTE_2);
					g_aExplosion[i].List.Poly.w = (float)(g_aExplosion[i].List.TexCut.w / _ExplosionAcolor_SCALE);
					g_aExplosion[i].List.Poly.h = (float)(g_aExplosion[i].List.TexCut.h / _ExplosionAcolor_SCALE);
					g_aExplosion[i].List.Poly.x -= (g_aExplosion[i].List.Poly.w / CENTER_POS);
					g_aExplosion[i].List.Poly.y -= (g_aExplosion[i].List.Poly.h / CENTER_POS);
					g_aExplosion[i].Flame.End = g_aExplosion[i].List.Anim.Frame*_ExplosionAcolor_ANIMATION_;
					g_aExplosion[i].List.Ang = List.Ang;
					g_aExplosion[i].List.bAffine = B_ON;
					break;
				case _EMERGENCE_: // エンゲージエフェクト
					g_aExplosion[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_EmergenceEffect;
					g_aExplosion[i].List.Anim.Direction = _EmergenceEffect_ANIMATION_;
					g_aExplosion[i].List.Anim.Pattern = _EmergenceEffect_ANIMATION_;
					g_aExplosion[i].List.Anim.AnimationON = B_ON;
					g_aExplosion[i].List.Anim.Frame = NUMBERPALETTE_5;
					g_aExplosion[i].List.TexCut.x = g_aExplosion[i].List.TexCut.w = (polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Widht / _EmergenceEffect_ANIMATION_);
					g_aExplosion[i].List.TexCut.y = g_aExplosion[i].List.TexCut.h = polygon.GetTextureInfo(g_aExplosion[i].List.TexName).Height;
					g_aExplosion[i].List.Poly.w = (float)(g_aExplosion[i].List.TexCut.w / _EmergenceEffect_SCALE);
					g_aExplosion[i].List.Poly.h = (float)(g_aExplosion[i].List.TexCut.h / _EmergenceEffect_SCALE);
					g_aExplosion[i].Flame.End = g_aExplosion[i].List.Anim.Frame*_EmergenceEffect_ANIMATION_;
					g_aExplosion[i].List.Ang = List.Ang;
					g_aExplosion[i].List.Poly.x -= (g_aExplosion[i].List.Poly.w / CENTER_POS);
					g_aExplosion[i].List.Poly.y -= (g_aExplosion[i].List.Poly.h / CENTER_POS);
					g_aExplosion[i].List.bAffine = B_ON;
					break;
				default:
					break;
				}
				g_aExplosion[i].List.Ang.x = g_aExplosion[i].List.Scale.x = (g_aExplosion[i].List.Poly.w / CENTER_POS);
				g_aExplosion[i].List.Ang.y = g_aExplosion[i].List.Scale.y = (g_aExplosion[i].List.Poly.h / CENTER_POS);
				g_aExplosion[i].bExplosion = true;
				break;
			}
		}
	}
}

//-----------------------------------------------------------------
// エフェクトの数
int CExplosion::GetNumRelease(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	return g_Debug[0].NumCount;
#else
	return 0;
#endif
}

//-----------------------------------------------------------------
// エフェクトカウンタ
void CExplosion::DebugNumCount(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug[0].NumCount++;
#endif
}