//=================================================================
// ��������[Distance.h]
// author: tatuya ogawa
//=================================================================

#include <math.h>
#include "IncludeData\DirectX2D.h"
#include "Distance.h"

//-----------------------------------------------------------------
// �^�[�Q�b�g�Ƃ̋���(�񎟌�)
float CDistance::Distance2D(FLOAT_List TargetA, FLOAT_List TargetB)
{
	return powf((TargetB.x - TargetA.x)*(TargetB.x - TargetA.x) + (TargetB.y - TargetA.y)*(TargetB.y - TargetA.y), CORRECTION); // ��������o��
}

//-----------------------------------------------------------------
// �^�[�Q�b�g�Ƃ̋���(�O����)
float CDistance::Distance3D(FLOAT_List TargetA, FLOAT_List TargetB)
{
	return powf((TargetB.x - TargetA.x)*(TargetB.x - TargetA.x) + (TargetB.y - TargetA.y)*(TargetB.y - TargetA.y) + (TargetB.z - TargetA.z)*(TargetB.z - TargetA.z), CORRECTION); // �O����
}