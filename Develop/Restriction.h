//=================================================================
// 制限[Restriction.h]
// author: tatuya ogawa
//=================================================================

#ifndef _RESTRICTION_H_ // インクルードガード
#define _RESTRICTION_H_

//=================================================================
// 制限
class CRestriction
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画判定
	bool Draw(FLOAT_List Poly);
};

#endif // !_RESTRICTION_H_