//=================================================================
// 背景[Background.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "Background.h"

//=================================================================
// グローバル変数
static BACKGROUND *g_aBackGround; // 背景データ

//-----------------------------------------------------------------
// 初期化
void CBackGround::Init(void)
{
	CPolygon polygon;

	g_aBackGround = MemoryAllocation(g_aBackGround, NUMBACKGROUND);
	g_aBackGround[0].List = polygon.PolygonDataInitList();
	g_aBackGround[0].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Sky;
	g_aBackGround[0].List.Poly.w = (float)polygon.GetTextureInfo(g_aBackGround[0].List.TexName).Widht;
	g_aBackGround[0].List.Poly.h = (float)polygon.GetTextureInfo(g_aBackGround[0].List.TexName).Height;
	g_aBackGround[0].List.bAffine = B_OFF;
}

//-----------------------------------------------------------------
// 終了処理
void CBackGround::Uninit(void)
{
	AllMemoryDelete(g_aBackGround);
}

//-----------------------------------------------------------------
// 更新処理
void CBackGround::Update(void)
{

}

//-----------------------------------------------------------------
// 描画開始
void CBackGround::Draw(void)
{
	CPolygon polygon;

	polygon.Draw(g_aBackGround[0].List);
}

//-----------------------------------------------------------------
// 全パラメーター取得
BACKGROUND CBackGround::Get(void)
{
	return g_aBackGround[0];
}