//=================================================================
// ロード処理[Load.h]
// author: tatuya ogawa
//=================================================================

#ifndef _LOAD_H_ // インクルードガード
#define _LOAD_H_

//=================================================================
// マクロ定義
#define SCENESPEED (80.0f) // スクリーンの展開速度
#define LOADTIME (30) // ロード時間
#define ENDLOADTIME (90) // フェードロード時間
#define LOADTEXNAM (4) // ロードのテクスチャ数
#define LOAD_FONT (0.05f) // フォント回転速度

//=================================================================
// 構造体定義
typedef struct _LOAD
{
	POLYGON List; // ポリゴン
	bool bDrawLoad; // 描画判定
	bool bLoad; // ロード判定
	bool bUpdateScene; // 画面更新フラグ
	bool bFlagUpdateScene; // 制御フラグ更新判定
}LOAD;

//=================================================================
// ロード
class CLoad
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// フラグ取得
	bool GetFlag(void);

	// 描画フラグ
	bool GetFlagDraw(void);

	// 更新フラグ
	bool GetUpdate(void);

	// 第二更新フラグ
	bool GetFlagUpdate(void);

	// ロード画面
	bool UpdateFade1(void);

	// ロードフォント
	void UpdateFade2(bool bLoad);

	// ロードタイプ切り替え(フェードタイプ)
	void LoadTypeON(void);

	// ロードタイプ切り替え(シャッタータイプ)
	void LoadTypeOFF(void);
};

// 初期化
void InitLoad(void);

// 終了処理
void UninitLoad(void);

// 更新処理
void UpdateLoad(void);

// 更新処理
void DrawLoad(void);


#endif // !_LOAD_H_
