//=================================================================
// エネミー[Enemy.h]
// author: tatuya ogawa
//=================================================================

#ifndef _ENEMY_H_ // インクルードガード
#define _ENEMY_H_

//=================================================================
// マクロ定義
#define _NUM_ENEMY_			(20)
#define _EME_TIME_			(200)
#define _ENEMY_LIFE_		(50)
#define _ENEMY_ATTAK_		(4)
#define _ENEMY_SCALE_		(5.5f)
#define _ENEMY_SPEED_		(15.0f)
#define _SEARCH_SIZE_00		(20.0f)
#define _SEARCH_SIZE_01		(100.0f)
#define _SEARCH_			(1000.0f)
#define _ENEMY_RANGE_		(500.0f)
#define _ENEMY_SPPED_Lif_	(1.5f)

//=================================================================
// エネミーの構造体
typedef struct _ENEMY
{
	POLYGON List; // 構造体リスト
	MAPDATA Mapdata; // マップデータ
	ID MoveID; // 移動ID
	INTFRAME Frame; // フレーム
	PARAMETERLIST Palameter; // パラメーターリスト
	int Mode; // モード
	int ModePursuit; // 追跡パターン
	int ID; // エネミーID
	int EmergenceTime;  // りポップまでのカウント
	bool bAttack; // 攻撃判断
	bool bEmergence; // 出現中
}ENEMY;

//=================================================================
// エネミー
class CEnemy
{
private:
	// AI
	ENEMY AI(ENEMY aEnemy, PLAYER aPlayer);

	// AI頭脳
	ENEMY AIBrain(ENEMY aEnemy, PLAYER aPlayer);

	// AI動き制御
	ENEMY AISquence(ENEMY aEnemy);

	// 移動に必要な値をセット
	ENEMY SetMove(ENEMY aEnemy);

	// エネミーの動き
	ENEMY LimitMove(ENEMY aEnemy);

	// エネミーの動き制御
	ENEMY MapMove(ENEMY aEnemy, SPEED PosData);

	// エネミーの攻撃
	void Attack(ENEMY aEnemy, PLAYER aPlayer);

	void DebugNumCount(void);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(bool ControlStart, bool ControlEnd);

	// 描画開始
	void Draw(void);

	// 全パラメーター取得
	ENEMY Get(int NumEnemy);

	// ダメージ渡し
	void Damage(bool Hit, ATTACK Attack, int NumEnemy);

	// エネミーの出現数取得
	int GetNumRelease(void);
};

#endif // !_ENEMY_H_
