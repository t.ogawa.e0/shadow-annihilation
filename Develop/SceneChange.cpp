//=================================================================
// シーン変更[SceneChange.cpp]
// author: 
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\sound.h"
#include "SceneChange.h"
#include "Load.h"
#include "Title.h"
#include "Home.h"
#include "Game.h"
#include "Result.h"

//=================================================================
// グローバル変数
typedef void(*SceneFunc)(void); // シーンチェンジ関数ポインタ

// 初期化呼び出し
static const SceneFunc g_pInit[] =
{
	InitLoad,
	InitTitle,
	InitHome,
	InitGame,
	InitResult
};

// 終了処理呼び出し
static const SceneFunc g_pUninit[] =
{
	UninitLoad,
	UninitTitle,
	UninitHome,
	UninitGame,
	UninitResult
};

// 更新処理呼び出し
static const SceneFunc g_pUpdate[] =
{
	UpdateLoad,
	UpdateTitle,
	UpdateHome,
	UpdateGame,
	UpdateResult
};

// 描画処理呼び出し
static const SceneFunc g_pDraw[] =
{
	DrawLoad,
	DrawTitle,
	DrawHome,
	DrawGame,
	DrawResult
};

static SCENE_ID g_nID = START_SCENE;
static SCENE_ID g_nNextID = START_SCENE;
static bool g_Change;

//-----------------------------------------------------------------
// 初期化
bool CScene::Init(void)
{
	CPolygon polygon;

	StopAllSound();
	if (!UpdateSoundDynamicArea(g_nID)) { return false; }
	if (!polygon.UpdateDynamicArea(g_nID)) { return false; }
	g_pInit[g_nID]();
	g_Change = false;

	return true;
}

//-----------------------------------------------------------------
// 終了処理
void CScene::Uninit(void)
{
	g_pUninit[g_nID]();
}

//-----------------------------------------------------------------
// 更新処理
bool CScene::Update(void)
{
	CLoad load;

	if (load.GetFlagDraw() == false)
	{
		g_pUpdate[g_nID]();
	}

	if (load.GetUpdate() == true)
	{
		g_pUpdate[SCENE_ID::SCENE_LOAD]();
	}

	if (g_nNextID != g_nID|| g_Change == true)
	{
		g_pUpdate[SCENE_ID::SCENE_LOAD]();
		if (load.GetFlagUpdate() == true)
		{
			Uninit();
			g_nID = g_nNextID;
			if (!Init())
			{
				ErrorMessage("テクスチャの読み込み失敗(4)");
				return false;
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------
// 描画処理
void CScene::Draw(void)
{
	CLoad load;

	if (load.GetFlagDraw() == false)
	{
		g_pDraw[g_nID]();
	}

	if (load.GetFlag() == true)
	{
		g_pDraw[SCENE_ID::SCENE_LOAD]();
	}
}

//-----------------------------------------------------------------
// シーンの変更
SCENE_ID CScene::Change(SCENE_ID id)
{
	g_nNextID = id;
	return g_nID;
}

//-----------------------------------------------------------------
// 再度展開
void CScene::Retry(bool ON)
{
	g_Change = ON;
}