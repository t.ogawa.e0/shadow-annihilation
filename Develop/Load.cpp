//=================================================================
// ロード処理[Load.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\NumBerPallet.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "Load.h"
#include "SceneChange.h"

//=================================================================
// グローバル変数
static LOAD *g_aLoad;
static bool *g_bFadeType;

//-----------------------------------------------------------------
// 初期化
void InitLoad(void)
{
	
}

//-----------------------------------------------------------------
// 終了処理
void UninitLoad(void)
{
	
}

//-----------------------------------------------------------------
// 更新処理
void UpdateLoad(void)
{
	CLoad load;
	bool bLoad;

	bLoad = load.UpdateFade1();
	load.UpdateFade2(bLoad);
}

//-----------------------------------------------------------------
// ロード画面
bool CLoad::UpdateFade1(void)
{
	static int LoadCount = I_NULL;
	static bool bLoad = false;
	static bool bSEON = false;

	if (g_bFadeType[0] == false)
	{
		// ロード画面展開開始
		if (g_aLoad[0].bUpdateScene == false)
		{
			if (bSEON == false)
			{
				PlaySound(LOAD_SOUND_LIST::LOAD_SE1);
				bSEON = true;
			}
			g_aLoad[0].List.Poly.y += SCENESPEED;
			g_aLoad[0].bLoad = true;
			if (F_NULL <= g_aLoad[0].List.Poly.y)
			{
				bLoad = true;
				g_aLoad[0].List.Poly.y = F_NULL;
				g_aLoad[0].bDrawLoad = true;
			}
		}
		// ロード画面展開終了
		else if (g_aLoad[0].bUpdateScene == true)
		{
			bLoad = true;
			// ロード画面収納完了
			if (LOADTIME <= LoadCount)
			{
				if (bSEON == true)
				{
					PlaySound(LOAD_SOUND_LIST::LOAD_SE2);
					bSEON = false;
				}
				bLoad = false;
				LoadCount = LOADTIME;
				g_aLoad[0].bDrawLoad = false;
				g_aLoad[0].List.Poly.y -= SCENESPEED;
				if (g_aLoad[0].List.Poly.y <= -(float)SCREEN_HEIGHT)
				{
					LoadCount = NUMBERPALETTE_0;
					g_aLoad[0].List.Poly.y = -(float)SCREEN_HEIGHT;
					g_aLoad[0].bLoad = false;
					g_aLoad[0].bUpdateScene = false;
					g_aLoad[0].bFlagUpdateScene = false;
				}
			}
		}
		// ロード画面展開時間制御
		if (g_aLoad[0].bDrawLoad == true)
		{
			LoadCount++;
			if ((LOADTIME / NUMBERPALETTE_2) <= LoadCount)
			{
				if (g_aLoad[0].bUpdateScene == false)
				{
					g_aLoad[0].bUpdateScene = true;
					g_aLoad[0].bFlagUpdateScene = true;
				}
			}
		}
	}
	else if (g_bFadeType[0] == true)
	{
		// ロード画面展開開始
		if (g_aLoad[0].bUpdateScene == false)
		{
			g_aLoad[0].List.Color.a += COLORPALLET_20;
			g_aLoad[3].List.Color.a += COLORPALLET_20;
			g_aLoad[0].bLoad = true;
			g_aLoad[3].bLoad = true;
			if (255 <= g_aLoad[0].List.Color.a)
			{
				bLoad = false;
				g_aLoad[0].List.Color.a = COLORPALLET_255;
				g_aLoad[3].List.Color.a = COLORPALLET_255;
				g_aLoad[0].bDrawLoad = true;
				g_aLoad[3].bDrawLoad = true;
			}
		}
		// ロード画面展開終了
		else if (g_aLoad[0].bUpdateScene == true)
		{
			bLoad = false;
			// ロード画面収納完了
			if (ENDLOADTIME <= LoadCount)
			{
				bLoad = false;
				LoadCount = ENDLOADTIME;
				g_aLoad[0].bDrawLoad = false;
				g_aLoad[3].bDrawLoad = false;
				g_aLoad[0].List.Color.a -= COLORPALLET_20;
				g_aLoad[3].List.Color.a -= COLORPALLET_20;
				if (g_aLoad[0].List.Color.a <= COLORPALLET_0)
				{
					LoadCount = NUMBERPALETTE_0;
					g_aLoad[0].List.Color.a = COLORPALLET_0;
					g_aLoad[3].List.Color.a = COLORPALLET_0;
					g_aLoad[0].bLoad = false;
					g_aLoad[0].bUpdateScene = false;
					g_aLoad[0].bFlagUpdateScene = false;
					g_aLoad[3].bLoad = false;
					g_aLoad[3].bUpdateScene = false;
					g_aLoad[3].bFlagUpdateScene = false;
				}
			}
		}
		// ロード画面展開時間制御
		if (g_aLoad[0].bDrawLoad == true)
		{
			LoadCount++;
			if ((ENDLOADTIME / NUMBERPALETTE_2) <= LoadCount)
			{
				if (g_aLoad[0].bUpdateScene == false)
				{
					g_aLoad[0].bUpdateScene = true;
					g_aLoad[0].bFlagUpdateScene = true;
					g_aLoad[3].bUpdateScene = true;
					g_aLoad[3].bFlagUpdateScene = true;
				}
			}
		}
	}

	return bLoad;
}

//-----------------------------------------------------------------
// ロードフォント
void CLoad::UpdateFade2(bool bLoad)
{
	if (bLoad == false)
	{
		g_aLoad[1].bLoad = false;
		g_aLoad[2].bLoad = false;
	}
	else
	{
		g_aLoad[1].List.Ang.Angle += LOAD_FONT;
		g_aLoad[1].bLoad = true;
		g_aLoad[2].bLoad = true;
	}
}

//-----------------------------------------------------------------
// ロードタイプ切り替え(フェードタイプ)
void CLoad::LoadTypeON(void)
{
	CPolygon polygon;

	g_aLoad[0].List = polygon.PolygonDataInitList();
	g_aLoad[0].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aLoad[0].List.Poly.y = F_NULL;
	g_aLoad[0].List.Poly.w = (float)SCREEN_WIDTH;
	g_aLoad[0].List.Poly.h = (float)SCREEN_HEIGHT;
	g_aLoad[0].List.Color.r = g_aLoad[0].List.Color.g = g_aLoad[0].List.Color.b = g_aLoad[0].List.Color.a = COLORPALLET_0;
	g_aLoad[0].bDrawLoad = g_aLoad[0].bLoad = g_aLoad[0].bUpdateScene = g_aLoad[0].bFlagUpdateScene = false;
	g_aLoad[3].bDrawLoad = g_aLoad[3].bLoad = g_aLoad[3].bUpdateScene = g_aLoad[3].bFlagUpdateScene = false;

	g_bFadeType[0] = true;
}

//-----------------------------------------------------------------
// ロードタイプ切り替え(シャッタータイプ)
void CLoad::LoadTypeOFF(void)
{
	CPolygon polygon;

	g_aLoad[0].List = polygon.PolygonDataInitList();
	g_aLoad[0].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_LoadScreen;
	g_aLoad[0].List.Poly.y = -(float)SCREEN_HEIGHT;
	g_aLoad[0].List.Poly.w = (float)SCREEN_WIDTH;
	g_aLoad[0].List.Poly.h = (float)SCREEN_HEIGHT;
	g_aLoad[0].bDrawLoad = g_aLoad[0].bLoad = g_aLoad[0].bUpdateScene = g_aLoad[0].bFlagUpdateScene = false;
	g_bFadeType[0] = false;
}

//-----------------------------------------------------------------
// 更新処理
void DrawLoad(void)
{
	CPolygon polygon;
	
	int aArea = polygon.StaticAreaStart();

	for (int i = 0; i < LOADTEXNAM; i++)
	{
		if (g_aLoad[i].bLoad != false)
		{
			polygon.Draw(g_aLoad[i].List);
		}
	}
		
	polygon.StaticAreaEnd(aArea);
}

//-----------------------------------------------------------------
// 初期化
void CLoad::Init(void)
{
	CPolygon polygon;
	int s = -NUMBERPALETTE_1;
	int aArea = polygon.StaticAreaStart(); 

	g_aLoad = MemoryAllocation(g_aLoad, LOADTEXNAM);
	g_bFadeType = MemoryAllocation(g_bFadeType, NUMBERPALETTE_1);

	for (int i = 0; i < LOADTEXNAM; i++)
	{
		g_aLoad[i].List = polygon.PolygonDataInitList();
	}

	s++;
	g_aLoad[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_LoadScreen;
	g_aLoad[s].List.Poly.y = -(float)SCREEN_HEIGHT;
	g_aLoad[s].List.Poly.w = (float)SCREEN_WIDTH;
	g_aLoad[s].List.Poly.h = (float)SCREEN_HEIGHT;
	g_aLoad[s].bDrawLoad = g_aLoad[s].bLoad = g_aLoad[s].bUpdateScene = g_aLoad[s].bFlagUpdateScene = false;

	s++;
	g_aLoad[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_BC53;
	g_aLoad[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Widht / NUMBERPALETTE_17;
	g_aLoad[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Height / NUMBERPALETTE_17;
	g_aLoad[s].List.Poly.x = SCREEN_WIDTH - g_aLoad[s].List.Poly.w;
	g_aLoad[s].List.Poly.y = SCREEN_HEIGHT - g_aLoad[s].List.Poly.h;
	g_aLoad[s].List.bAffine = true;
	g_aLoad[s].List.Ang.x = g_aLoad[s].List.Poly.w / CENTER_POS;
	g_aLoad[s].List.Ang.y = g_aLoad[s].List.Poly.h / CENTER_POS;
	g_aLoad[s].bDrawLoad = g_aLoad[s].bLoad = g_aLoad[s].bUpdateScene = g_aLoad[s].bFlagUpdateScene = false;

	float fPolyW = g_aLoad[s].List.Poly.w;
	s++;
	g_aLoad[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_LOADING;
	g_aLoad[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Widht / CENTER_POS;
	g_aLoad[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Height / CENTER_POS;
	g_aLoad[s].List.Poly.x = SCREEN_WIDTH - g_aLoad[s].List.Poly.w - fPolyW;
	g_aLoad[s].List.Poly.y = SCREEN_HEIGHT - g_aLoad[s].List.Poly.h;
	g_aLoad[s].bDrawLoad = g_aLoad[s].bLoad = g_aLoad[s].bUpdateScene = g_aLoad[s].bFlagUpdateScene = false;

	s++;
	g_aLoad[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_ThankYouForPlaying;
	g_aLoad[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Widht / CENTER_POS;
	g_aLoad[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aLoad[s].List.TexName).Height / CENTER_POS;
	g_aLoad[s].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aLoad[s].List.Poly.w / CENTER_POS);
	g_aLoad[s].List.Poly.y = (SCREEN_HEIGHT / CENTER_POS) - (g_aLoad[s].List.Poly.h / CENTER_POS);
	g_aLoad[s].List.Color.r = g_aLoad[s].List.Color.g = g_aLoad[s].List.Color.b = COLORPALLET_255;
	g_aLoad[s].List.Color.a = COLORPALLET_0;
	g_aLoad[s].bDrawLoad = g_aLoad[s].bLoad = g_aLoad[s].bUpdateScene = g_aLoad[s].bFlagUpdateScene = false;

	g_bFadeType[0] = false;

	polygon.StaticAreaEnd(aArea);
}

//-----------------------------------------------------------------
// 終了処理
void CLoad::Uninit(void)
{
	AllMemoryDelete(g_aLoad);
	AllMemoryDelete(g_bFadeType);
}

//-----------------------------------------------------------------
// フラグ取得
bool CLoad::GetFlag(void)
{
	return g_aLoad[0].bLoad;
}

//-----------------------------------------------------------------
// 描画フラグ
bool CLoad::GetFlagDraw(void)
{
	return g_aLoad[0].bDrawLoad;
}

//-----------------------------------------------------------------
// 更新フラグ
bool CLoad::GetUpdate(void)
{
	return g_aLoad[0].bUpdateScene;
}

//-----------------------------------------------------------------
// 第二更新フラグ
bool CLoad::GetFlagUpdate(void)
{
	return g_aLoad[0].bFlagUpdateScene;
}