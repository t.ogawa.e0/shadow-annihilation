//=================================================================
// 矢印処理[Arrow.h]
// author: tatuya ogawa
//=================================================================

#ifndef _ARROW_H_ // インクルードガード 
#define _ARROW_H_

//=================================================================
// マクロ定義
#define _NUM_ARROW_ (1) // 矢印の数
#define _ARROW_CORRECTION_	(0.9f)
#define _SHIFT_ (2.0f)
#define _ARROW2SIZE_ (1.5f)
#define _ARROW2_LEVEL2 (3000.0f)
#define _ARROW2_LEVEL3 (1500.0f)
#define _ARROW2_NOTDROW (500.0f)
#define _SHIFT2_ (17.5f)

//=================================================================
// 構造体
// 矢印の構造体
typedef struct _ARROW
{
	POLYGON List; // 構造体セット
	float Shift; // 回転中心座標補正
	float Distance; // 距離
	bool bDraw; // 描画判定
}ARROW;

//=================================================================
// 矢印
class CArrow
{
private:
	// カーソル標示位置
	FLOAT_List Arrow(FLOAT_List Player, FLOAT_List Arrow, float Shift);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void Draw(void);

	// 全パラメーター取得
	ARROW Get(int i);
};
#endif // !_ARROW_H_
