//=================================================================
// カーソル[Cursor.h]
// author: tatuya ogawa
//=================================================================

#ifndef _CURSOR_H_ // インクルードガード
#define _CURSOR_H_

//=================================================================
// マクロ定義
#define NUMCURSOR			(1) // カーソル数
#define _SCOPE_CONTROL_		(0.6f)
#define _SCOPE_SENSITIVITY_	(1.2f)
#define SCOPE_SPPED (0.015f)

//=================================================================
// 構造体
// カーソルの構造体
typedef struct _CURSOR
{
	POLYGON List; // 構造体セット
}CURSOR;

//=================================================================
// カーソル
class CCursor
{
private:
	// ウィンドウ外修正
	CURSOR FrameCursor(CURSOR Cursor, float WIDTH, float HEIGHT);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void Draw(void);

	// 全パラメーター取得
	CURSOR Get(void);
};
#endif // !_CURSOR_H_
