//=================================================================
// カウンタ[Counter.h]
// author: tatuya ogawa
//=================================================================

#ifndef _COUNTER_H_ // インクルードガード 
#define _COUNTER_H_

//=================================================================
// マクロ定義
#define COUNTER_SCALE (1.0f)
#define COUNTER_LINE (3) // 行数

//=================================================================
// 構造体定義
typedef struct _COUNTER
{
	POLYGON List; // リスト構造体
	NUMBER Number; // ナンバー
	int Count; // カウンタ
}COUNTER;

//=================================================================
// カウンタ
class CCounter
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);

	// 加算
	void Set(void);

	// 移動速度
	void MoveCounter(float fSpeed);

	// ゲット関数
	int Get(void);
};

#endif // !_COUNTER_H_
