//=================================================================
// エフェクト[Effect.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\NumBerPallet.h"
#include "main.h"
#include "Effect.h"
#include "Bullet.h"
#include "MovePosData.h"
#include "Restriction.h"

//=================================================================
// グローバル変数
static EFFECT *g_aEffect; // エフェクト構造体
#if defined(_DEBUG) || defined(DEBUG)
static int g_Debug = I_NULL; // デバッグ用カウンタ
#endif

//-----------------------------------------------------------------
// 初期化
void CEffect::Init(void)
{
	CPolygon polygon;

	g_aEffect = MemoryAllocation(g_aEffect, LIMITEFFECT);

	for (int i = 0; i < LIMITEFFECT; i++)
	{
		g_aEffect[i].List = polygon.PolygonDataInitList();
		g_aEffect[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_EFFECT;
		g_aEffect[i].List.Poly.w = (float)polygon.GetTextureInfo(g_aEffect[i].List.TexName).Widht / NUMBERPALETTE_2;
		g_aEffect[i].List.Poly.h = (float)polygon.GetTextureInfo(g_aEffect[i].List.TexName).Height / NUMBERPALETTE_2;
		g_aEffect[i].bDraw = g_aEffect[i].bUpdate = B_OFF;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CEffect::Uninit(void)
{
	AllMemoryDelete(g_aEffect);
}

//-----------------------------------------------------------------
// 更新処理
void CEffect::Update(void)
{
	CMovePosData move;
	CRestriction restriction;
#if defined(_DEBUG) || defined(DEBUG)
	g_Debug = I_NULL;
#endif
	for (int i = 0; i < LIMITEFFECT; i++)
	{
		if (g_aEffect[i].bUpdate == B_ON)
		{
			g_aEffect[i].bDraw = B_ON;
			g_aEffect[i].List.Poly.x -= move.Get().aMove.x;
			g_aEffect[i].List.Poly.y -= move.Get().aMove.y;
			g_aEffect[i].List.Color.a -= (int)((_RGBACOLOR_ / EFFECTTIME) + CORRECTION);
			
			if (restriction.Draw(g_aEffect[i].List.Poly) == false)
			{
				g_aEffect[i].List.Color.a = COLORPALLET_0;
				g_aEffect[i].bDraw = B_OFF;
				g_aEffect[i].bUpdate = B_OFF;
			}
			if (COLORPALLET_0 >= g_aEffect[i].List.Color.a)
			{
				g_aEffect[i].List.Color.a = COLORPALLET_0;
				g_aEffect[i].bDraw = B_OFF;
				g_aEffect[i].bUpdate = B_OFF;
			}
#if defined(_DEBUG) || defined(DEBUG)
			g_Debug++;
#endif
		}
	}
}

//-----------------------------------------------------------------
// 描画開始
void CEffect::Draw(void)
{
	CPolygon polygon;
	CRestriction restriction;

	for (int i = 0; i < LIMITEFFECT; i++)
	{
		if (restriction.Draw(g_aEffect[i].List.Poly) == true)
		{
			if (g_aEffect[i].bDraw == B_ON)
			{
				polygon.Draw(g_aEffect[i].List, B_ON);
			}
		}
	}
}

//-----------------------------------------------------------------
// エフェクト生成
void CEffect::SetEffect(FLOAT_List Poly, int ID)
{
	for (int i = 0; i < LIMITEFFECT; i++)
	{
		if (g_aEffect[i].bUpdate == B_OFF)
		{
			g_aEffect[i].List.Poly.x = Poly.x + (Poly.w / CENTER_POS) - (g_aEffect[i].List.Poly.w / CENTER_POS);
			g_aEffect[i].List.Poly.y = Poly.y + (Poly.h / CENTER_POS) - (g_aEffect[i].List.Poly.h / CENTER_POS);
			g_aEffect[i].List.Color.a = COLORPALLET_255;
			g_aEffect[i].bUpdate = B_ON;
			if (ID == PLAYERID)
			{
				g_aEffect[i].List.Color.r = COLORPALLET_248;
				g_aEffect[i].List.Color.g = COLORPALLET_170;
				g_aEffect[i].List.Color.b = COLORPALLET_250;
			}
			if (ID == ENEMYID)
			{
				g_aEffect[i].List.Color.r = COLORPALLET_0;
				g_aEffect[i].List.Color.g = COLORPALLET_30;
				g_aEffect[i].List.Color.b = COLORPALLET_248;
			}
			break;
		}
	}
}

//-----------------------------------------------------------------
// エフェクトの数の取得
int CEffect::GetNumEffect(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	return g_Debug;
#else
	return 0;
#endif

}