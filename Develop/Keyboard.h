//=================================================================
// キーボード処理[Keyboard.h]
// author: tatuya ogawa
//=================================================================

#ifndef _KEY_H_ // インクルードガード
#define _KEY_H_

//=================================================================
// マクロ定義
#define _NUMKEY_ (4)
#define _NUMKEYBOARD_ (27)

//=================================================================
// キーリスト
typedef struct _KEYLIST
{
	int Key[_NUMKEY_]; // キーリスト
	int Number[(_NUMKEY_ * 2)]; // 識別番号リスト
	float Angle[(_NUMKEY_ * 2)]; // 回転率リスト
}KEYLIST;

typedef struct _KEYBOARD
{
	int KeyCase;
	int KeyID;
}KEYBOARD;

//=================================================================
// キー(フロート型)
typedef struct _KEYFLOAT
{
	float x;
	float y;
}KEYFLOAT;

//=================================================================
// キーボード
class CKey
{
private:
	// 回転角度の計算
	float CreateAngle(float WIDTH, float HEIGHT, int AngleID);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void Draw(void);

	// キーの処理
	KEYFLOAT Case(int Key[]);

	// キー入力時の向き
	float Direction(KEYLIST KeyList);

	// キーボード
	int KeyBoard(void);

	// キーのリスト
	int KeyList(int num);

	// 全パラメーター取得
	KEYLIST Get(void);
};

#endif // !_KEY_H_
