//=================================================================
// 制限[Restriction.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "main.h"
#include "Restriction.h"

//-----------------------------------------------------------------
// 初期化
void CRestriction::Init(void)
{

}

//-----------------------------------------------------------------
// 終了処理
void CRestriction::Uninit(void)
{

}

//-----------------------------------------------------------------
// 更新処理
void CRestriction::Update(void)
{

}

//-----------------------------------------------------------------
// 描画判定
bool CRestriction::Draw(FLOAT_List Poly)
{
	// 画面領域外の時のフラグ
	if (-(float)SCREEN_WIDTH >= Poly.x && Poly.x >= (float)SCREEN_WIDTH) { return false; }
	if (-(float)SCREEN_HEIGHT >= Poly.y && Poly.y >= (float)SCREEN_HEIGHT) { return false; }

	return true;
}