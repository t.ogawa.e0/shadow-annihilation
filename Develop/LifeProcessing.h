//=================================================================
// ライフ[LifeProcessing.h]
// author: tatuya ogawa
//=================================================================

#ifndef _LIFEPROCESSSING_H_ // インクルードガード
#define _LIFEPROCESSSING_H_

//=================================================================
// マクロ定義
#define PLAYER_LIFEBAR_WIDTH (200.0f) // プレイヤーのライフバー幅
#define PLAYER_LIFEBAR_HEIGHT (30.0f) // プレイヤーのライフバー高さ
#define PLAYER_LIFEBAR_PARTS (4) // プレイヤー用ライフ構成マクロ
#define ENEMY_LIFEBAR_WIDTH (104.0f) // エネミーのライフバー幅
#define ENEMY_LIFEBAR_HEIGHT (14.0f) // エネミーのライフバー高さ
#define ENEMY_LIFEBAR_PARTS (2) // プレイヤー用ライフ構成マクロ
#define LIFBERLIMIT (6) // ライフバーの割合段階

//=================================================================
// ライフバー情報管理
typedef struct _LIFEBER
{
	POLYGON List; // ポリゴン
	LIFE Life; // HP
	LIFE OldLife; // 古いHP
}LIFEBER;

//=================================================================
// ライフバー
class CLife
{
private:
	// ライフバー処理
	LIFEBER Life(LIFEBER LifeBer, float Percent);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(int ID);

	// プレイヤーのライフセット
	void SetPlayerLife(LIFE Life);

	// エネミーのライフセット
	void SetEnemyLife(LIFE Life, FLOAT_List Poly, int NumEnemy);
};

#endif // !_LIFEPROCESSSING_H_
