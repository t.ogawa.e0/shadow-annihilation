//=================================================================
// メインヘッダ[main.h]
// author: tatuya ogawa
//=================================================================

#ifndef MAIN_H_	//インクルードガード
#define MAIN_H_

//=================================================================
// マクロ定義
#define CLASS_NAAME "SHADOW ANNIHILATION"
#define WINDOW_NAMW "SHADOW ANNIHILATION"
#define SCREEN_WIDTH (1000)	//ウィンドウの幅
#define SCREEN_HEIGHT (700)	//ウィンドウの高さ
#define WINDOW_STYLE (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX))

#endif // !MAIN_H_