//=================================================================
// リザルト処理[Result.h]
// author: 
//=================================================================

#ifndef _RESULT_H_ // インクルードガード
#define _RESULT_H_

#define NUM_RESULT_TEXTURE (9) // リザルトのテクスチャ数
#define RESULT_MOVE_SPPED (50.0f) // リザルトフォント移動速度
#define RESULT_LINE (100.0f) // 線の長さ
#define _BONTFONT_SCALE_ (2.0f) // 撃破数フォントのスケール

//=================================================================
// リザルト
typedef struct _RESULT
{
	POLYGON List; // リスト構造体
	float fLimitPos; // 移動制限
	bool bDraw; // 描画判定
	bool bUpdate; // 更新判定
	bool Enter; // カウンタ
}RESULT;

class CResult
{
private:
	// リザルトフォント
	RESULT ResultFont(RESULT aResult);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void Draw(void);
};

// 初期化
void InitResult(void);

// 終了処理
void UninitResult(void);

// 更新処理
void UpdateResult(void);

// 描画開始
void DrawResult(void);
#endif // !_RESULT_H_
