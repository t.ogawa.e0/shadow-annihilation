//=================================================================
// タイトル処理[Title.h]
// author: 
//=================================================================

#ifndef _TITLE_H_ // インクルードガード
#define _TITLE_H_

//=================================================================
// マクロ定義
#define _NUM_TITLETEX (2)
#define TITLE_FontScale (2.0f)
#define TITIE_FONT_COUNT (70)
#define TITLE_CHANGE_SPPED (5)
#define TITLE_FONT_ALFASPPED (8)
#define TITLE_LIMITFLAME (3600)

//=================================================================
// 構造体定義
typedef struct _TITLE
{
	POLYGON List; // ポリゴン
	INTTIM Time; // タイマー
	int nAlfaCount; // αカウント
	bool bAlfa; // α値フラグ
}TITLE;

//=================================================================
// タイトル
class CTitle
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画開始
	void Draw(void);
};

// 初期化
void InitTitle(void);

// 終了処理
void UninitTitle(void);

// 更新処理
void UpdateTitle(void);

// 描画開始
void DrawTitle(void);
#endif // !_TITLE_H_
