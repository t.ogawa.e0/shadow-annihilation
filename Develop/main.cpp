//=================================================================
// メインヘッダ[main.cpp]
// author: tatuya ogawa
//=================================================================

#include <Windows.h>
#include <math.h>
#include <time.h>
#include "IncludeData\DirectX2D.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "SceneChange.h"
#include "input.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "Load.h"

//=================================================================
// メイン
class CMain
{
public:
	bool Init(HINSTANCE hInstance, HWND hWnd, BOOL bWindow);
	void Uninit(void);
	bool Update(void);
	void Draw(void);
private:
};

//-----------------------------------------------------------------
// プロトタイプ宣言
static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

//-----------------------------------------------------------------
//	メイン関数
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wcex;	// ウインドウクラス構造体
	HWND hWnd;			// ウインドウハンドル	ウィンドウのポインタ
	MSG msg;			// メッセージ構造体		押したボタンによって表示メッセージを変えるためのもの
	int MainWidth = 0, MainHeight = 0;//ウィンドウのサイズ[main]
	RECT wr = { 0,0,SCREEN_WIDTH , SCREEN_HEIGHT }, dr{ 0,0,SCREEN_WIDTH,SCREEN_HEIGHT };
	DWORD DNewTime = 0, DOldTime = 0; //時間格納
	CMain main;

	lpCmdLine = lpCmdLine;
	hPrevInstance = hPrevInstance;
	MainWidth = wr.right - wr.left;
	MainHeight = wr.bottom - wr.top;
	//変数の初期化
	{
		wcex.cbSize			= sizeof(WNDCLASSEX);			// 構造体のサイズ
		wcex.style			= CS_VREDRAW | CS_HREDRAW;		// ウインドウスタイル
		wcex.lpfnWndProc	= (WNDPROC)WndProc;				// そのウインドウのメッセージを処理するコールバック関数へのポインタ
		wcex.cbClsExtra		= 0;							// ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
		wcex.cbWndExtra		= 0;							// ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
		wcex.hInstance		= hInstance;					// このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
		wcex.hIcon			= NULL;							// アイコンのハンドル
		wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);	// マウスカーソルのハンドル．LoadCursor( NULL, IDC_ARROW )とか．
		wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);	// ウインドウ背景色(背景描画用ブラシのハンドル)．
		wcex.lpszMenuName	= NULL;							// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
		wcex.lpszClassName	= CLASS_NAAME;					// このウインドウクラスにつける名前
		wcex.hIconSm		= NULL;							// 16×16の小さいサイズのアイコン
	}

	// マウスカーソル表示設定
	ShowCursor(TRUE);

	//	ウインドウクラスを登録します。
	RegisterClassEx(&wcex);

	AdjustWindowRect(&wr, wcex.style, TRUE);		//ウィンドウサイズ習得

	GetWindowRect(GetDesktopWindow(), &dr);		//デスクトップサイズ習得

	//	ウインドウを作成します。
	hWnd = CreateWindowEx(0, CLASS_NAAME, WINDOW_NAMW, WINDOW_STYLE, CW_USEDEFAULT, CW_USEDEFAULT, MainWidth, MainHeight, NULL, NULL, hInstance, NULL);

	//	ウインドウを表示します。
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	if (!main.Init(hInstance, hWnd, TRUE))
	{
		ErrorMessage("初期化に失敗しました");
		return-1;
	}

	timeBeginPeriod(1);//時間を正確にするもの

	//	メッセージループ
	for (;;)
	{
		//何があってもスルーする
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			//msgに入ったのがWM_QUITだったら
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				//メッセージ処理
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//ゲーム処理
			DNewTime = timeGetTime();
			
			if (_MINUS_(DNewTime, DOldTime) >= _DIVISION_(1000, 60))
			{
				//更新
				if (main.Update())
				{
					//描画
					main.Draw();
				}
				else
				{
					ErrorMessage("テクスチャの読み込み失敗(5)");
					return-1;
				}

				//時間渡し
				DOldTime = DNewTime;
			}
		}
	}
	timeEndPeriod(1);//timeBeginPeriod使用時ループ終了時に書く
	main.Uninit();

	//	戻り値を返します。
	return (int)msg.wParam;
}

//-----------------------------------------------------------------
// ウインドウプロシージャ
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	int nID;
	// メッセージの種類に応じて処理を分岐します。
	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:	// キー入力
		switch (wParam)
		{
		case VK_ESCAPE:
			// [ESC]キーが押されたら
			nID = MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2);
			if (nID == IDYES)
			{
				DestroyWindow(hWnd);	// ウィンドウを破棄
			}
			else
			{
				return 0;	// いいえの時
			}
		}
		break;
	case WM_LBUTTONDOWN:
		SetFocus(hWnd);
		break;
	case WM_CLOSE:	// ×ボタン押した時
		nID = MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2);
		if (nID == IDYES)
		{
			DestroyWindow(hWnd);	// ウィンドウを破棄
		}
		else
		{
			return 0;	// いいえの時
		}
		break;
	default:
		break;
	}

	//デフォルトの処理
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

//-----------------------------------------------------------------
//	初期化
bool CMain::Init(HINSTANCE hInstance, HWND hWnd, BOOL bWindow)
{
	CPolygon polygon;
	CDevice device;
	CScene scene;
	CMouse mouse;
	CKey key;
	CLoad load;

	srand((unsigned)time(NULL));

	if (!device.Init(hInstance, hWnd, bWindow, SCREEN_WIDTH, SCREEN_HEIGHT)) { return false; }
	if (InitSound(START_SCENE, hWnd) != S_OK)
	{
		ErrorMessage("サウンドデータの読み込みに失敗");
		return false;
	}
	if (!polygon.Init(START_SCENE))
	{
		ErrorMessage("テクスチャの読み込み失敗(1)");
		return false;
	}
	if (!polygon.InitStaticArea(SCENE_LOAD))
	{
		ErrorMessage("テクスチャの読み込み失敗(2)");
		return false;
	}
	if (!scene.Init())
	{
		ErrorMessage("テクスチャの読み込み失敗(3)");
		return false;
	}

	// キーボード初期化
	InitKeyboard(hInstance, hWnd);
	key.Init();
	mouse.Init(hWnd);
	load.Init();
	
	return true;
}

//-----------------------------------------------------------------
//	終了処理
void CMain::Uninit(void)
{
	CPolygon polygon;
	CDevice device;
	CScene scene;
	CMouse mouse;
	CKey key;
	CLoad load;

	UninitKeyboard();
	key.Uninit();
	polygon.Uninit();
	device.Uninit();
	scene.Uninit();
	mouse.Uninit();
	load.Uninit();
	UninitSound();
}

//-----------------------------------------------------------------
//	更新処理
bool CMain::Update(void)
{
	CScene scene;

	UpdateKeyboard();
	if (!scene.Update()) { return false; }

	return true;
}

//-----------------------------------------------------------------
//	描画処理
void CMain::Draw(void)
{
	CDevice device;
	CScene scene;

	device.GetD3DDevice()->Clear(0, NULL, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), BACKGROUND_COLOR, F_DEF, 0);

	// Direct3Dによる描画の開始
	if (SUCCEEDED(device.GetD3DDevice()->BeginScene()))
	{
		scene.Draw();
		device.GetD3DDevice()->EndScene();//Direct3Dによる描画の終了
	}
	device.GetD3DDevice()->Present(NULL, NULL, NULL, NULL);//第3　対象のウィンドウハンドル　hWnd
}
