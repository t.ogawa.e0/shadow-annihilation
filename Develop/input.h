//=================================================================
// 入力処理[input.h]
// author: tatuya ogawa
//=================================================================

#ifndef _INPUT_H_
#define _INPUT_H_

#include<Windows.h>
#define DIRECTINPUT_VERSION (0x0800)	//dinput.hの前に必ず入れる
#include<dinput.h>
#ifndef _PRESS_
#define _PRESS_(a) (GetKeyboardPress((a))) // 押している間
#endif // !_PRESS_
#ifndef _TRIGGER_
#define _TRIGGER_(a) (GetKeyboardTrigger((a))) // 押した瞬間
#endif // !_TRIGGER_
#ifndef _REPEAT_
#define _REPEAT_(a) (GetKeyboardRepeat((a))) // 押している間
#endif // !_REPEAT_
#ifndef _RELEASE_
#define _RELEASE_(a) (GetKeyboardRelease((a))) // 離した瞬間
#endif // !_RELEASE_

//=================================================================
// プロトタイプ宣言
HRESULT InitKeyboard(HINSTANCE hInstance, HWND hWnd);
void UninitKeyboard(void);
void UpdateKeyboard(void);

bool GetKeyboardPress(int nKey);
bool GetKeyboardTrigger(int nKey);
bool GetKeyboardRepeat(int nKey);
bool GetKeyboardRelease(int nKey);

#endif