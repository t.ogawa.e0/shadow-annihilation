//=================================================================
// 回転角度自動生成[RotationAngle.h]
// author: tatuya ogawa
//=================================================================

#ifndef _ROTATIONANGLE_H_ // インクルードガード
#define _ROTATIONANGLE_H_

//=================================================================
// 回転
class CAngle
{
private:
public:
	// 対象物に対しての方向追従
	float Angle2D(FLOAT_List Target_A, FLOAT_List Target_B);
};

#endif // !_ROTATIONANGLE_H_
