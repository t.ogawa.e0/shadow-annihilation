//=================================================================
// リザルト処理[Result.cpp]
// author: 
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "Number.h"
#include "Result.h"
#include "input.h"
#include "SceneChange.h"
#include "Ranking.h"
#include "Load.h"

//=================================================================
// グローバル変数
static RESULT *g_aResult;
static NUMBER *g_aNumber; // ナンバー
static INTTIM *g_aTimer;
static bool *g_aAlfa;

//-----------------------------------------------------------------
// 初期化
void InitResult(void)
{
	CRanking ranking;
	CResult result;

	ranking.Init();
	ranking.NameWindowInit();
	result.Init();
}

//-----------------------------------------------------------------
// 終了処理
void UninitResult(void)
{
	CResult result;
	CRanking ranking;

	ranking.Uninit();
	result.Uninit();
}

//-----------------------------------------------------------------
// 更新処理
void UpdateResult(void)
{
	CResult result;
	
	result.Update();
}

//-----------------------------------------------------------------
// 描画開始
void DrawResult(void)
{
	CResult result;
	CRanking ranking;

	result.Draw();
	ranking.Draw3();
}

//-----------------------------------------------------------------
// 初期化
void CResult::Init(void)
{
	CPolygon polygon;
	FLOAT_List Pos, Pos1;
	CNumber number;
	CRanking ranking; 
	
	int s = -1; 

	g_aResult = MemoryAllocation(g_aResult, NUM_RESULT_TEXTURE);
	g_aTimer = MemoryAllocation(g_aTimer, 1);
	g_aNumber = MemoryAllocation(g_aNumber, 1);
	g_aAlfa = MemoryAllocation(g_aAlfa, 1);

	for (int i = 0; i < NUM_RESULT_TEXTURE; i++)
	{
		g_aResult[i].List = polygon.PolygonDataInitList();
		g_aResult[i].bDraw = B_OFF;
		g_aResult[i].bUpdate = B_OFF;
		g_aResult[i].Enter = B_OFF;
	}
	
	// リザルト画面背景
	s++;
	g_aResult[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aResult[s].List.Poly.x = F_NULL;
	g_aResult[s].List.Poly.y = F_NULL;
	g_aResult[s].List.Poly.w = (float)SCREEN_WIDTH;
	g_aResult[s].List.Poly.h = (float)SCREEN_HEIGHT;
	g_aResult[s].List.Color.r = COLORPALLET_50;
	g_aResult[s].List.Color.g = COLORPALLET_50;
	g_aResult[s].List.Color.b = COLORPALLET_50;
	g_aResult[s].bDraw = B_ON;

	// リザルトフォント
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_Result;
	g_aResult[s].List.Poly.w = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / 2.5f);
	g_aResult[s].List.Poly.h = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Height / 2.5f);
	g_aResult[s].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aResult[s].List.Poly.w / CENTER_POS);
	g_aResult[s].List.Poly.y = -(SCREEN_HEIGHT / CENTER_POS) - (g_aResult[s].List.Poly.h / CENTER_POS);
	g_aResult[s].fLimitPos = -(float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Height / (CENTER_POS * 4));
	g_aResult[s].bUpdate = B_ON;

	// 線
	s++;
	g_aResult[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aResult[s].List.Poly.x = RESULT_LINE;
	g_aResult[s].List.Poly.y = (float)(SCREEN_HEIGHT / (CENTER_POS * 2));
	g_aResult[s].List.Poly.w = (float)SCREEN_WIDTH - (RESULT_LINE * 2);
	g_aResult[s].List.Poly.h = 10.0f;
	g_aResult[s].bDraw = B_ON;

	// 線
	s++;
	g_aResult[s].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aResult[s].List.Poly.x = RESULT_LINE;
	g_aResult[s].List.Poly.y = (float)(SCREEN_HEIGHT / 1.5f);
	g_aResult[s].List.Poly.w = (float)SCREEN_WIDTH - (RESULT_LINE * 2);
	g_aResult[s].List.Poly.h = 10.0f;
	g_aResult[s].bDraw = B_ON;

	// 撃破数フォント
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_BONTFONT;
	g_aResult[s].List.Poly.w = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / _BONTFONT_SCALE_);
	g_aResult[s].List.Poly.h = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Height / _BONTFONT_SCALE_);
	g_aResult[s].List.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aResult[s].List.Poly.w / 1.3f);
	g_aResult[s].List.Poly.y = (SCREEN_HEIGHT / CENTER_POS) - (g_aResult[s].List.Poly.h / 1.3f);
	g_aResult[s].bDraw = B_ON;
	Pos = g_aResult[s].List.Poly;

	// 順位フォント
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_Juni;
	g_aResult[s].List.Poly.w = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / _BONTFONT_SCALE_);
	g_aResult[s].List.Poly.h = (float)(polygon.GetTextureInfo(g_aResult[s].List.TexName).Height / _BONTFONT_SCALE_);
	g_aResult[s].List.Poly.x = Pos.x + 20.0f;
	g_aResult[s].List.Poly.y = (SCREEN_HEIGHT / CENTER_POS) - (g_aResult[s].List.Poly.h / 4.0f);
	g_aResult[s].bDraw = B_ON;
	Pos1 = g_aResult[s].List.Poly;

	// 数字フォント
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_BIGWhiteNumber;
	g_aResult[s].List.Anim.Count = -1;
	g_aResult[s].List.Anim.Direction = 10;
	g_aResult[s].List.Anim.Pattern = 10;
	g_aResult[s].List.Anim.AnimationON = B_ON;
	g_aResult[s].List.TexCut.x = g_aResult[s].List.TexCut.w = (polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / 10);
	g_aResult[s].List.TexCut.y = g_aResult[s].List.TexCut.h = polygon.GetTextureInfo(g_aResult[s].List.TexName).Height;
	g_aResult[s].List.Poly.w = (float)g_aResult[s].List.TexCut.w;
	g_aResult[s].List.Poly.h = (float)g_aResult[s].List.TexCut.h;
	g_aResult[s].List.Poly.x = Pos.x + Pos.w;
	g_aResult[s].List.Poly.y = Pos.y + (g_aResult[s].List.Poly.h / CENTER_POS) + 10.0f;
	g_aResult[s].List.Anim.Frame = 1;
	g_aResult[s].List.bAffine = B_ON;
	g_aResult[s].List.Ang.x = g_aResult[s].List.Scale.x = _DIVISION_(g_aResult[s].List.Poly.w, CENTER_POS);
	g_aResult[s].List.Ang.y = g_aResult[s].List.Scale.y = _DIVISION_(g_aResult[s].List.Poly.h, CENTER_POS);
	g_aResult[s].bDraw = B_ON;
	g_aResult[s].List.Anim.Count = ranking.Draw2().Score;

	// 数字フォント
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_BIGWhiteNumber;
	g_aResult[s].List.Anim.Count = -1;
	g_aResult[s].List.Anim.Direction = 10;
	g_aResult[s].List.Anim.Pattern = 10;
	g_aResult[s].List.Anim.AnimationON = B_ON;
	g_aResult[s].List.TexCut.x = g_aResult[s].List.TexCut.w = (polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / 10);
	g_aResult[s].List.TexCut.y = g_aResult[s].List.TexCut.h = polygon.GetTextureInfo(g_aResult[s].List.TexName).Height;
	g_aResult[s].List.Poly.w = (float)g_aResult[s].List.TexCut.w;
	g_aResult[s].List.Poly.h = (float)g_aResult[s].List.TexCut.h;
	g_aResult[s].List.Poly.x = Pos1.x + Pos1.w;
	g_aResult[s].List.Poly.y = Pos1.y;
	g_aResult[s].List.Anim.Frame = 1;
	g_aResult[s].List.bAffine = B_ON;
	g_aResult[s].List.Ang.x = g_aResult[s].List.Scale.x = _DIVISION_(g_aResult[s].List.Poly.w, CENTER_POS);
	g_aResult[s].List.Ang.y = g_aResult[s].List.Scale.y = _DIVISION_(g_aResult[s].List.Poly.h, CENTER_POS);
	g_aResult[s].bDraw = B_ON;
	g_aResult[s].List.Anim.Count = ranking.Draw2().Ranking;

	// PressEnter
	s++;
	g_aResult[s].List.TexName = RESULT_TEXTURE_LIST::TEXTURE_RESULT_PRESSENTER;
	g_aResult[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aResult[s].List.TexName).Widht / CENTER_POS;
	g_aResult[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aResult[s].List.TexName).Height / CENTER_POS;
	g_aResult[s].List.Poly.x = ((SCREEN_WIDTH / CENTER_POS) - (g_aResult[s].List.Poly.w / CENTER_POS));
	g_aResult[s].List.Poly.y = ((SCREEN_HEIGHT / CENTER_POS) - (g_aResult[s].List.Poly.h / CENTER_POS)) * 2;
	g_aResult[s].bDraw = B_ON;

	g_aAlfa[0] = false;
	g_aTimer[0].Count = -1;
	g_aTimer[0].End = 70;
	g_aNumber[0].Digit = 3;
	g_aNumber[0].LeftAlignment = B_ON;
	g_aNumber[0].Max = number.Init(g_aNumber[0].Digit);
	g_aNumber[0].Zero = B_ON;	
	PlaySound(RESULT_SOUND_LIST::RESULT_BeyondTheImmediateSide);
}

//-----------------------------------------------------------------
// 終了処理
void CResult::Uninit(void)
{
	AllMemoryDelete(g_aAlfa);
	AllMemoryDelete(g_aResult);
	AllMemoryDelete(g_aTimer);
	AllMemoryDelete(g_aNumber);
}

//-----------------------------------------------------------------
// 更新処理
void CResult::Update(void)
{
	CScene scene;
	CRanking ranking;

	g_aResult[1] = ResultFont(g_aResult[1]);

	// α値変更
	if (g_aTimer[0].Count >= (g_aTimer[0].End / 2))
	{
		g_aResult[8].List.Color.a = COLORPALLET_0;
	}

	// α値変更
	if (g_aTimer[0].Count >= g_aTimer[0].End)
	{
		g_aResult[8].List.Color.a = COLORPALLET_255;
		g_aTimer[0].Count = I_NULL;
	}
	g_aTimer[0].Count++;

	if (ranking.SetName() == true)
	{
		if (_TRIGGER_(DIK_RETURN))
		{
			CLoad load;
			load.LoadTypeON();
			scene.Change(SCENE_TITLE);
		}
	}
}

//-----------------------------------------------------------------
// 描画処理
void CResult::Draw(void)
{
	CPolygon polygon;
	CNumber number;
	int aArea = 0;
	
	for (int i = 0; i < NUM_RESULT_TEXTURE; i++)
	{
		if (g_aResult[i].bDraw != B_OFF)
		{
			if (i == 0 || i == 2 || i == 3) { aArea = polygon.StaticAreaStart(); }
			if (i == 6 || i == 7) { number.Draw(g_aResult[i].List, g_aNumber[0], g_aResult[i].List.Anim.Count); }
			else { polygon.Draw(g_aResult[i].List); }
			if (i == 0 || i == 2 || i == 3) { polygon.StaticAreaEnd(aArea); }
		}
	}
}

RESULT CResult::ResultFont(RESULT aResult)
{
	if (aResult.bUpdate == B_ON)
	{
		aResult.bDraw = B_ON;
		if (aResult.List.Poly.y <= aResult.fLimitPos)
		{
			aResult.List.Poly.y += RESULT_MOVE_SPPED;
			if (aResult.fLimitPos <= aResult.List.Poly.y)
			{
				aResult.List.Poly.y -= (aResult.List.Poly.y - aResult.fLimitPos);
				aResult.bUpdate = B_OFF;
			}
		}
	}

	return aResult;
}