//=================================================================
// マウスヘッダ[Mouse.h]
// author: tatuya ogawa
//=================================================================

#ifndef _MOUSE_H_
#define _MOUSE_H_

//=================================================================
// マウス
class CMouse
{
public:	
	// 初期化
	void Init(HWND hWnd);

	// 終了処理
	void Uninit(void);

	// 更新処理
	POINT Update(void);

	// 描画処理
	void Draw(void);

	// カーソルの座標
	FLOAT_List Cursor(float w, float h);

	// 左クリック
	SHORT LeftClick(void);

	// 右クリック
	SHORT RightClick(void);
private:
};

#endif // !_MOUSE_H_
