//=================================================================
// 情報[Information.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "IncludeData\ColorPallet.h"
#include "main.h"
#include "Number.h"
#include "Timer.h"
#include "Countdown.h"
#include "Information.h"
#include "Counter.h"
#include "Ranking.h"

//=================================================================
// グローバル変数
static INFORMATION *g_aInformation;

//-----------------------------------------------------------------
// 初期化
void CInformation::Init(void)
{
	CPolygon polygon;
	int s = -1;

	g_aInformation = MemoryAllocation(g_aInformation, NUM_INFORMATION);

	for (int i = 0; i < NUM_INFORMATION; i++)
	{
		g_aInformation[i].List = polygon.PolygonDataInitList();
		g_aInformation[i].bDraw = B_OFF;
		g_aInformation[i].bUpdate = B_OFF;
		g_aInformation[i].Enter = B_OFF;
		g_aInformation[i].Time.Count = -NUMBERPALETTE_1;
		g_aInformation[i].Time.End = TIINFORMATION_COUNT;
	}

	// インフォメーションバー
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_InformationBar;
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height;
	g_aInformation[s].List.Color.a = (int)(COLORPALLET_255 / NUMBERPALETTE_2);
	g_aInformation[s].bDraw = B_ON;
	g_aInformation[s].bUpdate = B_ON;
		
	// カウントダウンフォント
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Font00;
	g_aInformation[s].List.Poly.x = SCREEN_WIDTH - (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht - (NUMBER_W * CENTER_POS);
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height;
	g_aInformation[s].fLimitPos = INFO_MOVELIMIT;
	g_aInformation[s].bDraw = B_ON;
	g_aInformation[s].bUpdate = B_ON;

	// 撃破数フォント
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Font01;
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height;
	g_aInformation[s].List.Poly.x = F_NULL - ((float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht + (NUMBER_W * NUMBERPALETTE_3));
	g_aInformation[s].fLimitPos = (SCREEN_WIDTH - (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht - (NUMBER_W * NUMBERPALETTE_3));
	g_aInformation[s].bUpdate = B_ON;

	// WINフォント
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Font02;
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht / CENTER_POS;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height / CENTER_POS;
	g_aInformation[s].List.Poly.x = ((SCREEN_WIDTH / CENTER_POS) - (g_aInformation[3].List.Poly.w / CENTER_POS));
	g_aInformation[s].List.Poly.y = -((SCREEN_HEIGHT / CENTER_POS) - (g_aInformation[3].List.Poly.h / CENTER_POS));

	// LOUSフォント
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Font03;
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht / CENTER_POS;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height / CENTER_POS;
	g_aInformation[s].List.Poly.x = ((SCREEN_WIDTH / CENTER_POS) - (g_aInformation[4].List.Poly.w / CENTER_POS));
	g_aInformation[s].List.Poly.y = -((SCREEN_HEIGHT / CENTER_POS) - (g_aInformation[4].List.Poly.h / CENTER_POS));

	// PressEnterフォント
	s++;
	g_aInformation[s].List.TexName = GAME_TEXTURE_LIST::TEXTURE_PressEnter;
	g_aInformation[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Widht / CENTER_POS;
	g_aInformation[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aInformation[s].List.TexName).Height / CENTER_POS;
	g_aInformation[s].List.Poly.x = ((SCREEN_WIDTH / CENTER_POS) - (g_aInformation[5].List.Poly.w / CENTER_POS));
	g_aInformation[s].List.Poly.y = ((SCREEN_HEIGHT / CENTER_POS) - (g_aInformation[5].List.Poly.h / CENTER_POS)) * CENTER_POS;
}

//-----------------------------------------------------------------
// 終了処理
void CInformation::Uninit(void)
{
	AllMemoryDelete(g_aInformation);
}

//-----------------------------------------------------------------
// 更新処理
void CInformation::Update(void)
{
	g_aInformation[1] = InfoFont00(g_aInformation[1]);
	g_aInformation[2] = InfoFont01(g_aInformation[2], g_aInformation[1].bUpdate);
	
	for (int i = NUMBERPALETTE_3; i < NUMBERPALETTE_5; i++)
	{
		g_aInformation[i] = InfoFont02_03(g_aInformation[i]);
	}
	g_aInformation[5] = InfoFont5(g_aInformation[5]);
}

//-----------------------------------------------------------------
// 描画開始
void CInformation::Draw(void)
{
	CPolygon polygon;

	for (int i = 0; i < NUM_INFORMATION; i++)
	{
		if (g_aInformation[i].bDraw != B_OFF)
		{
			polygon.Draw(g_aInformation[i].List);
		}
	}
}

//-----------------------------------------------------------------
// カウントダウンフォント
INFORMATION CInformation::InfoFont00(INFORMATION Info)
{
	if (Info.bUpdate == B_ON)
	{
		CCountdown countdown;
		if (countdown.Get().Timer.Tim.Count <= countdown.Get().Timer.Tim.End)
		{
			Info.List.Poly.x += FONT_MOVE_SPPED;
			if (Info.fLimitPos <= Info.List.Poly.x)
			{
				Info.bUpdate = B_OFF;
				Info.bDraw = B_OFF;
			}
		}
	}

	return Info;
}

//-----------------------------------------------------------------
// 撃破数フォント
INFORMATION CInformation::InfoFont01(INFORMATION Info, bool InfoFont00)
{
	if (InfoFont00 != B_ON)
	{
		if (Info.bUpdate == B_ON)
		{
			Info.bDraw = B_ON;
			if (Info.List.Poly.x <= Info.fLimitPos)
			{
				CCounter counter;

				Info.List.Poly.x += FONT_MOVE_SPPED;
				counter.MoveCounter(FONT_MOVE_SPPED);
				if (Info.fLimitPos <= Info.List.Poly.x)
				{
					Info.List.Poly.x -= (Info.List.Poly.x - Info.fLimitPos);
					Info.bUpdate = B_OFF;
				}
			}
		}
	}

	return Info;
}

INFORMATION CInformation::InfoFont5(INFORMATION Info)
{
	if (Info.bUpdate == B_ON)
	{
		Info.bDraw = B_ON;

		// α値変更
		if (Info.Time.Count >= (Info.Time.End / NUMBERPALETTE_2))
		{
			Info.List.Color.a = I_NULL;
		}

		// α値変更
		if (Info.Time.Count >= Info.Time.End)
		{
			Info.List.Color.a = _RGBACOLOR_;
			Info.Time.Count = I_NULL;
		}
		Info.Time.Count++;
	}

	return Info;
}


//-----------------------------------------------------------------
// WIN & LOUS
INFORMATION CInformation::InfoFont02_03(INFORMATION Info)
{
	if (Info.bUpdate == B_ON)
	{
		Info.bDraw = B_ON;
		
		if (Info.List.Poly.y <= ((SCREEN_HEIGHT / CENTER_POS) - (Info.List.Poly.h / CENTER_POS)))
		{
			Info.List.Poly.y += FONT_MOVE_SPPED;
			if (((SCREEN_HEIGHT / CENTER_POS) - (Info.List.Poly.h / CENTER_POS)) <= Info.List.Poly.y)
			{
				Info.List.Poly.y -= Info.List.Poly.y - ((SCREEN_HEIGHT / CENTER_POS) - (Info.List.Poly.h / CENTER_POS));
				InfoFont04Draw();
				Info.Enter = B_ON;
				Info.bUpdate = B_OFF;
			}
		}
	}

	return Info;
}

void CInformation::InfoFont02Draw(void)
{
	if (g_aInformation[3].bDraw != B_ON)
	{
		CRanking ranking;
		CCounter countre;

		ranking.GetPoint(countre.Get());
		g_aInformation[3].bUpdate = B_ON;
	}
}

void CInformation::InfoFont03Draw(void)
{
	if (g_aInformation[4].bDraw != B_ON)
	{
		CRanking ranking;
		CCounter countre;

		ranking.GetPoint(countre.Get());
		g_aInformation[4].bUpdate = B_ON;
	}
}

bool CInformation::GetEnter(void)
{
	if (g_aInformation[3].Enter != B_OFF)
	{
		return B_ON;
	}

	if (g_aInformation[4].Enter != B_OFF)
	{
		return B_ON;
	}

	return B_OFF;
}

void CInformation::InfoFont04Draw(void)
{
	if (g_aInformation[5].bUpdate == B_OFF)
	{
		g_aInformation[5].bUpdate = B_ON;
	}
}