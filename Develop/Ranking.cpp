//=================================================================
// ランキング[Ranking.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\ColorPallet.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "Number.h"
#include "Ranking.h"
#include "Keyboard.h"
#include "Restriction.h"
#include "input.h"

//=================================================================
// グローバル変数
static int *g_nPoint; // 得点格納
static int *g_NumData; // データ数
static int *g_nPas; // パス
static int *g_nkeyCount; // キーボード入力カウンタ
static int *g_nCount; // 表示切替カウンタ
static int *g_nManu; // 選択ボタン切替
static bool *g_bNemeInputWindow; // 名前入力ウィンドウ表示判定
static bool *g_bUpdate; // 更新判定
static bool *g_bNameInput; // 名前入力判定
static bool *g_bRiset; // 入力情報リセット
static RANKINGDATA *g_aRankingData; // ランキングデータ
static POLYGON *g_aWindow; // ウィンドウポリゴン
static POLYGON *g_aRankingPolygon; // ランキングポリゴン
static RANKINGFONT *g_aRankingFont; // ランキングフォント
static NUMBER *g_aNumber; // 数字
static NAMEWINDOW *g_aNameWindow; // 名前入力ウィンドウ
static bool *g_bBotoon;

//-----------------------------------------------------------------
// 初期化
void CRanking::Init(void)
{
	g_bBotoon = MemoryAllocation(g_bBotoon, 1);
	g_nManu = MemoryAllocation(g_nManu, 1);
	g_bRiset = MemoryAllocation(g_bRiset, 1);
	g_nCount = MemoryAllocation(g_nCount, 1);
	g_bNemeInputWindow = MemoryAllocation(g_bNemeInputWindow, 1);
	g_aNumber = MemoryAllocation(g_aNumber, 1);
	g_bNameInput = MemoryAllocation(g_bNameInput, 1);
	g_bUpdate = MemoryAllocation(g_bUpdate, 1);
	g_nkeyCount = MemoryAllocation(g_nkeyCount, 1);
	g_NumData = MemoryAllocation(g_NumData, 1);
	g_NumData[0] = 0;
	Load();
	g_aRankingData[g_NumData[0] - 1].Score = 0;
	g_aRankingData[g_NumData[0] - 1].Ranking = g_NumData[0];
	for (int n = 0; n < NAMELIMIT; n++)
	{
		g_aRankingData[g_NumData[0] - 1].NameCase[n] = 0;
	}
	g_nPas = MemoryAllocation(g_nPas, 1);
	g_nPas[0] = 0;
	g_nkeyCount[0] = 0;
	g_bUpdate[0] = false;
	g_bNameInput[0] = false;
	g_bNemeInputWindow[0] = false;
	g_bRiset[0] = false;
	g_bBotoon[0] = false;
	g_nManu[0] = 0;
	g_nCount[0] = 0;
}

//-----------------------------------------------------------------
// 描画のための初期化
void CRanking::DrawInit(void)
{
	CNumber number;
	CPolygon polygon;
	CKey key;
	int nData = (g_NumData[0] - 1);
	
	FLOAT_List Pos[1], FontPos[1];

	g_aRankingPolygon = MemoryAllocation(g_aRankingPolygon, nData);
	g_aRankingFont = MemoryAllocation(g_aRankingFont, nData);
	g_aWindow = MemoryAllocation(g_aWindow, 1);

	// ウィンドウ初期化
	g_aWindow[0] = polygon.PolygonDataInitList();
	g_aWindow[0].TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aWindow[0].Poly.w = SCREEN_WIDTH / 1.2f;
	g_aWindow[0].Poly.h = SCREEN_HEIGHT;
	g_aWindow[0].Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aWindow[0].Poly.w / CENTER_POS);
	g_aWindow[0].Poly.y = (SCREEN_HEIGHT / CENTER_POS) - (g_aWindow[0].Poly.h / CENTER_POS);
	g_aWindow[0].Color.r = COLORPALLET_0;
	g_aWindow[0].Color.g = COLORPALLET_255;
	g_aWindow[0].Color.b = COLORPALLET_252;
	g_aWindow[0].Color.a = COLORPALLET_150;
	Pos[0] = polygon.PolygonDataInitList().Poly;
	
	// ランキング用の初期化
	for (int i = 0; i < nData; i++)
	{
		// 背景
		g_aRankingPolygon[i] = polygon.PolygonDataInitList();
		g_aRankingPolygon[i].TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
		g_aRankingPolygon[i].Poly.w = SCREEN_WIDTH / 1.3f;
		g_aRankingPolygon[i].Poly.h = 200.0f;
		g_aRankingPolygon[i].Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aRankingPolygon[i].Poly.w / CENTER_POS);
		g_aRankingPolygon[i].Poly.y = g_aWindow[0].Poly.y + 30.0f + Pos[0].h+ Pos[0].y;
		g_aRankingPolygon[i].Color.a = COLORPALLET_200;
		Pos[0] = g_aRankingPolygon[i].Poly;

		// 数字
		g_aRankingFont[i].RankingNumBer = polygon.PolygonDataInitList();
		g_aRankingFont[i].RankingNumBer.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE_BIGWhiteNumber;
		g_aRankingFont[i].RankingNumBer.Anim.Count = g_aRankingData[i].Ranking;
		g_aRankingFont[i].RankingNumBer.Anim.Direction = 10;
		g_aRankingFont[i].RankingNumBer.Anim.Pattern = 10;
		g_aRankingFont[i].RankingNumBer.Anim.Frame = 1;
		g_aRankingFont[i].RankingNumBer.Anim.AnimationON = B_ON;
		g_aRankingFont[i].RankingNumBer.TexCut.x = g_aRankingFont[i].RankingNumBer.TexCut.w = (polygon.GetTextureInfo(g_aRankingFont[i].RankingNumBer.TexName).Widht / g_aRankingFont[i].RankingNumBer.Anim.Pattern);
		g_aRankingFont[i].RankingNumBer.TexCut.y = g_aRankingFont[i].RankingNumBer.TexCut.h = polygon.GetTextureInfo(g_aRankingFont[i].RankingNumBer.TexName).Height;
		g_aRankingFont[i].RankingNumBer.Poly.w = (float)g_aRankingFont[i].RankingNumBer.TexCut.w / CENTER_POS;
		g_aRankingFont[i].RankingNumBer.Poly.h = (float)g_aRankingFont[i].RankingNumBer.TexCut.h / CENTER_POS;
		g_aRankingFont[i].RankingNumBer.Poly.x = Pos[0].x + 10.0f;
		g_aRankingFont[i].RankingNumBer.Poly.y = Pos[0].y + 10.0f;
		g_aRankingFont[i].RankingNumBer.bAffine = B_ON;
		g_aRankingFont[i].RankingNumBer.Ang.x = g_aRankingFont[i].RankingNumBer.Scale.x = _DIVISION_(g_aRankingFont[i].RankingNumBer.Poly.w, CENTER_POS);
		g_aRankingFont[i].RankingNumBer.Ang.y = g_aRankingFont[i].RankingNumBer.Scale.y = _DIVISION_(g_aRankingFont[i].RankingNumBer.Poly.h, CENTER_POS);
		g_aRankingFont[i].NunBer.Digit = 1;
		g_aRankingFont[i].RankingNumBer.Color.r = COLORPALLET_50;
		g_aRankingFont[i].RankingNumBer.Color.g = COLORPALLET_50;
		g_aRankingFont[i].RankingNumBer.Color.b = COLORPALLET_50;
		if (g_aRankingData[i].Ranking == 1)
		{
			g_aRankingFont[i].RankingNumBer.Color.r = COLORPALLET_255;
			g_aRankingFont[i].RankingNumBer.Color.g = COLORPALLET_215;
			g_aRankingFont[i].RankingNumBer.Color.b = COLORPALLET_0;
		}
		if (g_aRankingData[i].Ranking == 2)
		{
			g_aRankingFont[i].RankingNumBer.Color.r = COLORPALLET_192;
			g_aRankingFont[i].RankingNumBer.Color.g = COLORPALLET_192;
			g_aRankingFont[i].RankingNumBer.Color.b = COLORPALLET_192;
		}
		if (g_aRankingData[i].Ranking == 3)
		{
			g_aRankingFont[i].RankingNumBer.Color.r = COLORPALLET_196;
			g_aRankingFont[i].RankingNumBer.Color.g = COLORPALLET_112;
			g_aRankingFont[i].RankingNumBer.Color.b = COLORPALLET_34;
		}
		if (g_aRankingData[i].Ranking >= 10) { g_aRankingFont[i].NunBer.Digit = 2; }
		if (g_aRankingData[i].Ranking >= 100) { g_aRankingFont[i].NunBer.Digit = 3; }

		// 順位
		g_aRankingFont[i].RankingFont = polygon.PolygonDataInitList();
		g_aRankingFont[i].RankingFont.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE_Ranking;
		g_aRankingFont[i].RankingFont.Poly.w = (float)polygon.GetTextureInfo(g_aRankingFont[i].RankingFont.TexName).Widht / 5.0f;
		g_aRankingFont[i].RankingFont.Poly.h = (float)polygon.GetTextureInfo(g_aRankingFont[i].RankingFont.TexName).Height / 5.0f;
		g_aRankingFont[i].RankingFont.Poly.x = g_aRankingFont[i].RankingNumBer.Poly.x + (g_aRankingFont[i].RankingNumBer.Poly.w * g_aRankingFont[i].NunBer.Digit);
		g_aRankingFont[i].RankingFont.Poly.y = g_aRankingFont[i].RankingNumBer.Poly.y + 10.0f;
		g_aRankingFont[i].RankingFont.Color.r = COLORPALLET_50;
		g_aRankingFont[i].RankingFont.Color.g = COLORPALLET_50;
		g_aRankingFont[i].RankingFont.Color.b = COLORPALLET_50;
		if (g_aRankingData[i].Ranking == 1)
		{
			g_aRankingFont[i].RankingFont.Color.r = COLORPALLET_255;
			g_aRankingFont[i].RankingFont.Color.g = COLORPALLET_215;
			g_aRankingFont[i].RankingFont.Color.b = COLORPALLET_0;
		}
		if (g_aRankingData[i].Ranking == 2)
		{
			g_aRankingFont[i].RankingFont.Color.r = COLORPALLET_192;
			g_aRankingFont[i].RankingFont.Color.g = COLORPALLET_192;
			g_aRankingFont[i].RankingFont.Color.b = COLORPALLET_192;
		}
		if (g_aRankingData[i].Ranking == 3)
		{
			g_aRankingFont[i].RankingFont.Color.r = COLORPALLET_196;
			g_aRankingFont[i].RankingFont.Color.g = COLORPALLET_112;
			g_aRankingFont[i].RankingFont.Color.b = COLORPALLET_34;
		}

		// 撃破数フォント
		g_aRankingFont[i].BontFont = polygon.PolygonDataInitList();
		g_aRankingFont[i].BontFont.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE_BONTFONT;
		g_aRankingFont[i].BontFont.Poly.w = (float)polygon.GetTextureInfo(g_aRankingFont[i].BontFont.TexName).Widht / 4.0f;
		g_aRankingFont[i].BontFont.Poly.h = (float)polygon.GetTextureInfo(g_aRankingFont[i].BontFont.TexName).Height / 4.0f;
		g_aRankingFont[i].BontFont.Poly.x = 100.0f + (g_aRankingFont[i].BontFont.Poly.w / CENTER_POS);
		g_aRankingFont[i].BontFont.Poly.y = g_aRankingFont[i].RankingFont.Poly.y + g_aRankingFont[i].BontFont.Poly.h;
		g_aRankingFont[i].BontFont.Color.r = COLORPALLET_50;
		g_aRankingFont[i].BontFont.Color.g = COLORPALLET_50;
		g_aRankingFont[i].BontFont.Color.b = COLORPALLET_50;

		// 数字
		g_aRankingFont[i].ScoreFont = polygon.PolygonDataInitList();
		g_aRankingFont[i].ScoreFont.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE_BIGWhiteNumber;
		g_aRankingFont[i].ScoreFont.Anim.Count = g_aRankingData[i].Score;
		g_aRankingFont[i].ScoreFont.Anim.Direction = 10;
		g_aRankingFont[i].ScoreFont.Anim.Pattern = 10;
		g_aRankingFont[i].ScoreFont.Anim.Frame = 1;
		g_aRankingFont[i].ScoreFont.Anim.AnimationON = B_ON;
		g_aRankingFont[i].ScoreFont.TexCut.x = g_aRankingFont[i].ScoreFont.TexCut.w = (polygon.GetTextureInfo(g_aRankingFont[i].ScoreFont.TexName).Widht / g_aRankingFont[i].ScoreFont.Anim.Pattern);
		g_aRankingFont[i].ScoreFont.TexCut.y = g_aRankingFont[i].ScoreFont.TexCut.h = polygon.GetTextureInfo(g_aRankingFont[i].ScoreFont.TexName).Height;
		g_aRankingFont[i].ScoreFont.Poly.w = (float)g_aRankingFont[i].ScoreFont.TexCut.w / CENTER_POS;
		g_aRankingFont[i].ScoreFont.Poly.h = (float)g_aRankingFont[i].ScoreFont.TexCut.h / CENTER_POS;
		g_aRankingFont[i].ScoreFont.Poly.x = g_aRankingFont[i].BontFont.Poly.x + g_aRankingFont[i].BontFont.Poly.w;
		g_aRankingFont[i].ScoreFont.Poly.y = g_aRankingFont[i].BontFont.Poly.y;
		g_aRankingFont[i].ScoreFont.bAffine = B_ON;
		g_aRankingFont[i].ScoreFont.Ang.x = g_aRankingFont[i].ScoreFont.Scale.x = _DIVISION_(g_aRankingFont[i].ScoreFont.Poly.w, CENTER_POS);
		g_aRankingFont[i].ScoreFont.Ang.y = g_aRankingFont[i].ScoreFont.Scale.y = _DIVISION_(g_aRankingFont[i].ScoreFont.Poly.h, CENTER_POS);
		g_aRankingFont[i].ScoreFont.Color.r = COLORPALLET_50;
		g_aRankingFont[i].ScoreFont.Color.g = COLORPALLET_50;
		g_aRankingFont[i].ScoreFont.Color.b = COLORPALLET_50;
		if (g_aRankingData[i].Ranking == 1)
		{
			g_aRankingFont[i].ScoreFont.Color.r = COLORPALLET_255;
			g_aRankingFont[i].ScoreFont.Color.g = COLORPALLET_0;
			g_aRankingFont[i].ScoreFont.Color.b = COLORPALLET_0;
		}

		// 名前フォント
		for (int n = 0; n < NAMELIMIT; n++)
		{
			g_aRankingFont[i].NameFont[n] = polygon.PolygonDataInitList();
			g_aRankingFont[i].NameFont[n].TexName = key.KeyList(g_aRankingData[i].NameCase[n]);
			g_aRankingFont[i].NameFont[n].Poly.w = (float)polygon.GetTextureInfo(g_aRankingFont[i].NameFont[n].TexName).Widht / 5.5f;
			g_aRankingFont[i].NameFont[n].Poly.h = (float)polygon.GetTextureInfo(g_aRankingFont[i].NameFont[n].TexName).Height / 5.5f;
			g_aRankingFont[i].NameFont[n].Poly.x = 100.0f + g_aRankingFont[i].RankingFont.Poly.x + g_aRankingFont[i].RankingFont.Poly.w + (g_aRankingFont[i].NameFont[n].Poly.w*n);
			g_aRankingFont[i].NameFont[n].Poly.y = g_aRankingFont[i].RankingFont.Poly.y;
			g_aRankingFont[i].NameFont[n].Color.r = COLORPALLET_50;
			g_aRankingFont[i].NameFont[n].Color.g = COLORPALLET_50;
			g_aRankingFont[i].NameFont[n].Color.b = COLORPALLET_50;
			if (g_aRankingData[i].Ranking == 1)
			{
				g_aRankingFont[i].NameFont[n].Color.r = COLORPALLET_255;
				g_aRankingFont[i].NameFont[n].Color.g = COLORPALLET_215;
				g_aRankingFont[i].NameFont[n].Color.b = COLORPALLET_0;
			}
			if (g_aRankingData[i].Ranking == 2)
			{
				g_aRankingFont[i].NameFont[n].Color.r = COLORPALLET_192;
				g_aRankingFont[i].NameFont[n].Color.g = COLORPALLET_192;
				g_aRankingFont[i].NameFont[n].Color.b = COLORPALLET_192;
			}
			if (g_aRankingData[i].Ranking == 3)
			{
				g_aRankingFont[i].NameFont[n].Color.r = COLORPALLET_196;
				g_aRankingFont[i].NameFont[n].Color.g = COLORPALLET_112;
				g_aRankingFont[i].NameFont[n].Color.b = COLORPALLET_34;
			}
			if (g_aRankingData[i].NameCase[n] == 0)
			{
				g_aRankingFont[i].NameFont[n].Color.a = COLORPALLET_0;
			}
		}
		FontPos[0] = g_aRankingFont[i].NameFont[0].Poly;
		g_aRankingFont[i].NunBer.Max = number.Init(g_aRankingFont[i].NunBer.Digit);
		g_aRankingFont[i].NunBer.LeftAlignment = B_OFF;
		g_aRankingFont[i].NunBer.Zero = B_OFF;
	}
	g_aNumber[0].Digit = 3;
	g_aNumber[0].Max = number.Init(g_aNumber[0].Digit);
	g_aNumber[0].LeftAlignment = B_ON;
	g_aNumber[0].Zero = B_ON;
}

//-----------------------------------------------------------------
// 終了処理
void CRanking::Uninit(void)
{
	AllMemoryDelete(g_nManu);
	AllMemoryDelete(g_bBotoon);
	AllMemoryDelete(g_bRiset);
	AllMemoryDelete(g_nCount);
	AllMemoryDelete(g_aNameWindow);
	AllMemoryDelete(g_bNemeInputWindow);
	AllMemoryDelete(g_bNameInput);
	AllMemoryDelete(g_bUpdate);
	AllMemoryDelete(g_aRankingFont);
	AllMemoryDelete(g_aRankingPolygon);
	AllMemoryDelete(g_aWindow);
	AllMemoryDelete(g_aNumber);
	AllMemoryDelete(g_nkeyCount);
	AllMemoryDelete(g_nPas);
	AllMemoryDelete(g_nPoint);
	AllMemoryDelete(g_NumData);
	AllMemoryDelete(g_aRankingData);
}

//-----------------------------------------------------------------
// 順位更新処理
void CRanking::Update(void)
{
	if (g_bUpdate[0] == false)
	{
		// サーチ
		for (int i = (g_NumData[0] - 1); 0 <= i; i--)
		{
			g_nPas[0] = i;
			if (g_nPoint[0] <= g_aRankingData[i].Score)
			{
				g_nPas[0] = i + 1;
				break;
			}
		}

		// セット
		if (g_NumData[0] != g_nPas[0]) // データ数と一致していない時に処理
		{
			if ((g_NumData[0] - 1) == g_nPas[0])
			{
				g_aRankingData[g_nPas[0]].Score = g_nPoint[0];
			}
			else
			{
				for (int i = (g_NumData[0] - 2); 0 <= i; i--)
				{
					g_aRankingData[i + 1] = g_aRankingData[i];
					g_aRankingData[i + 1].Ranking++;
					if (g_nPas[0] == i)
					{
						g_aRankingData[i].Ranking = (i + 1);
						g_aRankingData[i].Score = g_nPoint[0];
						break;
					}
				}
			}
		}
		g_bUpdate[0] = true;
	}
}

//-----------------------------------------------------------------
// 描画開始
void CRanking::Draw(void)
{
	CPolygon polygon;
	CNumber number;
	CRestriction restriction;
	int nData = (g_NumData[0] - 1);

	int nArea = polygon.StaticAreaStart();
	polygon.Draw(g_aWindow[0]);
	polygon.StaticAreaEnd(nArea);

	for (int i = 0; i < nData; i++)
	{
		if (restriction.Draw(g_aRankingPolygon[i].Poly) == true)
		{
			nArea = polygon.StaticAreaStart();
			polygon.Draw(g_aRankingPolygon[i]);
			polygon.StaticAreaEnd(nArea);

			number.Draw(g_aRankingFont[i].RankingNumBer, g_aRankingFont[i].NunBer, g_aRankingFont[i].RankingNumBer.Anim.Count);
			polygon.Draw(g_aRankingFont[i].RankingFont);
			for (int n = 0; n < NAMELIMIT; n++)
			{
				polygon.Draw(g_aRankingFont[i].NameFont[n]);
			}
			polygon.Draw(g_aRankingFont[i].BontFont);
			number.Draw(g_aRankingFont[i].ScoreFont, g_aNumber[0], g_aRankingFont[i].ScoreFont.Anim.Count);
		}
	}
}

//-----------------------------------------------------------------
// ランキングウィンドウスクロール
void CRanking::WindowScroll(void)
{
	int nData = (g_NumData[0] - 1);
	int nSpeed;

	nSpeed = Scroll();

	// スクロール判定切替
	switch (nSpeed)
	{
	case -1:
		for (int i = nData; 0 <= i; i--)
		{
			if (g_aRankingPolygon[0].Poly.y < 30.0f)
			{
				g_aRankingPolygon[i].Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].BontFont.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].RankingFont.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].RankingNumBer.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].ScoreFont.Poly.y -= nSpeed * RANKING_SPPED;
				for (int n = 0; n < NAMELIMIT; n++)
				{
					g_aRankingFont[i].NameFont[n].Poly.y -= nSpeed * RANKING_SPPED;
				}
			}
		}
		break;
	case 1:
		for (int i = 0; i < nData; i++)
		{
			if (g_aRankingPolygon[nData - 1].Poly.y >(SCREEN_HEIGHT - 30.0f - g_aRankingPolygon[nData - 1].Poly.h))
			{
				g_aRankingPolygon[i].Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].BontFont.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].RankingFont.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].RankingNumBer.Poly.y -= nSpeed * RANKING_SPPED;
				g_aRankingFont[i].ScoreFont.Poly.y -= nSpeed * RANKING_SPPED;
				for (int n = 0; n < NAMELIMIT; n++)
				{
					g_aRankingFont[i].NameFont[n].Poly.y -= nSpeed * RANKING_SPPED;
				}
			}
		}
		break;
	default:
		break;
	}
}

//-----------------------------------------------------------------
// スクロール
int CRanking::Scroll(void)
{
	if (_REPEAT_(DIK_W) || _REPEAT_(DIK_UP))
	{
		return -1;
	}

	if (_REPEAT_(DIK_S) || _REPEAT_(DIK_DOWN))
	{
		return 1;
	}

	return 0;
}

//-----------------------------------------------------------------
// 名前入力ウィンドウ生成
void CRanking::NameWindowInit(void)
{
	CPolygon polygon;
	CKey key;
	
	g_aNameWindow = MemoryAllocation(g_aNameWindow, 1);
	Update();

	g_aNameWindow[0].NameWindow = polygon.PolygonDataInitList();
	g_aNameWindow[0].NameWindow.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
	g_aNameWindow[0].NameWindow.Poly.w = SCREEN_WIDTH / 1.2f;
	g_aNameWindow[0].NameWindow.Poly.h = SCREEN_HEIGHT;
	g_aNameWindow[0].NameWindow.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aNameWindow[0].NameWindow.Poly.w / CENTER_POS);
	g_aNameWindow[0].NameWindow.Poly.y = SCREEN_WIDTH;
	g_aNameWindow[0].NameWindow.Color.r = COLORPALLET_0;
	g_aNameWindow[0].NameWindow.Color.g = COLORPALLET_255;
	g_aNameWindow[0].NameWindow.Color.b = COLORPALLET_252;
	g_aNameWindow[0].NameWindow.Color.a = COLORPALLET_150;

	g_aNameWindow[0].YourName = polygon.PolygonDataInitList();
	g_aNameWindow[0].YourName.TexName = TEXTURE_RESULT_YourName;
	g_aNameWindow[0].YourName.Poly.w = (float)polygon.GetTextureInfo(g_aNameWindow[0].YourName.TexName).Widht / 1.5f;
	g_aNameWindow[0].YourName.Poly.h = (float)polygon.GetTextureInfo(g_aNameWindow[0].YourName.TexName).Height / 1.5f;
	g_aNameWindow[0].YourName.Poly.y = 100.f;
	g_aNameWindow[0].YourName.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aNameWindow[0].YourName.Poly.w / CENTER_POS);

	for (int n = 0; n < NAMELIMIT; n++)
	{
		g_aNameWindow[0].NameFont[n] = polygon.PolygonDataInitList();
		g_aNameWindow[0].NameFont[n].TexName = key.KeyList(0);
		g_aNameWindow[0].NameFont[n].Poly.w = (float)polygon.GetTextureInfo(g_aNameWindow[0].NameFont[n].TexName).Widht / 5.5f;
		g_aNameWindow[0].NameFont[n].Poly.h = (float)polygon.GetTextureInfo(g_aNameWindow[0].NameFont[n].TexName).Height / 5.5f;
		g_aNameWindow[0].NameFont[n].Poly.x = g_aNameWindow[0].YourName.Poly.x + g_aNameWindow[0].NameFont[n].Poly.w*n;
		g_aNameWindow[0].NameFont[n].Poly.y = g_aNameWindow[0].YourName.Poly.y + g_aNameWindow[0].NameFont[n].Poly.h + 50.0f;
		g_aNameWindow[0].NameFont[n].Color.a = COLORPALLET_0;
	}

	g_aNameWindow[0].OKQUESTION = polygon.PolygonDataInitList();
	g_aNameWindow[0].OKQUESTION.TexName = TEXTURE_RESULT_OKQ;
	g_aNameWindow[0].OKQUESTION.Poly.w = (float)polygon.GetTextureInfo(g_aNameWindow[0].OKQUESTION.TexName).Widht / CENTER_POS;
	g_aNameWindow[0].OKQUESTION.Poly.h = (float)polygon.GetTextureInfo(g_aNameWindow[0].OKQUESTION.TexName).Height / CENTER_POS;
	g_aNameWindow[0].OKQUESTION.Poly.y = 300.0f;
	g_aNameWindow[0].OKQUESTION.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aNameWindow[0].OKQUESTION.Poly.w / CENTER_POS);

	g_aNameWindow[0].OK = polygon.PolygonDataInitList();
	g_aNameWindow[0].OK.TexName = TEXTURE_RESULT_OK;
	g_aNameWindow[0].OK.Poly.w = (float)polygon.GetTextureInfo(g_aNameWindow[0].OK.TexName).Widht / CENTER_POS;
	g_aNameWindow[0].OK.Poly.h = (float)polygon.GetTextureInfo(g_aNameWindow[0].OK.TexName).Height / CENTER_POS;
	g_aNameWindow[0].OK.Poly.y = 350.0f + g_aNameWindow[0].OKQUESTION.Poly.h;
	g_aNameWindow[0].OK.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aNameWindow[0].OK.Poly.w / CENTER_POS) - 100.0f - g_aNameWindow[0].OK.Poly.w;
	g_aNameWindow[0].OK.Color.r = COLORPALLET_255;
	g_aNameWindow[0].OK.Color.g = COLORPALLET_253;
	g_aNameWindow[0].OK.Color.b = COLORPALLET_49;

	g_aNameWindow[0].NO = polygon.PolygonDataInitList();
	g_aNameWindow[0].NO.TexName = TEXTURE_RESULT_NO;
	g_aNameWindow[0].NO.Poly.w = (float)polygon.GetTextureInfo(g_aNameWindow[0].NO.TexName).Widht / CENTER_POS;
	g_aNameWindow[0].NO.Poly.h = (float)polygon.GetTextureInfo(g_aNameWindow[0].NO.TexName).Height / CENTER_POS;
	g_aNameWindow[0].NO.Poly.y = 350.0f + g_aNameWindow[0].OKQUESTION.Poly.h;
	g_aNameWindow[0].NO.Poly.x = (SCREEN_WIDTH / CENTER_POS) - (g_aNameWindow[0].NO.Poly.w / CENTER_POS) + 100.0f + g_aNameWindow[0].NO.Poly.w;
}

//-----------------------------------------------------------------
// 限定描画
RANKINGDATA CRanking::Draw2(void)
{
	return g_aRankingData[g_nPas[0]];
}

void CRanking::Draw3(void)
{
	CPolygon polygon;

	if (g_bNemeInputWindow[0] == true)
	{
		int nArea = polygon.StaticAreaStart();
		polygon.Draw(g_aNameWindow[0].NameWindow);
		polygon.StaticAreaEnd(nArea);
		for (int n = 0; n < NAMELIMIT; n++)
		{
			polygon.Draw(g_aNameWindow[0].NameFont[n]);
		}
		polygon.Draw(g_aNameWindow[0].YourName);

		if (g_bRiset[0] == true)
		{
			polygon.Draw(g_aNameWindow[0].OKQUESTION);
			polygon.Draw(g_aNameWindow[0].OK);
			polygon.Draw(g_aNameWindow[0].NO);
		}
	}
}

//-----------------------------------------------------------------
// 名前入力
bool CRanking::SetName(void)
{
	CKey key;
	CPolygon polygon;
	int nKey;
	bool bOK = false;

	// 名前入力可能なとき
	if (g_bNameInput[0] != true)
	{
		// エンター
		if (_TRIGGER_(DIK_RETURN))
		{
			PlaySound(LOAD_SOUND_LIST::Button_SE);
			if (g_bNemeInputWindow[0] == false)
			{
				g_bNemeInputWindow[0] = true;
			}
		}

		// 名前入力ウィンドウの動き
		if (g_bNemeInputWindow[0] == true)
		{
			g_aNameWindow[0].NameWindow.Poly.y -= (float)(SCREEN_HEIGHT / 10);
			if (F_NULL >= g_aNameWindow[0].NameWindow.Poly.y)
			{
				g_aNameWindow[0].NameWindow.Poly.y = F_NULL;
			}
		}

		// 名前入力ウィンドウ
		if (g_bNameInput[0] != true && g_bNemeInputWindow[0] == true)
		{
			// 未入力点滅
			if (g_nCount[0] == 35) { g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color = polygon.PolygonDataInitList().Color; }
			if (g_nCount[0] == 70) { g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.a = COLORPALLET_0; g_nCount[0] = 0; }

			nKey = key.KeyBoard();

			// エンター
			if (_TRIGGER_(DIK_RETURN))
			{
				// 入力字数が0とNAMELIMITでないとき
				if (g_nkeyCount[0] != 0 && g_nkeyCount[0] != NAMELIMIT)
				{
					g_bBotoon[0] = true;
					g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.a = COLORPALLET_0;
					for (int i = g_nkeyCount[0]; i < NAMELIMIT; i++)
					{
						g_aRankingData[g_nPas[0]].NameCase[i] = 0;
					}
					g_nkeyCount[0] = NAMELIMIT;
				}
			}
			else
			{
				g_bBotoon[0] = false;
			}

			// 文字セット
			if (nKey < _NUMKEYBOARD_)
			{
				PlaySound(LOAD_SOUND_LIST::Button_SE);
				if (g_nkeyCount[0] != NAMELIMIT)
				{
					switch (nKey)
					{
					case 0:
						if (g_nkeyCount[0] != 0)
						{
							g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color = polygon.PolygonDataInitList().Color;
							g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.a = COLORPALLET_0;
							g_nkeyCount[0]--;
							g_aNameWindow[0].NameFont[g_nkeyCount[0]].TexName = key.KeyList(nKey);
							g_aRankingData[g_nPas[0]].NameCase[g_nkeyCount[0]] = nKey;
						}
						break;
					default:
						g_aNameWindow[0].NameFont[g_nkeyCount[0]].TexName = key.KeyList(nKey);
						g_aRankingData[g_nPas[0]].NameCase[g_nkeyCount[0]] = nKey;
						g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.a = COLORPALLET_255;
						g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.r = COLORPALLET_0;
						g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.g = COLORPALLET_0;
						g_aNameWindow[0].NameFont[g_nkeyCount[0]].Color.b = COLORPALLET_0;
						g_nkeyCount[0]++;
						break;
					}
				}
			}

			// 入力字数がNAMELIMITの時
			if (g_nkeyCount[0] == NAMELIMIT)
			{
				g_bRiset[0] = true;
				if (_TRIGGER_(DIK_A) || _TRIGGER_(DIK_LEFT))
				{
					PlaySound(LOAD_SOUND_LIST::Button_SE);
					g_nManu[0] = 0;
					g_aNameWindow[0].OK.Color.r = COLORPALLET_255;
					g_aNameWindow[0].OK.Color.g = COLORPALLET_253;
					g_aNameWindow[0].OK.Color.b = COLORPALLET_49;
					g_aNameWindow[0].NO.Color = polygon.PolygonDataInitList().Color;
				}
				if (_TRIGGER_(DIK_D) || _TRIGGER_(DIK_RIGHT))
				{
					PlaySound(LOAD_SOUND_LIST::Button_SE);
					g_nManu[0] = 1;
					g_aNameWindow[0].NO.Color.r = COLORPALLET_255;
					g_aNameWindow[0].NO.Color.g = COLORPALLET_253;
					g_aNameWindow[0].NO.Color.b = COLORPALLET_49;
					g_aNameWindow[0].OK.Color = polygon.PolygonDataInitList().Color;
				}
				if (_TRIGGER_(DIK_RETURN) && g_bBotoon[0] == false)
				{
					PlaySound(LOAD_SOUND_LIST::Button_SE);
					switch (g_nManu[0])
					{
					case 0:
						bOK = true;
						break;
					case 1:
						g_bRiset[0] = _BOOL_(g_bRiset[0]);
						g_nkeyCount[0] = 0;
						for (int i = 0; i < NAMELIMIT; i++)
						{
							g_aNameWindow[0].NameFont[i].Color.a = COLORPALLET_0;
							g_aRankingData[g_nPas[0]].NameCase[i] = 0;
							g_aNameWindow[0].NameFont[i].TexName = key.KeyList(0);
						}
						g_aNameWindow[0].OK.Color.r = COLORPALLET_255;
						g_aNameWindow[0].OK.Color.g = COLORPALLET_253;
						g_aNameWindow[0].OK.Color.b = COLORPALLET_49;
						g_aNameWindow[0].NO.Color = polygon.PolygonDataInitList().Color;
						g_nManu[0] = 0;
						break;
					default:
						break;
					}
				}
			}

			if (bOK == true)
			{
				Save();
				g_bNameInput[0] = true;
			}
			g_nCount[0]++;
			g_aNameWindow[0].NameWindow.Color.r = COLORPALLET_0;
			g_aNameWindow[0].NameWindow.Color.g = COLORPALLET_255;
			g_aNameWindow[0].NameWindow.Color.b = COLORPALLET_252;
			g_aNameWindow[0].NameWindow.Color.a = COLORPALLET_150;
		}
	}

	return g_bNameInput[0];
}

//-----------------------------------------------------------------
// データの保存
void CRanking::Save(void)
{
	FILE *pFile;

	pFile = fopen(SCORE_LINK, "wb");
	if (pFile)
	{
		// 書き込む数を書き込む
		fwrite(&g_NumData[0], sizeof(int), 1, pFile);

		// 数分書き込む
		fwrite(g_aRankingData, sizeof(RANKINGDATA), g_NumData[0], pFile);
		fclose(pFile);
	}
}

//-----------------------------------------------------------------
// データの読み取り
void CRanking::Load(void)
{
	FILE *pFile;

	pFile = fopen(SCORE_LINK, "rb");
	if (pFile)
	{
		// データ数読み込み
		fread(&g_NumData[0], 4, 1, pFile);
		g_NumData[0]++;
		g_aRankingData = MemoryAllocation(g_aRankingData, g_NumData[0]);
		fread(g_aRankingData, sizeof(RANKINGDATA), g_NumData[0] - 1, pFile);
		fclose(pFile);
	}
}

//-----------------------------------------------------------------
// 得点格納
void CRanking::GetPoint(int Point)
{
	if (g_nPoint == NULL)
	{
		g_nPoint = MemoryAllocation(g_nPoint, 1);
	}
	g_nPoint[0] = Point;
}