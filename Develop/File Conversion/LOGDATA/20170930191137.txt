 ********************************************** 
 *************** Conversion Log *************** 
 ********************************************** 

 Processing Start...

 FileOpen << Sound_Data.txt
 Init << SOUND_DATA

 OpenData		= 4
 
 Number			= 0
 FileName		= ./data/SOUND_CONTAINER/SE/LoadingSE.wav
 Pattern		= 0
 
 Number			= 1
 FileName		= ./data/SOUND_CONTAINER/SE/LoadingOpnSE.wav
 Pattern		= 0
 
 Number			= 2
 FileName		= ./data/SOUND_CONTAINER/SE/RPG_SE[232].wav
 Pattern		= 0
 
 Number			= 3
 FileName		= ./data/SOUND_CONTAINER/BGM/BeyondTheImmediateSide.wav
 Pattern		= -1
 
 FileClose << Sound_Data.txt

 読み込みました

 バイナリ形式に変換します
 FileOpen << Sound_Data.bin
 Conversion << Sound_Data.bin << Sound_Data.txt
 FileClose << Sound_Data.bin

 データを変換しました

 Uninit << SOUND_DATA

 FileOpen << Sound_Data.bin
 Init << SOUND_DATA

 OpenData		= 4
 
 Number			= 0
 FileName		= ./data/SOUND_CONTAINER/SE/LoadingSE.wav
 Pattern		= 0
 
 Number			= 1
 FileName		= ./data/SOUND_CONTAINER/SE/LoadingOpnSE.wav
 Pattern		= 0
 
 Number			= 2
 FileName		= ./data/SOUND_CONTAINER/SE/RPG_SE[232].wav
 Pattern		= 0
 
 Number			= 3
 FileName		= ./data/SOUND_CONTAINER/BGM/BeyondTheImmediateSide.wav
 Pattern		= -1
 
 FileClose << Sound_Data.bin

 読み込みました

 Uninit << SOUND_DATA

 Processing End...
 
 ********************************************** 
 ******************* Log End ****************** 
 ********************************************** 