//=================================================================
// 回転角度自動生成[RotationAngle.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "RotationAngle.h"

//-----------------------------------------------------------------
// 対象物に対しての方向追従
float CAngle::Angle2D(FLOAT_List Target_A, FLOAT_List Target_B)
{
	float fAngle = (float)atan2(Target_B.y - Target_A.y, Target_B.x - Target_A.x);

	return fAngle;
}
