//=================================================================
// カウントダウン[Countdown.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\NumBerPallet.h"
#include "main.h"
#include "Number.h"
#include "Timer.h"
#include "Countdown.h"
#include "Information.h"

//=================================================================
// グローバル変数
static COUNTDOWN *g_aCountdown; // カウンタ

//-----------------------------------------------------------------
// 初期化
void CCountdown::Init(void)
{
	CPolygon polygon;
	CNumber number;

	g_aCountdown = MemoryAllocation(g_aCountdown, NUM_COUNTDOWN);

	for (int i = 0; i < NUM_COUNTDOWN; i++)
	{
		g_aCountdown[i].Timer.List = polygon.PolygonDataInitList();
		g_aCountdown[i].Timer.List.TexName = LOAD_TEXTURE_LIST::TEXTURE_NumberList0;
		g_aCountdown[i].Timer.List.Anim.Count = -NUMBERPALETTE_1;
		g_aCountdown[i].Timer.List.Anim.Direction = NUM_NUMBER_W;
		g_aCountdown[i].Timer.List.Anim.Pattern = NUM_NUMBER;
		g_aCountdown[i].Timer.List.Anim.AnimationON = B_ON;
		g_aCountdown[i].Timer.List.TexCut.x = g_aCountdown[i].Timer.List.TexCut.w = NUMBER_W;
		g_aCountdown[i].Timer.List.TexCut.y = g_aCountdown[i].Timer.List.TexCut.h = NUMBER_H;
		g_aCountdown[i].Timer.List.Poly.x = (float)(SCREEN_WIDTH - (NUMBER_W * CENTER_POS));
		g_aCountdown[i].Timer.List.Poly.y = F_NULL;
		g_aCountdown[i].Timer.List.Poly.w = (float)_MULTIPLIED_(g_aCountdown[i].Timer.List.TexCut.w, TIMER_SCALE);
		g_aCountdown[i].Timer.List.Poly.h = (float)_MULTIPLIED_(g_aCountdown[i].Timer.List.TexCut.h, TIMER_SCALE);
		g_aCountdown[i].Timer.List.Anim.Frame = NUMBERPALETTE_1;
		g_aCountdown[i].Timer.List.bAffine = B_ON;
		g_aCountdown[i].Timer.List.Ang.x = g_aCountdown[i].Timer.List.Scale.x = _DIVISION_(g_aCountdown[i].Timer.List.Poly.w, CENTER_POS);
		g_aCountdown[i].Timer.List.Ang.y = g_aCountdown[i].Timer.List.Scale.y = _DIVISION_(g_aCountdown[i].Timer.List.Poly.h, CENTER_POS);
		g_aCountdown[i].Timer.Number.Digit = NUMBERPALETTE_1;
		g_aCountdown[i].Timer.Number.LeftAlignment = B_ON;
		g_aCountdown[i].Timer.Number.Max = number.Init(g_aCountdown[i].Timer.Number.Digit);
		g_aCountdown[i].Timer.Number.Zero = B_ON;
		g_aCountdown[i].Timer.Tim.Count = COUNTDOWN_TIME;
		g_aCountdown[i].Timer.Comma.Count = OneSecond;
		g_aCountdown[i].Timer.Comma.End = g_aCountdown[i].Timer.Tim.End = I_NULL;
		g_aCountdown[i].bDraw = B_ON;
		g_aCountdown[i].bUpdate = B_ON;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CCountdown::Uninit(void)
{
	AllMemoryDelete(g_aCountdown);
}

//-----------------------------------------------------------------
// 更新処理
void CCountdown::Update(void)
{
	// 描画フラグがONのとき
	if (g_aCountdown[0].bDraw == B_ON)
	{
		g_aCountdown[0] = Countdown(g_aCountdown[0]);
	}
}

//-----------------------------------------------------------------
// 描画開始
void CCountdown::Draw(void)
{
	// 描画フラグがONのとき
	if (g_aCountdown[0].bDraw == B_ON)
	{
		CPolygon polygon;
		CNumber number;

		int aArea = polygon.StaticAreaStart();
		number.Draw(g_aCountdown[0].Timer.List, g_aCountdown[0].Timer.Number, g_aCountdown[0].Timer.Tim.Count);
		polygon.StaticAreaEnd(aArea);
	}
}

//-----------------------------------------------------------------
// カウントダウンの処理
COUNTDOWN CCountdown::Countdown(COUNTDOWN aCountdown)
{
	// 描画フラグがONのとき
	if (aCountdown.bDraw == B_ON)
	{
		// 更新フラグがONのとき
		if (aCountdown.bUpdate == B_ON)
		{
			// 秒とコンマの値が終了値より大きい間続ける
			if (aCountdown.Timer.Tim.End <= aCountdown.Timer.Tim.Count&&aCountdown.Timer.Comma.End <= aCountdown.Timer.Comma.Count)
			{
				// コンマの値が終了値と一致したとき
				if (aCountdown.Timer.Comma.Count <= aCountdown.Timer.Comma.End)
				{
					aCountdown.Timer.Tim.Count--;
					aCountdown.Timer.Comma.Count = OneSecond;
				}
				aCountdown.Timer.Comma.Count--;

				// 秒の値が終了値と一致したとき
				if (aCountdown.Timer.Tim.Count <= aCountdown.Timer.Tim.End)
				{
					aCountdown.bUpdate = B_OFF;
				}
			}
		}
		else
		{
			// タイマー終了時にカウンタ表示を移動させる
			aCountdown.Timer.List.Poly.x += FONT_MOVE_SPPED;
			if (1500.0f <= aCountdown.Timer.List.Poly.x)
			{
				aCountdown.bDraw = B_OFF;
			}
		}
	}

	return aCountdown;
}

//-----------------------------------------------------------------
// 全パラメーター取得
COUNTDOWN CCountdown::Get(void)
{
	return g_aCountdown[0];
}