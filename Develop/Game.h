//=================================================================
// ゲーム処理[Game.h]
// author: tatuya ogawa
//=================================================================

#ifndef _GAME_H_ // インクルードガード
#define _GAME_H_

//=================================================================
// 構造体
typedef struct _GAME
{
	bool Control; // 演出フラグ
	bool ControlStart; // START時の演出フラグ
	bool ControlEnd; // 終了時の演出フラグ
	bool ControlOver; // ゲームオーバー時の演出フラグ
}GAME;

//=================================================================
// ゲーム
class CGame
{
private:
	// ゲームスタート演出
	GAME Start(GAME aGame);

	// ゲーム終了演出
	GAME End(GAME aGame);

	// ゲームオーバー演出
	GAME Over(GAME aGame);
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	GAME Update(void);

	// 描画開始
	void Draw(void);
};

// 初期化
void InitGame(void);

// 終了処理
void UninitGame(void);

// 更新処理
void UpdateGame(void);

// 描画開始
void DrawGame(void);
#endif // !_GAME_H_
