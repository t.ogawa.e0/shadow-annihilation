//=================================================================
// 当たり判定[Judgment.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "MovePosData.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "Judgment.h"
#include "Explosion.h"

//-----------------------------------------------------------------
// 初期化
void CJudgment::Init(void)
{

}

//-----------------------------------------------------------------
// 終了処理
void CJudgment::Uninit(void)
{

}

//-----------------------------------------------------------------
// 更新処理
void CJudgment::Update(bool ControlStart)
{
	// 処理有効時
	if (ControlStart != true)
	{
		CBullet bullet;
		CEnemy enemy;
		CPlayer player;
		CExplosion explosion;

		// バレットの当たり判定
		for (int i = 0; i < MAX_BULLET; i++)
		{
			// バレット出現時
			if (bullet.Get(i).bBullet == true)
			{
				// ID識別
				if (bullet.Get(i).HitID != player.Get().ID)
				{
					// プレイヤー出現時
					if (player.Get().bEmergence != false)
					{
						bool bHit = Judgment(bullet.Get(i).List.Poly, player.Get().List.Poly);
						HitBullet(bHit, i);
						explosion.Set(player.Get().List, bullet.Get(i).HitID, bHit);
						player.Damage(bHit, bullet.Get(i).Damage);
					}
				}

				// エネミーループ
				for (int s = 0; s < _NUM_ENEMY_; s++)
				{
					// ID識別
					if (bullet.Get(i).HitID != enemy.Get(s).ID)
					{
						// エネミー出現時
						if (enemy.Get(s).bEmergence != false)
						{
							bool bHit = Judgment(bullet.Get(i).List.Poly, enemy.Get(s).List.Poly);
							HitBullet(bHit, s);
							explosion.Set(enemy.Get(s).List, bullet.Get(i).HitID, bHit);
							enemy.Damage(bHit, bullet.Get(i).Damage, s);
						}
					}
				}
			}
		}

		// エネミーとプレイヤーの当たり判定
		for (int i = 0; i < _NUM_ENEMY_; i++)
		{
			// エネミーとプレイヤーが出現時
			if (player.Get().bEmergence != false && enemy.Get(i).bEmergence != false)
			{
				bool bHit = Judgment(player.Get().List.Poly, enemy.Get(i).List.Poly);
				player.Damage(bHit, enemy.Get(i).Palameter.Attack);
				enemy.Damage(bHit, player.Get().Palameter.Attack, i);
				explosion.Set(enemy.Get(i).List, _COLLISION, bHit);
				explosion.Set(player.Get().List, _COLLISION, bHit);
			}
		}
	}
}

//-----------------------------------------------------------------
// 描画開始
void CJudgment::Draw(void)
{

}

//-----------------------------------------------------------------
// 当たり判定
bool CJudgment::Judgment(FLOAT_List TargetA, FLOAT_List TargetB)
{
	ROUND A, B;
	
	A.x = (TargetA.x + (TargetA.w / CENTER_POS));
	A.y = (TargetA.y + (TargetA.h / CENTER_POS));
	A.r = min(TargetA.w, TargetA.h);

	B.x = (TargetB.x + (TargetB.w / CENTER_POS));
	B.y = (TargetB.y + (TargetB.h / CENTER_POS));
	B.r = min(TargetB.w, TargetB.h);

	return  Circle(A, B);
}

//-----------------------------------------------------------------
// 対象物A,Bの当たり判定(円形 : 2D用)
bool CJudgment::Circle(ROUND TargetA, ROUND TargetB)
{
	ROUND Target;

	TargetA.r = (TargetA.r * CORRECTION) * CORRECTION;
	TargetB.r = (TargetB.r * CORRECTION) * CORRECTION;
	Target.x = TargetB.x - TargetA.x;
	Target.y = TargetB.y - TargetA.y;
	Target.r = Target.x * Target.x + Target.y * Target.y; // 次条

	return _ROUNDCRATIO_(Target.r, TargetA.r, TargetB.r); // 半径×半径と次条を比べる
}

//-----------------------------------------------------------------
// バレットに接触時
void CJudgment::HitBullet(bool Hit, int Count)
{
	if (Hit)
	{
		CBullet bullet;

		bullet.EndProcess(Count);
	}
}