//=================================================================
// カーソル[Cursor.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "main.h"
#include "Cursor.h"
#include "Mouse.h"

//=================================================================
// グローバル変数
static CURSOR *g_aCursor; // カーソルのデータ

//-----------------------------------------------------------------
// 初期化
void CCursor::Init(void)
{
	CPolygon polygon;

	g_aCursor = MemoryAllocation(g_aCursor, NUMCURSOR);

	// カーソルの初期化
	for (int i = 0; i < NUMCURSOR; i++)
	{
		g_aCursor[i].List = polygon.PolygonDataInitList();
		g_aCursor[i].List.TexName = GAME_TEXTURE_LIST::TEXTURE_Cursor;
		g_aCursor[i].List.Poly.w = (float)polygon.GetTextureInfo(g_aCursor[i].List.TexName).Widht;
		g_aCursor[i].List.Poly.h = (float)polygon.GetTextureInfo(g_aCursor[i].List.TexName).Height;
		g_aCursor[i].List.bAffine = B_OFF;
		g_aCursor[i].List.Ang.x = (g_aCursor[i].List.Poly.w / CENTER_POS);
		g_aCursor[i].List.Ang.y = (g_aCursor[i].List.Poly.h / CENTER_POS);
		g_aCursor[i].List.Ang.Angle = F_NULL;
		g_aCursor[i].List.bAffine = B_ON;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CCursor::Uninit(void)
{
	AllMemoryDelete(g_aCursor);
}

//-----------------------------------------------------------------
// 更新処理
void CCursor::Update(void)
{
	CMouse mouse;

	for (int i = 0; i < NUMCURSOR; i++)
	{
		g_aCursor[i].List.Poly = mouse.Cursor(g_aCursor[i].List.Poly.w, g_aCursor[i].List.Poly.h);
		g_aCursor[i] = FrameCursor(g_aCursor[i], (float)SCREEN_WIDTH, (float)SCREEN_HEIGHT);
		g_aCursor[i].List.Ang.Angle += SCOPE_SPPED;
	}
}

//-----------------------------------------------------------------
// 描画処理
void CCursor::Draw(void)
{
	CPolygon polygon;

	for (int i = 0; i < NUMCURSOR; i++)
	{
		polygon.Draw(g_aCursor[i].List);
	}
}

//-----------------------------------------------------------------
// ウィンドウ外修正
CURSOR CCursor::FrameCursor(CURSOR Cursor, float WIDTH, float HEIGHT)
{
	if (Cursor.List.Poly.x <= F_NULL)
	{
		Cursor.List.Poly.x = F_NULL;
	}

	if (Cursor.List.Poly.y <= F_NULL)
	{
		Cursor.List.Poly.y = F_NULL;
	}

	if (WIDTH - Cursor.List.Poly.w <= Cursor.List.Poly.x)
	{
		Cursor.List.Poly.x = WIDTH - Cursor.List.Poly.w;
	}

	if (HEIGHT - Cursor.List.Poly.h <= Cursor.List.Poly.y)
	{
		Cursor.List.Poly.y = HEIGHT - Cursor.List.Poly.h;
	}

	return Cursor;
}

//-----------------------------------------------------------------
// 全パラメーター取得
CURSOR CCursor::Get(void)
{
	return g_aCursor[0];
}