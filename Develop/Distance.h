//=================================================================
// ��������[Distance.h]
// author: tatuya ogawa
//=================================================================

#ifndef _DISTANCE_H_
#define _DISTANCE_H_

class CDistance
{
private:
public:
	// �^�[�Q�b�g�Ƃ̋���(�񎟌�)
	float Distance2D(FLOAT_List TargetA, FLOAT_List TargetB);

	// �^�[�Q�b�g�Ƃ̋���(�O����)
	float Distance3D(FLOAT_List TargetA, FLOAT_List TargetB);
};

#endif // !_DISTANCE_H_
