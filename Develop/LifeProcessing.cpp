//=================================================================
// ライフ[LifeProcessing.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "main.h"
#include "LifeProcessing.h"
#include "MovePosData.h"
#include "Player.h"
#include "Enemy.h"
#include "Restriction.h"

//=================================================================
// グローバル変数
static LIFEBER *g_aPlayerLifeBer;
static LIFEBER *g_aEnemyLifeBer;

//-----------------------------------------------------------------
// 初期化
void CLife::Init(void)
{
	CPolygon polygon;
	int Name = GAME_TEXTURE_LIST::TEXTURE_LIFEBACKGROUND;
	int nCount = 0;

	g_aPlayerLifeBer = MemoryAllocation(g_aPlayerLifeBer, PLAYER_LIFEBAR_PARTS);
	g_aEnemyLifeBer = MemoryAllocation(g_aEnemyLifeBer, _MULTIPLIED_(ENEMY_LIFEBAR_PARTS, _NUM_ENEMY_));

	// プレイヤーのライフバー初期化
	for (int i = 0; i < PLAYER_LIFEBAR_PARTS; i++)
	{
		g_aPlayerLifeBer[i].List = polygon.PolygonDataInitList();
		g_aPlayerLifeBer[i].List.TexName = Name;
		g_aPlayerLifeBer[i].List.Poly.w = PLAYER_LIFEBAR_WIDTH;
		g_aPlayerLifeBer[i].List.Poly.h = PLAYER_LIFEBAR_HEIGHT;
		g_aPlayerLifeBer[i].Life.Current = g_aPlayerLifeBer[i].Life.Max = I_NULL;
		g_aPlayerLifeBer[i].OldLife.Current = g_aPlayerLifeBer[i].OldLife.Max = I_MAX;
		if (Name == GAME_TEXTURE_LIST::TEXTURE_LIFEMAIN)
		{
			g_aPlayerLifeBer[i].List.Poly.x += 5.0f;
			g_aPlayerLifeBer[i].List.Poly.w = PLAYER_LIFEBAR_WIDTH - 10.0f;
			g_aPlayerLifeBer[i].List.Poly.h = PLAYER_LIFEBAR_HEIGHT;
		}
		Name--;
	}

	Name = 0;
	// エネミーのライフバー生成
	for (int i = 0; i < (ENEMY_LIFEBAR_PARTS * _NUM_ENEMY_); i++)
	{
		if (nCount == ENEMY_LIFEBAR_PARTS) { nCount = 0; Name = 0; }
		g_aEnemyLifeBer[i].List = polygon.PolygonDataInitList();
		g_aEnemyLifeBer[i].List.TexName = LOAD_TEXTURE_LIST::TEXTURE_COLOR;
		g_aEnemyLifeBer[i].List.Poly.w = ENEMY_LIFEBAR_WIDTH;
		g_aEnemyLifeBer[i].List.Poly.h = ENEMY_LIFEBAR_HEIGHT;
		g_aEnemyLifeBer[i].Life.Current = g_aEnemyLifeBer[i].Life.Max = I_NULL;
		g_aEnemyLifeBer[i].OldLife = g_aEnemyLifeBer[i].Life;
		g_aEnemyLifeBer[i].List.Color.r = 123;
		g_aEnemyLifeBer[i].List.Color.g = 0;
		g_aEnemyLifeBer[i].List.Color.b = 0;

		if (Name == 1)
		{
			g_aEnemyLifeBer[i].List.Poly.w = ENEMY_LIFEBAR_WIDTH - 4.0f;
			g_aEnemyLifeBer[i].List.Poly.h = ENEMY_LIFEBAR_HEIGHT - 4.0f;
			g_aEnemyLifeBer[i].List.Color.r = 255;
			g_aEnemyLifeBer[i].List.Color.g = 0;
			g_aEnemyLifeBer[i].List.Color.b = 0;
		}
		Name++;
		nCount++;
	}
}

//-----------------------------------------------------------------
// 終了処理
void CLife::Uninit(void)
{
	AllMemoryDelete(g_aPlayerLifeBer);
	AllMemoryDelete(g_aEnemyLifeBer);
}

//-----------------------------------------------------------------
// 更新処理
void CLife::Update(void)
{
	int Name = 0;
	int nCount = 0;
	for (int i = 0; i < (ENEMY_LIFEBAR_PARTS * _NUM_ENEMY_); i++)
	{
		if (nCount == ENEMY_LIFEBAR_PARTS) { nCount = 0; Name = 0; }
		if (Name == 1)
		{
			g_aEnemyLifeBer[i] = Life(g_aEnemyLifeBer[i], ENEMY_LIFEBAR_WIDTH - 4.0f);
		}
		Name++;
		nCount++;
	}
	g_aPlayerLifeBer[1] = Life(g_aPlayerLifeBer[1], PLAYER_LIFEBAR_WIDTH - 10.0f);
}

//-----------------------------------------------------------------
// 描画開始
void CLife::Draw(int ID)
{
	CPolygon polygon;
	bool bON = B_OFF;
	int aArea = 0;
	int s = 0;
	int k = 0;
	
	// IDを判別し描画する
	switch (ID)
	{
	case 0: // プレイヤーのHPバー
		for (int i = 0; i < PLAYER_LIFEBAR_PARTS; i++)
		{
			if (i == 1) { bON = B_ON; }
			else { bON = B_OFF; }
			polygon.Draw(g_aPlayerLifeBer[i].List, bON);
		}
		break;
	case 1: // エネミーのHPバー
		CEnemy enemy;
		CRestriction restriction;
		
		aArea = polygon.StaticAreaStart();
		for (int i = 0; i < (ENEMY_LIFEBAR_PARTS * _NUM_ENEMY_); i++)
		{
			if (s == 2) { s = 0; k++; }
			if (restriction.Draw(g_aEnemyLifeBer[i].List.Poly) == true)
			{
				// エネミーが出現時
				if (enemy.Get(k).bEmergence != false)
				{
					polygon.Draw(g_aEnemyLifeBer[i].List);
				}
			}
			s++;
		}
		polygon.StaticAreaEnd(aArea);
		break;
	default:
		break;
	}
}

//-----------------------------------------------------------------
// ライフバー処理
LIFEBER CLife::Life(LIFEBER LifeBer, float Percent)
{
	int Life[LIFBERLIMIT] = { 1,10,100,1000,10000,100000 };
	int LifeMAX[LIFBERLIMIT] = { 9,99,999,9999,99999,999999 };
	// 前回処理した値と一致していないとき
	if (LifeBer.OldLife.Current != LifeBer.Life.Current)
	{
		float fPercent; // 100％ゲージ
		float fLifePercent; // ％HP
		float fLifeBer; // ライフバー

		fPercent = fLifePercent = fLifeBer = F_NULL;

		for (int i = 0; i < LIFBERLIMIT; i++)
		{
			if (Life[i] <= LifeBer.Life.Max&&LifeBer.Life.Max <= LifeMAX[i]) // 100%のバー
			{
				if (Life[0] <= LifeBer.Life.Max&&LifeBer.Life.Max <= LifeMAX[0]) // 100%のバー
				{
					fPercent = Percent / (float)LifeBer.Life.Max; // ライフバーの幅をHP最大値で割る
					fLifePercent = (float)LifeBer.Life.Max / (float)LifeBer.Life.Max; // 1%のHPを出す
				}
				else
				{
					fPercent = Percent / (float)Life[i]; // ライフバーの幅をHP最大値で割る
					fLifePercent = (float)LifeBer.Life.Max / (float)Life[i]; // 1%のHPを出す
				}
			}
		}

		// ライフバー生成
		for (float i = F_NULL; i < (float)LifeBer.Life.Current; i += fLifePercent)
		{
			fLifeBer += fPercent;
		}
		LifeBer.List.Poly.w = fLifeBer;
		LifeBer.OldLife = LifeBer.Life;
	}

	return LifeBer;
}

//-----------------------------------------------------------------
// プレイヤーのライフセット
void CLife::SetPlayerLife(LIFE Life)
{
	g_aPlayerLifeBer[1].Life = Life;
}

//-----------------------------------------------------------------
// エネミーのライフセット
void CLife::SetEnemyLife(LIFE Life, FLOAT_List Poly, int NumEnemy)
{
	int nCount = 0;

	for (int i = (ENEMY_LIFEBAR_PARTS * NumEnemy); i< (ENEMY_LIFEBAR_PARTS * (NumEnemy + 1)); i++)
	{
		g_aEnemyLifeBer[i].List.Poly.x = Poly.x - (Poly.w / 2.0f) - 4.0f;
		g_aEnemyLifeBer[i].List.Poly.y = Poly.y - (Poly.w / 2.0f) - 4.0f;
		if (nCount == 1)
		{
			g_aEnemyLifeBer[i].List.Poly.x = Poly.x - (Poly.w / 2.0f) - 2.0f;
			g_aEnemyLifeBer[i].List.Poly.y = Poly.y - (Poly.w / 2.0f) - 2.0f;
			g_aEnemyLifeBer[i].Life = Life;
		}
		nCount++;
	}
}