//=================================================================
// 番号[Number.cpp]
// author: tatuya ogawa
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "Number.h"

//-----------------------------------------------------------------
// 初期化
int CNumber::Init(int Digit)
{
	int MaxScore = PUSYU_SCORE;

	for (int i = 1; i < Digit; i++)
	{
		MaxScore *= PUSYU_SCORE;
	}
	MaxScore--;

	return MaxScore;
}

//-----------------------------------------------------------------
// 終了処理
void CNumber::Uninit(void)
{
	
}

//-----------------------------------------------------------------
// 更新処理
void CNumber::Update(void)
{

}

//-----------------------------------------------------------------
// 描画開始
void CNumber::Draw(POLYGON List, NUMBER Number, int Score)
{
	CPolygon polygon;

	float PosX = List.Poly.x;

	//左詰め対応
	if (Number.LeftAlignment == true)
	{
		int Set = Score;

		Number.Digit = 1;
		for (;;)
		{
			Set /= PUSYU_SCORE;
			if (Set == I_NULL)
			{
				break;
			}
			Number.Digit++;
		}
	}

	Score = min(Number.Max, Score);

	for (int i = Number.Digit - 1; i >= I_NULL; i--)
	{
		// 1234567
		List.Anim.Count = Score % PUSYU_SCORE;
		// 123456

		List.Poly.x = PosX + i * List.Poly.w;
		polygon.Draw(List);

		Score /= PUSYU_SCORE;
		// 12345.6
		if (!Number.Zero && Score == I_NULL) { break; }
	}
}