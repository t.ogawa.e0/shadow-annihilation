//=================================================================
// デバッグ[Debug.h]
// author: tatuya ogawa
//=================================================================

#ifndef _DEBUG_H_ // インクルードガード
#define _DEBUG_H_

//=================================================================
// デバッグ
class _CDebug
{
private:
public:
	// 初期化
	void Init(void);

	// 終了処理
	void Uninit(void);

	// 更新処理
	void Update(void);

	// 描画処理
	void GameDraw(void);
};

#endif // !_DEBUG_H_
