//=================================================================
// キーボード処理[Keyboard.cpp]
// author: tatuya ogawa 
//=================================================================

#include <math.h>
#include "main.h"
#include "input.h"
#include "IncludeData\DirectX2D.h"
#include "IncludeData\NumBerPallet.h"
#include "Keyboard.h"

//=================================================================
// KEYデータ格納用構造体
typedef struct _KEYINT
{
	int x;
	int y;
}KEYINT;

//=================================================================
// グローバル変数
static KEYLIST g_aKeyList;
static KEYBOARD g_aKeyBoard[_NUMKEYBOARD_];

//-----------------------------------------------------------------
// 初期化
void CKey::Init(void)
{
	int nKey[] = { DIK_W, DIK_A, DIK_S, DIK_D }; // 使うキー格納
	int nKeyCase[] = { DIK_BACK,DIK_A,DIK_B,DIK_C,DIK_D,DIK_E,DIK_F,DIK_G,DIK_H,DIK_I,DIK_J,DIK_K,DIK_L,DIK_M,DIK_N,DIK_O,DIK_P,DIK_Q,DIK_R,DIK_S,DIK_T,DIK_U,DIK_V,DIK_W,DIK_X,DIK_Y,DIK_Z };
	int nNumber[] = { 0,1,2,3,4,5,6,7 }; // キーID

	for (int i = 0; i < (int)(sizeof(nKeyCase) / _BIT_); i++)
	{
		g_aKeyBoard[i].KeyCase = nKeyCase[i];
		g_aKeyBoard[i].KeyID = i;
	}

	// キー情報格納
	for (int i = 0; i < (int)(sizeof(nKey) / _BIT_); i++)
	{
		g_aKeyList.Key[i] = nKey[i];
	}

	// キーID格納
	for (int i = 0; i < (int)(sizeof(nNumber) / _BIT_); i++)
	{
		g_aKeyList.Number[i] = nNumber[i];
	}

	// 回転角度自動割り出し
	for (int i = 0; i<(int)(sizeof(g_aKeyList.Angle) / _BIT_); i++)
	{
		g_aKeyList.Angle[i] = CreateAngle(SCREEN_WIDTH, SCREEN_HEIGHT, i);
	}
}

// キーボード
int CKey::KeyBoard(void)
{
	int nKey = _NUMKEYBOARD_;

	for (int i = 0; i < _NUMKEYBOARD_; i++)
	{
		if (_TRIGGER_(g_aKeyBoard[i].KeyCase))
		{
			nKey = g_aKeyBoard[i].KeyID;
			break;
		}
	}

	return nKey;
}

//-----------------------------------------------------------------
// 終了処理
void CKey::Uninit(void)
{

}

//-----------------------------------------------------------------
// 更新処理
void CKey::Update(void)
{

}

//-----------------------------------------------------------------
// 描画処理
void CKey::Draw(void)
{

}

//-----------------------------------------------------------------
// 回転角度の計算
float CKey::CreateAngle(float WIDTH, float HEIGHT, int AngleID)
{
	float Angle = F_NULL;
	float Posx = (float)WIDTH / CENTER_POS;
	float Posy = (float)HEIGHT / CENTER_POS;
	float fCenterX = Posx;
	float fCenterY = Posy;

	switch (AngleID)
	{
	case 0: // 上
		Angle = (float)atan2(fCenterY - fCenterY, WIDTH - fCenterX);
		break;
	case 1: // 左
		Angle = (float)atan2(F_NULL - fCenterY, fCenterX - fCenterX);
		break;
	case 2: // 下
		Angle = (float)atan2(fCenterY - fCenterY, F_NULL - fCenterX);
		break;
	case 3: // 左	
		Angle = (float)atan2(HEIGHT - fCenterY, fCenterX - fCenterX);
		break;
	case 4: // 右上
		for (;;)
		{
			Posx += F_DEF;
			Posy += F_DEF;
			if (WIDTH <= Posx || HEIGHT <= Posy)
			{
				break;
			}
		}
		Angle = (float)atan2(Posy - fCenterY, Posx - fCenterX);
		break;
	case 5: // 左上
		for (;;)
		{
			Posx += F_DEF;
			Posy -= F_DEF;
			if (WIDTH <= Posx || Posy <= F_NULL)
			{
				break;
			}
		}
		Angle = (float)atan2(Posy - fCenterY, Posx - fCenterX);
		break;
	case 6: // 右下
		for (;;)
		{
			Posx -= F_DEF;
			Posy += F_DEF;
			if (Posx <= F_NULL || HEIGHT <= Posy)
			{
				break;
			}
		}
		Angle = (float)atan2(Posy - fCenterY, Posx - fCenterX);
		break;
	case 7: // 左下	
		for (;;)
		{
			Posx -= F_DEF;
			Posy -= F_DEF;
			if (Posx <= F_NULL || Posy <= F_NULL)
			{
				break;
			}
		}
		Angle = (float)atan2(Posy - fCenterY, Posx - fCenterX);
		break;
	default:
		Angle = F_NULL;
		break;
	}

	return Angle;
}

//-----------------------------------------------------------------
// キーの処理
KEYFLOAT CKey::Case(int Key[])
{
	KEYINT Dir;
	KEYFLOAT aKey;
	static bool bLock[] = { false ,false };
	int nDir = -1;
	int nLock = 0;

	aKey.x = aKey.y = 0.0f;
	Dir.x = Dir.y = 0;

	// WASDキー
	for (int i = 0; i < _NUMKEY_; i++)
	{
		if (nLock == (int)sizeof bLock) { nLock = 0; }
		if (2 <= i) { nDir = 1; }
		if (GetKeyboardPress(Key[i]))
		{
			if (bLock[nLock] == false)
			{
				if (i == 0 || i == 2) { Dir.y = nDir; }
				if (i == 1 || i == 3) { Dir.x = nDir; }
				bLock[nLock] = true;
			}
		}
		else
		{
			bLock[nLock] = false;
		}
		nLock++;
	}

	if (Dir.x || Dir.y)
	{
		float length = (float)sqrt((float)(Dir.x * Dir.x + Dir.y * Dir.y));
		aKey.x = Dir.x / length;
		aKey.y = Dir.y / length;
	}

	return aKey;
}

//-----------------------------------------------------------------
// キー入力時の向き
float CKey::Direction(KEYLIST KeyList)
{
	static int nKey = 0;
	static bool bLock[] = { false ,false };
	int nCount = 0;

	// WASDキー
	for (int i = 0; i < _NUMKEY_; i++)
	{
		if (nCount == (int)sizeof bLock) { nCount = 0; }
		if (GetKeyboardPress(KeyList.Key[i]))
		{
			if (bLock[nCount] == false)
			{
				nKey = KeyList.Number[i];
				bLock[nCount] = true;
			}
		}
		else
		{
			bLock[nCount] = false;
		}
		nCount++;
	}
	
	// UpperRight 右上
	if (GetKeyboardPress(KeyList.Key[0]) && GetKeyboardPress(KeyList.Key[3]))
	{
		nKey = KeyList.Number[4];
	}

	// UpperLeft 左上
	if (GetKeyboardPress(KeyList.Key[0]) && GetKeyboardPress(KeyList.Key[1]))
	{
		nKey = KeyList.Number[5];
	}

	// BottomRight 右下
	if (GetKeyboardPress(KeyList.Key[2]) && GetKeyboardPress(KeyList.Key[3]))
	{
		nKey = KeyList.Number[6];
	}

	// BottomLeft 左下
	if (GetKeyboardPress(KeyList.Key[2]) && GetKeyboardPress(KeyList.Key[1]))
	{
		nKey = KeyList.Number[7];
	}

	return KeyList.Angle[nKey];
}

//-----------------------------------------------------------------
// キーのリスト
int CKey::KeyList(int num)
{
	int nAlphabet[] =
	{
		TEXTURE_TITLE_KUUHAKU,
		TEXTURE_TITLE_A,
		TEXTURE_TITLE_B,
		TEXTURE_TITLE_C,
		TEXTURE_TITLE_D,
		TEXTURE_TITLE_E,
		TEXTURE_TITLE_F,
		TEXTURE_TITLE_G,
		TEXTURE_TITLE_H,
		TEXTURE_TITLE_I,
		TEXTURE_TITLE_J,
		TEXTURE_TITLE_K,
		TEXTURE_TITLE_L,
		TEXTURE_TITLE_M,
		TEXTURE_TITLE_N,
		TEXTURE_TITLE_O,
		TEXTURE_TITLE_P,
		TEXTURE_TITLE_Q,
		TEXTURE_TITLE_R,
		TEXTURE_TITLE_S,
		TEXTURE_TITLE_T,
		TEXTURE_TITLE_U,
		TEXTURE_TITLE_V,
		TEXTURE_TITLE_W,
		TEXTURE_TITLE_X,
		TEXTURE_TITLE_Y,
		TEXTURE_TITLE_Z,
	};

	return nAlphabet[num];
}

//-----------------------------------------------------------------
// 全パラメーター取得
KEYLIST CKey::Get(void)
{
	return g_aKeyList;
}