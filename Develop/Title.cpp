//=================================================================
// タイトル処理[Title.cpp]
// author: 
//=================================================================

#include "IncludeData\DirectX2D.h"
#include "IncludeData\Template.h"
#include "IncludeData\sound.h"
#include "main.h"
#include "Title.h"
#include "input.h"
#include "SceneChange.h"
#include "Ranking.h"
#include "Load.h"

//=================================================================
// グローバル変数
static TITLE *g_aTitle;
static bool *g_Lanking;
static int *g_nFlameCount;

//-----------------------------------------------------------------
// 初期化
void InitTitle(void)
{
	CRanking ranking;
	CTitle title;

	ranking.Init();
	ranking.DrawInit();
	title.Init();
}

//-----------------------------------------------------------------
// 終了処理
void UninitTitle(void)
{
	CRanking ranking;
	CTitle title;

	ranking.Uninit();
	title.Uninit();
}

//-----------------------------------------------------------------
// 更新処理
void UpdateTitle(void)
{
	CTitle title;
	CRanking ranking;

	title.Update();

	if (g_Lanking[0] == true)
	{
		ranking.WindowScroll();
	}
}

//-----------------------------------------------------------------
// 描画開始
void DrawTitle(void)
{
	CTitle title;
	CRanking ranking;

	title.Draw();

	if (g_Lanking[0] == true)
	{
		ranking.Draw();
	}
}

//-----------------------------------------------------------------
// 初期化
void CTitle::Init(void)
{
	CPolygon polygon;
	int s = -1;

	g_Lanking = MemoryAllocation(g_Lanking, 1);
	g_nFlameCount = MemoryAllocation(g_nFlameCount, 1);
	g_aTitle = MemoryAllocation(g_aTitle, _NUM_TITLETEX);

	g_Lanking[0] = false;
	g_nFlameCount[0] = 0;

	for (int i = 0; i < _NUM_TITLETEX; i++)
	{
		g_aTitle[i].List = polygon.PolygonDataInitList();
		g_aTitle[i].bAlfa = false;
		g_aTitle[i].nAlfaCount = -1;
		g_aTitle[i].Time.Count = -1;
		g_aTitle[i].Time.End = TITIE_FONT_COUNT;
	}

	s++;
	g_aTitle[s].List.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE;
	g_aTitle[s].List.Poly.x = F_NULL;
	g_aTitle[s].List.Poly.y = F_NULL;
	g_aTitle[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aTitle[s].List.TexName).Widht;
	g_aTitle[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aTitle[s].List.TexName).Height;

	// PressEnterフォント
	s++;
	g_aTitle[s].List.TexName = TITLE_TEXTURE_LIST::TEXTURE_TITLE_PRESSENTER;
	g_aTitle[s].List.Poly.w = (float)polygon.GetTextureInfo(g_aTitle[s].List.TexName).Widht / TITLE_FontScale;
	g_aTitle[s].List.Poly.h = (float)polygon.GetTextureInfo(g_aTitle[s].List.TexName).Height / TITLE_FontScale;
	g_aTitle[s].List.Poly.x = ((SCREEN_WIDTH / CENTER_POS) - (g_aTitle[s].List.Poly.w / CENTER_POS));
	g_aTitle[s].List.Poly.y = ((SCREEN_HEIGHT / CENTER_POS) - (g_aTitle[s].List.Poly.h / CENTER_POS)) * 2;
	PlaySound(TITLE_SOUND_LIST::TITLE_Quriora);
}

//-----------------------------------------------------------------
// 終了処理
void CTitle::Uninit(void)
{
	AllMemoryDelete(g_nFlameCount);
	AllMemoryDelete(g_Lanking);
	AllMemoryDelete(g_aTitle);
}

//-----------------------------------------------------------------
// 更新処理
void CTitle::Update(void)
{
	CScene scene;
	
	int s = 1;

	if (_TRIGGER_(DIK_R))
	{
		PlaySound(LOAD_SOUND_LIST::Button_SE);
		g_Lanking[0] = _BOOL_(g_Lanking[0]);
		g_nFlameCount[0] = 0;
	}
	
	if (g_Lanking[0] == false)
	{
		if (g_nFlameCount[0] == TITLE_LIMITFLAME)
		{
			g_Lanking[0] = true;
			g_nFlameCount[0] = 0;
		}
	}
	else if (g_Lanking[0] == true)
	{
		if (g_nFlameCount[0] == (TITLE_LIMITFLAME / 2))
		{
			g_Lanking[0] = false;
			g_nFlameCount[0] = 0;
		}
	}

	// α値変動値の変更
	if (g_aTitle[s].bAlfa == false)
	{
		if (_TRIGGER_(DIK_RETURN))
		{
			PlaySound(LOAD_SOUND_LIST::Button_SE);
			g_Lanking[0] = false;
			g_aTitle[s].bAlfa = true;
			g_aTitle[s].Time.End = g_aTitle[s].Time.End / TITLE_FONT_ALFASPPED;
		}
	}

	// α値変更
	if (g_aTitle[s].Time.Count >= (g_aTitle[s].Time.End / 2))
	{
		g_aTitle[s].List.Color.a = I_NULL;
	}

	// α値変更
	if (g_aTitle[s].Time.Count >= g_aTitle[s].Time.End)
	{
		g_aTitle[s].List.Color.a = _RGBACOLOR_;
		g_aTitle[s].Time.Count = I_NULL;
		if (g_aTitle[s].bAlfa == true)
		{
			g_aTitle[s].nAlfaCount++;
		}
	}

	// シーン切り替えタイミング
	if (g_aTitle[s].nAlfaCount == TITLE_CHANGE_SPPED)
	{
		CLoad load;
		load.LoadTypeOFF();
		scene.Change(SCENE_HOME);
	}
	g_aTitle[s].Time.Count++;
	g_nFlameCount[0]++;
}

//-----------------------------------------------------------------
// 描画開始
void CTitle::Draw(void)
{
	CPolygon polygon;

	for (int i = 0; i < _NUM_TITLETEX; i++)
	{
		polygon.Draw(g_aTitle[i].List);
	}
}